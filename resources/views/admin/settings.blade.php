@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/generalSettings.settings') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>General Settings</h1>
        <!-- <ol class="breadcrumb">
            <li><a href="/admin"><span class="fa fa-dashboard"></span> {!! trans('admin/common.home') !!}</a></li>
            <li class="active">{!! trans('admin/generalSettings.settings') !!}</li>
        </ol> -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
                <!-- ./ notifications -->
            </div>
            <div class="col-xs-12">
                <div class="box">
                    @if(isset($setting))
                        {!! Form::model($setting, array('route' => array('settings.update', $setting->id), 'method' => 'PATCH', 'id' => 'setting-form', 'files' => true )) !!}
                        
                    @else
                        {!! Form::open(array('route' => 'settings.store', 'id' => 'setting-form', 'files' => true)) !!}
                    @endif
                    {!! Form::hidden('setting_id', isset($setting) ? $setting->id : 0 ,array('class'=>'form-control', 'id' => 'setting_id')) !!}
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            {!! Form::label('site_title', 'Site Title') !!}
                            {!! Form::text('site_title', old('site_title'),array('class'=>'form-control')) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        
                        <div class="form-group has-feedback">
                            {!! Form::label('Address','Address') !!}
                            {!! Form::textarea('address', old('address'),array('class'=>'form-control')) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        
                        
                        <div class="form-group has-feedback">
                            {!! Form::label('email','Email') !!}
                            {!! Form::text('email', old('email'),array('class'=>'form-control')) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        
                        <div class="form-group has-feedback">
                            {!! Form::label('phone', 'Phone No') !!}
                            {!! Form::text('phone', old('phone'),array('class'=>'form-control')) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        
                       
                        
                    </div>

                    <div class="form-group has-feedback">
                        {!! Form::label('term_condition','Terms and Condition') !!}
                        {!! Form::textarea('term_condition', old('term_condition'),array('class'=>'form-control')) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    </div>

                    <div class="box-footer">
                        {!! Form::submit('Submit',array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}
                        <a href="{!! URL::route('settings.index') !!}" class="btn btn-default">{!! trans('Cancel') !!}</a>
                    </div>
                    {!! Form::close()!!}
                </div> <!-- /.box -->
            </div> <!-- /.col-xs-12 -->
        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@section('scripts')
    <script>
        $(document).ready(function(){
            $(document).on('change','#language',function(){
                location.href = '{!! url("admin/settings/") !!}' + '/' + $(this).val() + '/edit';
            })
        });
    </script>
@stop