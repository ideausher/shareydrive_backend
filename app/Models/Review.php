<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Review"))
 */


class Review extends Model
{
    /**
     * @var string
     * @SWG\Property(
     *   property="ride_id",
     *   type="integer" 
     * )
     *  @SWG\Property(
     *   property="user_id",
     *   type="integer" 
     * )
     * @SWG\Property(
     *   property="rating",
     *   type="integer" 
     * )
     * @SWG\Property(
     *   property="rating_select",
     *   type="integer" 
     * )
     * @SWG\Property(
     *   property="info",
     *   type="integer" 
     * )
     * @SWG\Property(
     *   property="type",
     *   type="integer" 
     * )
     */
    protected $table = 'review';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ride_id', 'user_id','user_id_to' ,'rating', 'rating_select', 'info','type'
    ];

}
