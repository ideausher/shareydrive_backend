<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use App\Helpers\Response;

class AddVehicleReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'country' => ['required','string'],
            'car_manufacture' => ['required','string'],
            'car_model' => ['required','string'],
            'car_type' => ['required','string'],
            'car_color' => ['required','string'],
            'register_year' => ['required','integer','date_format:Y'],
            'register_state' => ['required','string']
        ];
    }


    /**
     * Overwrite Validation error response
     * 
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {   
        $failedRules = $validator->failed();
        $statusCode = Response::resposeCode(key($failedRules),$failedRules);
        $response = new JsonResponse([ 
            'message' => $validator->errors()->first()
        ], $statusCode);

        throw new ValidationException($validator, $response);

    }
}
