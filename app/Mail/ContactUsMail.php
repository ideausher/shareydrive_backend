<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUsMail extends Mailable
{
    use Queueable, SerializesModels;

    protected  $infos;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($infos)
    {
         $this->infos  =  $infos;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->infos['email'],$this->infos['firstname']." is trying to administrator.")
                    ->view('emails.contactus')
                    ->with($this->infos);
    }
}
