<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Ride Accept"))
 */

class RideAccepted extends Model
{
    /**
     * @var string
     * @SWG\Property(
     *   property="ride_id",
     *   type="integer" 
     * )
     *  @SWG\Property(
     *   property="user_id",
     *   type="integer" 
     * )
     * @SWG\Property(
     *   property="consume_seats",
     *   type="integer" 
     * )
     */

    protected $table = 'ride_accepted';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ride_id', 'user_id', 'consume_seats','status','is_accepted_by_driver'
    ];


    public function rides()
    {
        return $this->belongsTo('App\Models\Rides','id','ride_id');
    } 
    public function startLocation(){
        return $this->hasOne('App\Models\RideAddress','ride_id','ride_id')
                    ->where('type',1);
    }       

    /**
     * Relation with ride address 
     * End Location
     */
    public function endLocation(){
        return $this->hasOne('App\Models\RideAddress','ride_id','ride_id')
                    ->where('type',2);
    }

    // public function rideAddress()
    // {
    //     return $this->hasMany('App\Models\RideAddress','ride_id','ride_id');
    // }
    public function rideDetails()
    {
        return $this->hasMany('App\Models\Rides','id','ride_id');
    }

    public function userPassengerDetails()
    {
        return $this->hasMany('App\User','id','user_id');
    }
    
}
