<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{!! trans('admin/login.sitename') !!} :: {!! trans('admin/login.sign_in') !!}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        
        <link rel="shortcut icon" href="{!!asset('assets/admin/logo.png')!!}"  >
        
        <!-- bootstrap 3.2.0 -->
        <link href="{!! asset('assets/admin/bootstrap/css/bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{!! asset('assets/admin/font-awesome/css/font-awesome.min.css')!!}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{!!asset('assets/admin/dist/css/AdminLTE.min.css')!!}" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <link href="{!!asset('assets/admin/css/style.css')!!}" rel="stylesheet" type="text/css" />
    </head>
    <body>

    <nav class="navbar navbar-expand-md navbar-light mb-3">
    <div class="container-fluid">
        <a href="#" class="navbar-brand mr-3"><img  src="{{ url('assets/admin/logo.png') }}" height="80px" alt="image"></a>
    </div>    
</nav>


    <div class="jumbotron" style="background-color:white;">
        <div class="row">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-6">
                <img src="{{ url('assets/admin/carpool.png') }}" height="400px" alt="image">
            </div>
            <div class="col-sm-3">
                <h2 class="text-center">Verify Otp</h2>
                
                    <!-- <div class="form-row">
                      <div>
                        <label class="sr-only" for="inlineFormInputGroupUsername">Username</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <div class="input-group-text">Phone Number</div>
                          </div>
                          <input type="text" class="form-control" id="inlineFormInputGroupUsername">
                        </div>
                      </div>
                      &nbsp;&nbsp;&nbsp;
                      <button class="btn" type="submit" style="background-color: rgb(193, 9, 88); color: white;">Login/Register</button>
                    </div> -->

        <div class="login-box">
            <!-- Notifications -->
                @include('admin.includes.notifications')
            <!-- ./ notifications -->
			@if(isset($attempt_no))
		<div class="alert alert-danger alert-block session-box">
    <span class="fa fa-ban"></span>
    <button type="button" class="close" data-dismiss="alert"><span class="glyphicon glyphicon-remove"></span></button>
    <strong>Error!</strong>
       Invalid otp  attempt no {{$attempt_no}}
    </div>
	@endif
            {!! Form::open(array('method' => 'POST','url' => 'admin/password/generate', 'id' => 'verifyLoginOtp','novalidate' => 'novalidate')) !!}
            <div class="form-group has-feedback form-feedback">
                {!! Form::number('otp', null,array('class'=>'form-control', 'placeholder' => 'OTP')) !!}
                <span class="glyphicon glyphicon-phone form-control-feedback"></span>
            </div>
           
                                <input type="hidden" name="id" value="{{ $user->id }}">
								
            
            <div class="row sign-in-row">
                <div class="col-xs-12 sign-in">
                    {!! Form::submit('Submit Otp', array('class'=>'btn bg-purple btn-block')) !!}
                </div>
            </div>
            {!! Form::close() !!}
           
        </div><!-- /.login-box-body -->
                 
                  
            </div>
        </div>
    </div>





   

        <!-- jQuery 2.1.4 -->
        <script src="{!!asset('assets/admin/plugins/jQuery/jQuery-2.1.4.min.js')!!}" type="text/javascript"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="{!!asset('assets/admin/bootstrap/js/bootstrap.min.js')!!}" type="text/javascript"></script>

        <!-- jQuery Validation js -->
        <script src="{!!asset('assets/admin/plugins/validation/jquery.validate.min.js')!!}" type="text/javascript"></script>
        <script src="{!!asset('assets/admin/plugins/validation/additional-methods.js')!!}" type="text/javascript"></script>
        @if(config('app.locale')!='en')
        <script src="{!!asset('assets/admin/plugins/validation/localization/messages_'.config('app.locale').'.js')!!}" type="text/javascript"></script>
        @endif
        <!-- AdminLTE App -->
        <script src="{!!asset('assets/admin/dist/js/app.min.js')!!}" type="text/javascript"></script>
        <script src="{!!asset('assets/admin/js/common.js')!!}" type="text/javascript"></script>
    </body>
</html>
<script>
$(function () {
    //hide alert message when click on remove icon
    $(".close").click(function () {
        $(this).closest('.alert').addClass('hide');
    });
});
</script>