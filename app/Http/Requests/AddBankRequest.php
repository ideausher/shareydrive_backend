<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use App\Helpers\Response;

class AddBankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'country' => ['required','string','regex:/^[a-z A-Z]+$/|max:25'],
            'car_manufacture' => ['required','string','regex:/^[a-z A-Z]+$/|max:100'],
            'car_model' => ['required','string','regex:/^[a-z A-Z0-9]+$/|max:25'],
            'car_type' => ['required','string','regex:/^[a-z A-Z]+$/|max:150'],
            'car_color' => ['required','string','regex:/^[a-zA-Z]+$/|max:50'],
            'register_year' => ['required','integer','date_format:Y'],
            'register_state' => ['required','string','regex:/^[a-z A-Z]+$/|max:100']
        ];
    }


    /**
     * Overwrite Validation error response
     * 
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {   
        $failedRules = $validator->failed();
        $statusCode = Response::resposeCode(key($failedRules),$failedRules);
        $response = new JsonResponse([ 
            'message' => $validator->errors()->first()
        ], $statusCode);

        throw new ValidationException($validator, $response);

    }
}
