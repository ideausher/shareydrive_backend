@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/generalSettings.settings') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Referral Settings</h1>
        <!-- <ol class="breadcrumb">
            <li><a href="/admin"><span class="fa fa-dashboard"></span> {!! trans('admin/common.home') !!}</a></li>
            <li class="active">{!! trans('admin/generalSettings.settings') !!}</li>
        </ol> -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
                <!-- ./ notifications -->
            </div>
            <div class="col-xs-12">
                <div class="box">
                @if(isset($data))
                    {!! Form::model($data, array('url' => 'admin/referralSettings', 'method' => 'POST', 'id' => 'updateBrand-form', 'files' => true )) !!}
                 
                    @else
                    {!! Form::open(array('url' => 'admin/addBrand', 'id' => 'addBrand-form','method'=>'POST', 'files' => true)) !!}
                @endif    
                {!! Form::open(array('method' => 'POST','url' => 'admin/referralSettings', 'id' => 'referralForm')) !!}
                     
                
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            {!! Form::label('referral_code_text', 'Referral Default Text') !!}
                            {!! Form::text('referral_code_text', old('referral_code_text'),array('class'=>'form-control' )) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            {!! Form::label('referral_code_discount', 'Referral Discount : (Percentage)') !!}
                            {!! Form::text('referral_code_discount', old('referral_code_discount'),array('class'=>'form-control' )) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        
                    <div class="box-footer">
                        {!! Form::submit('Save',array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}
                        <a href="{!! URL::route('settings.index') !!}" class="btn btn-default">{!! trans('Cancel') !!}</a>
                    </div>    
                    {!! Form::close()!!}
                </div> <!-- /.box -->
            </div> <!-- /.col-xs-12 -->
        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@section('scripts')
 
@stop