<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Datatable\SSP;
use App\Helpers\Common;
use App\Service;
use App\User;
use App\Booking;
use App\BookingDetail;
use App\Http\Requests\RefundStripe;
use App\BookingRefund;
use Validator;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Admin\BookingStatusMail;
use Stripe\Stripe;
use App\Transaction;

class BookingController extends Controller {

    /**
     * Booking Model
     * @var Booking
     */
    protected $booking;
    protected $pageLimit;
    protected $serviceList;


    protected $response = [
        'status' => 0,
        'message' => '',
    ];

    /**
     * Inject the models.
     * @param Booking $booking
     */
    public function __construct(Booking $booking) {
        $this->booking = $booking;
        $this->pageLimit = config('settings.pageLimit');
        //$this->serviceList = ['' => 'Select Service'] + Service::pluck('title', 'id')->all();
       // $this->userList = ['' => 'Select User'] + User::select(DB::raw("CONCAT(firstname, ' ',lastname) as name"), 'id')->pluck('name', 'id')->toArray();
    }

    /**
     * Display a listing of bookings
     *
     * @return Response
     */
    public function index(Request $request) {
        //remove search array
        //session()->forget('SEARCH');
        //end code

        // $serviceList = $this->serviceList;
        // $userList = $this->userList;

        // $userId = request()->segment(3);
        // if ($userId) {
        //     // Grab user the bookings
            
        // } else {
        //     // Grab all the bookings
        //     $bookings = Booking::orderBy('created_at', 'DESC')->paginate($this->pageLimit);
        // }
        
        return view('admin/bookingList');
    }


    /**
     * Display the specified booking.
     *
     * @param  int  $id
     * @return Response
     */
    
    public function show($id) {

        $booking = Booking::find($id);
        return view('admin/bookingList', compact('id'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Booking::destroy($id);

        session()->flash('success_message', trans('admin/booking.booking_delete_message'));
        $array = array();
        $array['success'] = true;
        $array['message'] = trans('admin/booking.booking_delete_message');
        echo json_encode($array);
    }

    public function changeBookingStatus(Request $request) {
        $data = $request->all();

        $booking = Booking::find($data['id']);
        $booking->status = $data['value'];
        $booking->save();

        if ($data['value'] == 'cancel') {
            $user = User::find($booking->user->id);
            $user->credit = $user->credit + $booking->amount;
            $user->save();
        }

        $userEmail = $booking->user->email;
        //send booking mail to user and bcc to admin
        Mail::to($userEmail)->send(new BookingStatusMail($booking));

        session()->flash('success_message', trans('admin/booking.booking_status_message'));
        $array = array();
        $array['success'] = true;
        $array['message'] = trans('admin/booking.booking_status_message');
        echo json_encode($array);
    }

    /**
     * search booking from database.
     * 
     * @author Dhaval
     * @param  Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {

        //reset search
        if ($request->has('reset')) {
            $request->session()->forget('SEARCH');
            return redirect('admin/booking');
        }
        //end code

        if ($request->get('search_by') != '') {
            session(['SEARCH.SEARCH_BY' => trim($request->get('search_by'))]);
        }

        if ($request->get('search_txt') != '') {
            session(['SEARCH.SEARCH_TXT' => trim($request->get('search_txt'))]);
        }

        if ($request->session()->get('SEARCH.SEARCH_BY') != '') {


            $query = Booking::select('*');

            if ($request->session()->get('SEARCH.SEARCH_BY') == 'service') {

                $query->where('service_name', 'LIKE', '%' . $request->session()->get('SEARCH.SEARCH_TXT') . '%');
            }

            if ($request->session()->get('SEARCH.SEARCH_BY') == 'id') {

                $query->where('id', '=', $request->session()->get('SEARCH.SEARCH_TXT'));
            }

            if ($request->session()->get('SEARCH.SEARCH_BY') == 'user') {

                $getUser_id = User::select('id')->where('firstname', 'LIKE', '%' . $request->session()->get('SEARCH.SEARCH_TXT') . '%')->get()->toArray();
                $user_type = '';
                $query = '';
                
                $query = Booking::select('*');
                if ($getUser_id) {
                    //echo $getUser_id->count();
                    $ids = array_column($getUser_id, 'id');
                    if ($ids) {
                        $query->whereIn('user_id', $ids);
                    }
                } else {
                    $query->where('phone', 'LIKE', '%' . $request->session()->get('SEARCH.SEARCH_TXT') . '%');
                }
            }

            if ($request->session()->get('SEARCH.SEARCH_BY') == 'vendor') {
                $query->where('vender_name', 'LIKE', '%' . $request->session()->get('SEARCH.SEARCH_TXT') . '%');
            }

            if ($request->session()->get('SEARCH.SEARCH_BY') == 'online') {
                $query->where('payment_type', '=', 0);
            }
            if ($request->session()->get('SEARCH.SEARCH_BY') == 'offline') {
                $query->where('payment_type', '=', 1);
            }

            $bookings = $query->orderBy('created_at', 'desc')->paginate($this->pageLimit);

            $serviceList = $this->serviceList;
            $userList = $this->userList;
            return view('admin/bookingList', compact('bookings', 'serviceList', 'userList'));
        } else {
            return redirect('admin/booking');
        }
    }

    /**
     *  export transactions-list.csv
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request) {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=bookings-list.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array('Service', 'Name', 'Email', 'Mobile', 'Credits', 'Date', 'Booking Status'));

        $bookings = Booking::orderBy('created_at', 'DESC')->get();
        foreach ($bookings as $data) {
            $date = '';
            $spots = $data->bookingDetail;
            foreach ($spots as $key => $spot) {
                $date .= ' (' . ($key + 1) . ') ' . date('d-m-Y h:i A', strtotime($spot->start_time)) . 'to' . date('d-m-Y h:i A', strtotime($spot->end_time));
            }
            fputcsv($output, array(
                $data->service->title,
                $data->full_name,
                $data->email,
                $data->phone,
                $data->amount,
                $date,
                $data->status
                    )
            );
        }
        fclose($output);
        exit;
    }
    
    public function getBookingData(Request $request) {
        $doctorid = $request->id;
        
        $table = 'bookings';
        $primaryKey = 'id';
        $columns = array(
            array('db' => 'bookings.id', 'dt' => 11, 'field' => 'id'), 
            array('db' => 'bookings.reference_id', 'dt' => 0, 'field' => 'reference_id'), 
            array('db' => 'vu.email', 'as' => 'vendorEmail', 'dt' => 1, 'field' => 'vendorEmail'),
            array('db' => 'pu.email', 'dt' => 2, 'field' => 'email'),
            array('db' => 'sc.cat_name', 'dt' => 3, 'field' => 'cat_name'),
            array('db' => 'pu.gender', 'dt' => 4, 'field' => 'gender'),
            array('db' => 'bookings.booking_date', 'dt' => 5, 'field' => 'booking_date'),
            array('db' => 'vs.start_time', 'dt' => 6, 'field' => 'start_time'),
            //array('db' => 'tr.currency', 'dt' => 6, 'field' => 'currency'),
            array('db' => "CONCAT(tr.currency, ' ', bookings.price )", 'as' => 'amount', 'dt' => 7, 'field' => 'amount'),
            array('db' => 'bookings.created_at', 'dt' => 8, 'field' => 'created_at'),
            array('db' => 'bookings.status', 'dt' => 9, 'field' => 'status', 'formatter' => function($d,$row){
                if($row['status'] == 1){
                    return '<span class="label label-warning">'.trans('doctor/booking.active').'</span>';
                }
                elseif($row['status'] == 2){
                    return '<span class="label label-danger">'.trans('doctor/booking.cancel').'</span>';
                }else{
                    return '<span class="label label-success">'.trans('doctor/booking.complete').'</span>';
                }
            }),
            array('db' => 'bookings.id', 'dt' => 10, 'formatter' => function( $d, $row ) {
                    if($row['status'] == 1){
                        $operation = '<a href="javascript:;" id="' . $d . '" class="btn btn-danger delete-btn" title="' . trans('admin/common.delete') . '" data-toggle="tooltip"><span class="fa fa-times"></span></a>';
                    }else{
                        $operation = '';
                    }
                    return $operation;
                }, 'field' => 'id')
        );

        $sql_details = array(
            'user' => config('database.connections.mysql.username'),
            'pass' => config('database.connections.mysql.password'),
            'db' => config('database.connections.mysql.database'),
            'host' => config('database.connections.mysql.host')
        );
  
        $joinQuery = '';
        $joinQuery .= " LEFT JOIN users vu ON vu.id = bookings.vender_id";
        $joinQuery .= " LEFT JOIN users pu ON pu.id = bookings.user_id";
        $joinQuery .= " LEFT JOIN service_categories sc ON bookings.service_id = sc.id";
        $joinQuery .= " LEFT JOIN vender_slots vs ON vs.id = bookings.vendor_slot_id";
        $joinQuery .= " LEFT JOIN transactions tr ON tr.booking_id = bookings.id";
        if($doctorid=="No Value")
            $extraWhere = "";
        else
            $extraWhere = "bookings.vender_id=".$doctorid; //"r.name='doctor'";
        $groupBy = "";//"users.id";

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
        );
    }


    /**
     * Function to show refund page 
     * @param View
     * @author Durga Parshad
     * @since 02-Jan-2020
     */
    public function refund(){
        return view('admin.refund');
    }


    /**
     * Function to get refund data table 
     * @param 
     * @author Durga Parshad
     * @since 02-Jan-2020
     */
    public function refundData(){
        try{
            $table = 'booking_refund_request';
            $primaryKey = 'id';
            $columns = array( 
                array('db' => 'bookings.reference_id', 'dt' => 0, 'field' => 'reference_id'), 
                array('db' => 'vu.email', 'as' => 'vendorEmail', 'dt' => 2, 'field' => 'vendorEmail'),
                array('db' => 'pu.email', 'dt' => 1, 'field' => 'email'),
                array('db' => 'booking_refund_request.amount', 'dt' => 3, 'field' => 'amount'),
                array('db' => 'booking_refund_request.currency', 'dt' => 4, 'field' => 'currency'),
                array('db' => 'booking_refund_request.note', 'dt' => 5, 'field' => 'note'),
                array('db' => 'booking_refund_request.status', 'dt' => 6, 'field' => 'status', 'formatter' => function($d,$row){
                    if($row['status'] == 1){
                        return '<span class="label label-warning">'.trans('admin/refund.pending').'</span>';
                    }
                    elseif($row['status'] == 3){
                        return '<span class="label label-danger">'.trans('admin/refund.rejected').'</span>';
                    }else{
                        return '<span class="label label-success">'.trans('admin/refund.accepted').'</span>';
                    }
                }),
                array('db' => 'bookings.id', 'dt' => 7, 'formatter' => function( $d, $row ) {
                        if($row['status'] == 1){
                            $operation = ' <a href="#" class="btn btn-primary d-inline-block accepted" data-id = "'. $d .'"title="' . trans('admin/refund.accepted') . '" data-toggle="tooltip"><span class="fa fa-check"></span></a> <a href="#" data-id="' . $d . '" class="btn btn-danger delete-btn" title="' . trans('admin/refund.rejected') . '" data-toggle="tooltip"><span class="fa fa-times"></span></a>';
                            return $operation;
                        }
                        
                    }, 'field' => 'id'),
                array('db' => 'booking_refund_request.id', 'dt' => 8, 'field' => 'id')
            );

            $sql_details = array(
                'user' => config('database.connections.mysql.username'),
                'pass' => config('database.connections.mysql.password'),
                'db' => config('database.connections.mysql.database'),
                'host' => config('database.connections.mysql.host')
            );

            $joinQuery = '';
            $joinQuery .= 'LEFT JOIN bookings ON bookings.id = booking_refund_request.booking_id';
            $joinQuery .= " LEFT JOIN users vu ON vu.id = bookings.vender_id";
            $joinQuery .= " LEFT JOIN users pu ON pu.id = bookings.user_id";  
        

            $extraWhere = ""; //"r.name='doctor'";
            $groupBy = "";//"users.id";

        

            echo json_encode(
                    SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
            );
        }
        catch (\Exception $ex) {

            $this->response['message'] = $ex->getMessage();
            return response($this->response, 500);
        }
    }


    /**
     * Function refund send on stripe
     * @param '{id}'
     * @author Durga Parshad
     * @since 03-Jan-2020
     */
    public function refundStripe(RefundStripe $request){
        try{
            Stripe::setApiKey(env("STRIPE_SECRET"));
            $bookingRefund = BookingRefund::find($request['id']);
            $transaction = Transaction::where('booking_id',$bookingRefund->booking_id)->first();
            
            if($request['req_type'] == 1){
                
                $stripeResult = \Stripe\Refund::create([
                    'charge' => $transaction->trans_id,
                ]);

                if($stripeResult->status == "succeeded" && $stripeResult->id){

                    $bookingRefund->status = 2 ; 
                    $bookingRefund->save();

                    $transaction->status = 4;
                    $transaction->save();

                    $this->response['status'] = 1;
                    $this->response['message'] = trans('admin/refund.refund_success');
                    
                    $this->response['data'] = $stripeResult;
                    return response()->json($this->response, 200);
                }
            }
            else{

                $booking = Booking::find($bookingRefund->booking_id);
                $booking->status = 1;
                $booking->save();

                $bookingRefund->status = 3 ; 
                $bookingRefund->save();

                $this->response['status'] = 1;
                $this->response['message'] = trans('admin/refund.refund_reject');
                
                $this->response['data'] = 'sfas';
                return response()->json($this->response, 200);
            }            

        }
        catch (\Exception $ex) {

            $this->response['message'] = $ex->getMessage();
            return response($this->response, 500);
        }
    }
}
