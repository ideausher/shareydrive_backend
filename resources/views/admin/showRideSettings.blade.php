@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/generalSettings.settings') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Ride Related Settings</h1>
        <!-- <ol class="breadcrumb">
            <li><a href="/admin"><span class="fa fa-dashboard"></span> {!! trans('admin/common.home') !!}</a></li>
            <li class="active">{!! trans('admin/generalSettings.settings') !!}</li>
        </ol> -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
                <!-- ./ notifications -->
            </div>
            <div class="col-xs-12">
                <div class="box">
                @if(isset($data))
                    {!! Form::model($data, array('url' => 'admin/rideSettings', 'method' => 'POST', 'id' => 'updateBrand-form', 'files' => true )) !!}
                 
                    @else
                    {!! Form::open(array('url' => 'admin/addBrand', 'id' => 'addBrand-form','method'=>'POST', 'files' => true)) !!}
                @endif    
                {!! Form::open(array('method' => 'POST','url' => 'admin/rideSettings', 'id' => 'referralForm')) !!}
                     
                
                    <div class="box-body">
                    <div class="form-group has-feedback">
                            {!! Form::label('sug_price_value', 'Suggested Price  : (Per KM.)') !!}
                            {!! Form::number('sug_price_value', old('sug_price_value'),array('class'=>'form-control','step' => '.01' )) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            {!! Form::label('ride_time_diff_search', 'Time Difference While Searching For Rides : (Hours)') !!}
                            {!! Form::number('ride_time_diff_search', old('ride_time_diff_search'),array('class'=>'form-control' )) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            {!! Form::label('before_pass_cancel_time', 'Time Before a passenger can cancel Ride Offered : (Minutes)') !!}
                            {!! Form::number('before_pass_cancel_time', old('before_pass_cancel_time'),array('class'=>'form-control' )) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            {!! Form::label('before_driv_cancel_time', 'Time Before a Driver can cancel Ride Requested : (Minutes) ') !!}
                            {!! Form::number('before_driv_cancel_time', old('before_driv_cancel_time'),array('class'=>'form-control' )) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                    <div class="box-footer">
                        {!! Form::submit('Save',array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}
                        <a href="{!! URL::route('settings.index') !!}" class="btn btn-default">{!! trans('Cancel') !!}</a>
                    </div>    
                    {!! Form::close()!!}
                </div> <!-- /.box -->
            </div> <!-- /.col-xs-12 -->
        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@section('scripts')
 
@stop