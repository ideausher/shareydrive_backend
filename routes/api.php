<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Http\Request;

Route::prefix('v1')->group(function () {
    Route::post('user','API\v1\UsersController@register'); 
    Route::post('login', 'API\v1\UsersController@login'); 
    Route::post('sociallogin', 'API\v1\UsersController@socialLogin');
    Route::post('checkotp', 'API\v1\UsersController@checkOtp');
    Route::post('sendotp', 'API\v1\UsersController@sendOtp'); 
    Route::post('checkPhoneOtp', 'API\v1\UsersController@checkPhoneOtp');
    Route::post('forgetpassword', 'API\v1\UsersController@forgetPassword'); 
    Route::get('term_condition', 'API\v1\SettingController@termsandcondition'); 
    Route::put('updateforgetpassword', 'API\v1\UsersController@updateForgetPassword');

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api']], function() {
    Route::prefix('v1')->group(function () {
        Route::post('offerRide','API\v1\RideController@createRide');
        Route::get('getRides','API\v1\RideController@getRides');
        Route::get('getUserRides','API\v1\RideController@getUserRides');
        Route::put('updatepassword', 'API\v1\UsersController@updatePassword');
        Route::get('logout', 'API\v1\UsersController@logout');
        Route::put('user', 'API\v1\UsersController@editUser');
        
        Route::post('editcheckPhoneOtp', 'API\v1\UsersController@checkPhoneOtp');
        Route::get('user', 'API\v1\UsersController@getUsers');
        Route::post('uploadImage', 'API\v1\UsersController@uploadImage');    
        Route::post('vehicle','API\v1\VehicleController@create');
        Route::post('ride/payment','API\v1\RideController@rideAccept');
        Route::post('review','API\v1\RideController@createReview');
        Route::post('getintouch','API\v1\HelpAndSupportController@getintouch');
        Route::get('card', 'API\v1\CardController@getCard');
        Route::post('bank','API\v1\CardController@saveBank');
        Route::get('bank','API\v1\CardController@getBank');
        Route::get('getRideById', 'API\v1\RideController@getRideById');
        Route::get('notification', 'API\v1\NotificationController@getNotifications');
        Route::patch('notification', 'API\v1\NotificationController@updateStatus');
        Route::get('/vehicle', 'API\v1\VehicleController@getVehicle');
        Route::get('bookingDetails', 'API\v1\RideController@bookingDetails');
        Route::post('/contact_us', 'API\v1\SettingController@contactUs');
        Route::get('getUserDetailsById', 'API\v1\UsersController@getUserDetails');
        Route::get('getRideRequest', 'API\v1\RideController@getRideRequest');
        Route::post('acceptOrRejectRideRequest','API\v1\RideController@acceptOrRejectRideRequest');
        Route::post('sendBookingRequest','API\v1\RideController@sendBookingRequest');
        Route::post('ride/completeRide','API\v1\RideController@completeRide');
        Route::post('ride/driver/changeStatus','API\v1\RideController@driverchangeridestatus');
        Route::post('ride/passenger/changeStatus','API\v1\RideController@passengerchangeridestatus');
        Route::get('ride/passengers','API\v1\RideController@getPassengers');
        Route::post('sendNotiAPI', 'API\v1\NotificationController@sendNotiAPI');
        Route::post('hitCustomNotification','API\v1\RideController@hitCustomNotification');
    });

});



