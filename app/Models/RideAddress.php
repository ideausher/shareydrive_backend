<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RideAddress extends Model
{
    protected $table = 'ride_address';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ride_date_time','max_no_seats','price_per_seat','total_toll_tax','latitude','longitude','country','place_id','city','state','full_address','ride_id','type',
    ];
}