<?php

namespace App\Services;

use App\Models\DeviceDetails;
Use Illuminate\Support\Facades\Auth;

class PushNotification {

    public function __construct() {
        
    }

    private function androidPushNotifiction($message, $title, $deviceToken, $moreData = []) {
        $registrationIds = (is_array($deviceToken)) ? $deviceToken : ["$deviceToken"];
        
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $data = [
            'title' => $title,
            'body' => $message,
        ];
        
        $fcmNotification = [
            'registration_ids' => $registrationIds, //multple token array
            "data" => $moreData,
            "notification" => $data,
        ];
 
            //'Authorization: key='.config('services.fcm_token'),
            
            // $headers = [
            //     'Authorization: key=AAAAKOvdvvk:APA91bFocMsBUXiuDTYMRgpcbQhqEx08ZravftERO1YthZ22MU-WAvE56k6wm1oJUYPM5TiosGbNWDWdExZc7f5yLtQxg1RqF5I_nfpXbIl7gTjVwzJR7_lEbKguYggDXyO7xTOp8WOb',
            //     'Content-Type: application/json',
            //     'Accept: application/json'
            // ];
        $headers = [
            'Authorization: key=AAAAJ09mspc:APA91bFPPBp2zWazSCOW2aBwbeDOUDUi3IOuW_CXo2El6FUZpdXEM6vrHSYuLnQuxfQ3XX8r-AW8YxgSlIMmimLtPR4YyP81JI8rBuhSK9A8I51t5N1c181dlOblzUyFnugd1XDdKpjP',
            'Content-Type: application/json',
            'Accept: application/json'
        ];
        
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        // $result = curl_exec($ch);
        // if ($result) {
        //     return TRUE;
        // } else {
        //     return FALSE;
        // }
    }

    private function iphonePushNotifiction($message, $title, $deviceToken, $moreData = []) {
        $registrationIds = (is_array($deviceToken)) ? $deviceToken : ["$deviceToken"];
        
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $data = [
            'title' => $title,
            'body' => $message,
            'priority' => '"high"',
        ];
        
        $fcmNotification = [
            'registration_ids' => $registrationIds, //multple token array
            "data" => $moreData,
            "notification" => $data,
        ];
 

        $headers = [
            'Authorization: key=' . config('services.fcm_token'),
            'Content-Type: application/json',
            'Accept: application/json'
        ];
        
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        // $result = curl_exec($ch);
        // if ($result) {
        //     return TRUE;
        // } else {
        //     return FALSE;
        // }
    }

    public function sendNotification($data, $userId) {
        
        $androidDeviceTokens = DeviceDetails::where('user_id', $userId)->AndroidTokens()->get()->toArray();
        $iphoneTokens = DeviceDetails::where('user_id', $userId)->IosToken()->get()->toArray();
        if (!empty($androidDeviceTokens)) {
            $this->androidPushNotifiction($data['message'], $data['title'], array_column($androidDeviceTokens, 'device_token'), $data['data']);
        }
        if (!empty($iphoneTokens)) {
            $this->iphonePushNotifiction($data['message'], $data['title'], array_column($iphoneTokens, 'device_token'), $data['data']);
        }
    }

}
