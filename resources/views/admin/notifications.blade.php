@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/notification.list') !!}
@stop
@section('styles')
<link href="{!! asset('assets/admin/plugins/bootstrap3-editable/css/bootstrap-editable.css') !!}" rel="stylesheet"
      type="text/css" />
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{!! trans('admin/notification.list') !!}</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
                <!-- ./ notifications -->
            </div>
            <div class="col-xs-12">
                <?php
                ?>

                <div class="box">
                    <div class="row">
                        
                        <div class='col-sm-6'>
                            <div class="show-list">
                            </div>
                        </div>
                    </div>
                    <div class="box-body table-responsive">
                        <table id="user_list" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>{!! trans('Sender ID') !!}</th>
                                    <th>{!! trans('Receiver ID') !!}</th>
                                    <th>{!! trans('Title') !!}</th>
                                    <th>{!! trans('Message') !!}</th>                            
                                </tr>
                            </thead>
                            <tbody>
                                @if($notifications->count() > 0)
                                @foreach($notifications as $notification)
                                <tr>
                                    <td>{{ $notification->user_id_one }}</td>
                                    <td>{{ $notification->user_id_two}}</td>
                                    <td>{{ $notification->title}}</td>
                                    <td>{{ $notification->message}}</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td>No Record found.</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if($type=='all')
                        <div class="box-footer clearfix">
                            <div class="col-md-12 text-center pagination pagination-sm no-margin">
                                @if($notifications)
                                {!! $notifications->render() !!}
                                @endif
                            </div>
                            <div class="col-md-12 text-center">
                                <a class="btn">{!! trans('admin/common.total') !!} {!! $notifications->total() !!} </a>
                            </div>
                        </div><!-- /. box-footer -->
                        @endif
                    </div> <!-- /. box body -->
                </div> <!-- /.box -->
            </div> <!-- /.col-xs-12 -->
        </div>
     

        <!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
@section('scripts')
<script>

    $(document).on('click', '.patient-window', function () {


    });
    /*   var oTable;
     
     oTable = $('#user_list1').DataTable({
     "dom": "<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'l><'col-xs-12 col-sm-4 col-md-4 col-lg-4'r><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'f>>t<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'i><'col-xs-12 col-sm-4 col-md-4 col-lg-4'><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'p>>",
     "processing": true,
     "serverSide": true,
     "ajax": "{!! url('admin/notification/doctors') !!}",
     
     
     "language": {
     "emptyTable": "{!! trans('admin/common.datatable.empty_table') !!}",
     "info": "{!! trans('admin/common.datatable.info') !!}",
     "infoEmpty": "{!! trans('admin/common.datatable.info_empty') !!}",
     "infoFiltered": "({!! trans('admin/common.datatable.info_filtered') !!})",
     "lengthMenu": "{!! trans('admin/common.datatable.length_menu') !!}",
     "loadingRecords": "{!! trans('admin/common.datatable.loading') !!}",
     "processing": "{!! trans('admin/common.datatable.processing') !!}",
     "search": "{!! trans('admin/common.datatable.search') !!}:",
     "zeroRecords": "{!! trans('admin/common.datatable.zero_records') !!}",
     
     }
     });  
     }); */
</script>
@stop