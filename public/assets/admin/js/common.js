// When the browser is ready...
$(function () {
    //start code jQuery Form Validation code

    /* author: dhaval
     * Description: url validaion*/
    
    jQuery.validator.addMethod("url", function (value, element) {
        return this.optional(element) || /^((https?|s?ftp):\/\/)?(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
    }, "Please enter a valid URL.");
    
    jQuery.validator.addMethod("urlsuffix", function (value, element) {
        //return this.optional(element) || /(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,63}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?/i.test(value);
        var indexNumber = value.lastIndexOf(".");
        var str = value.substring(indexNumber);
        var len = str.length;
        if(len > 2)
           return true;
    }, "Please enter a valid URL.");

    jQuery.validator.addMethod("greaterThan",
            function (value, element, params) {
                if (!/Invalid|NaN/.test(new Date(value))) {
                    return new Date(value) >= new Date($(params).val());
                }
                return isNaN(value) && isNaN($(params).val())
                        || (Number(value) > Number($(params).val()));
            }, 'Must be greater than {0}.');
 
    /* Description: greateThan {0} amount*/
    $.validator.addMethod('minAmount', function (value, el, param) {
        return value > param;
    }, 'Must be greater than {0}.');

    /* Description: lessThan {0} filesize*/
    $.validator.addMethod("filsize", function (value, element, param) {
        if (typeof element.files[0] != "undefined") {
            var size = element.files[0].size;
            return (size / 1024) < param;
        } else {
            return true;
        }
    }, "Maximum allowed filesize {0} KB");
    
    //reset form remove highlight
    $.validator.prototype.resetForm = function () {
        if ($.fn.resetForm) {
            $(this.currentForm).resetForm();
        }
        this.submitted = {};
        this.lastElement = null;
        this.prepareForm();
        this.hideErrors();
        var elements = this.elements().removeData("previousValue").removeAttr("aria-invalid");
        if (this.settings.removehighlight) {
            for (var i = 0; elements[i]; i++) {
                this.settings.removehighlight.call(this, elements[i], this.settings.errorClass, this.settings.validClass);
            }
        }
    }

    $(document).on('click', '.modal-window-patient', function () {
      
        $('.js-patient-basic-multiple').select2();
    });
    
    $(document).on('click', '.modal-window-doctor', function () {
        
        $('.js-doctor-basic-multiple').select2();
    });
    //end code

    $("#login-form").validate({
        // Specify the validation rules
        rules: {
            username: {
                required: true
            },
            password: {
                required: true
            }
        },
        // Specify the validation error messages
        messages: {
            /*username: "Please enter your username",
            password: {
                required: "Please enter a password",
            }*/
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }

    });

$("#makenewpassword").validate({
        // Specify the validation rules
        rules: {
            password: {
                required: true,
               
                minlength: 6
            },
            c_password: {
                required: true,
                equalTo: '#password'
            }
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }

    });

    $("#password-form").validate({
        // Specify the validation rules
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }

    });
    
    $("#reset-password-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                nowhitespace: true,
                minlength: 6
            },
            password_confirmation: {
                required: true,
                equalTo: '#password'
            }
        },
        submitHandler: function (form) {
            
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove glyphicon-lock').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $("#change-password-form").validate({
        // Specify the validation rules
        rules: {
            old_password: {
                required: true
            },
            password: {
                required: true,
                nowhitespace: true,
                minlength: 5
            },
            password_confirmation: {
                required: true,
                equalTo: '#password'
            }
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }

    });

    $("#profile-form").validate({
        // Specify the validation rules
        rules: {
            email: {
                required: true,
                email: true
            },
            firstname: {
                required: true
            },
            lastname: {
                required: true
            }
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }

    });
    
    $("#setting-form").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            language: {
                required: true  
            },
            site_title: {
                required: true
            },
            address: {
                required: true
            },
            email: {
                required: true
            },
            phone: {
                required: true
            },
            facebook: {
                url: true
            },
            linkedin: {
                url: true
            },
            twitter: {
                url: true
            },
            googleplus: {
                url: true
            },
            logo: {
                required: function(){
                    return $("#setting_id").val() == 0;
                },
                extension: "jpe?g|png|gif|bmp",
                filsize: 3072
            }
        },
        // Specify the validation error messages
        messages: {
            logo: {
                extension: "Upload only jpeg, jpg, png, gif, bmp"
            },
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    

    

    
    
     $("#patient-notification").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            
           
            "id[]": {
                required:true
            }
            
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    $("#doctor-notification").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            
           
            "id[]": {
                required:true
            }
            
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $("#service-category-form").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            cat_name: {
                required: true
            },
            cat_name_es: {
                required: true
            },
            cat_name_ar: {
                required: true
            },
            image: {
                required: true,
                extension: "jpe?g|png|gif|bmp",
                filsize: 3072
            }
           
        },
        // Specify the validation error messages
        messages: {
            image: {
                extension: "Upload only jpeg, jpg, png, gif, bmp"
            },
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    
        $("#edit-service-category-form").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            cat_name: {
                required: true
            },
            cat_name_es: {
                required: true
            },
            cat_name_ar: {
                required: true
            }
          
           
        },
  
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

    var regx = /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]*$/;
    $("#add-vendor-form").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            first_name: {
                required: true,
                minlength:3,
                pattern: regx
            },
            last_name: {
                required: true,
                minlength:3,
                pattern: regx
            },
            password: {
                required: true,
                nowhitespace: true,
                minlength: 5
                
            },
            email: {
                required: true,
                 email: true
            },
            confirm_password: {
                required: true,
                equalTo: '#password' 
            },
            phone: {
                required:true,
                number:true,
                minlength:5
            },
            gender:{
                required:true
            },
            address: {
                required:true
            },
            city: {
                required:true,
                pattern: regx
            },
             state: {
                required:true,
                pattern: regx
            },
            country: {
                required:true,
                pattern: regx
            },
            postal_code: {
               required:true,
               number:true
            },
            experience: {
                required:true,
                number:true
            },
            price: {
                required:true,
                integer:true,
                min:1
            },
            specialization: {
                required:true
            },
            
           "degree[]": {
                required:true
            },
            "batch[]": {
                required:true
            },
            "edu_desc[]": {
                required:true
            },
          
            image: {
                required: function(){
                    return $("#category_id").val() == 0;
                },
                extension: "jpe?g|png|gif|bmp",
                filsize: 3072
            }
        },
        // Specify the validation error messages
        messages: {
            image: {
                extension: "Upload only jpeg, jpg, png, gif, bmp"
            },
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        },
        onkeyup: function(element, event) {
            this.element(element); // triggers validation
        }
    });
   ///////////////////////////////////
    //Validating  Vendor form code ends here
    
    $("#edit-vendor-form").validate({
        
        ignore: [],
        // Specify the validation rules
        rules: {
            
            firstname: {
                required: true,
                pattern: regx  
            },
            lastname: {
                required: true,
                pattern: regx
            },
            email: {
                required: true,
                 email: true
            },
            phone: {
                required:true,
                number:true,
                minlength:5
            },
            gender:{
                required:true
            },
             address: {
                required:true
            },
            city: {
                required:true
            },
             state: {
                required:true
            },
             country: {
                required:true
            },
           postal_code: {
               required:true
            },
            experience: {
                required:true
            },
            price: {
                required:true
            },
            specialization: {
                required:true
            },
            
           "degree[]": {
                required:true
            },
            "batch[]": {
                required:true
            },
            "edu_desc[]": {
                required:true
            },

        },
        // Specify the validation error messages
        messages: {
            image: {
                extension: "Upload only jpeg, jpg, png, gif, bmp"
            },
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });

    $("#service-form").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            title: {
                required: true
            },
            price: {
                required: true
            },
            duration: {
                required: true
            },
            close_booking_before_time: {
                required: true
            },
            start_date: {
                required: true
            },
            end_date: {
                required: true
            },
            start_time: {
                required: true
            },
            end_time: {
                required: true
            }
        },
        // Specify the validation error messages
        messages: {
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $("#paypalsetting-form").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            mode: {
                required: true
            },
            client_id_sandbox: {
                required: true
            },
            secret_sandbox: {
                required: true
            },
            client_id_live: {
                required: true
            },
            secret_live: {
                required: true
            }
        },
        // Specify the validation error messages
        messages: {
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $("#paymentsetting-form").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            currency_id: {
                required: true
            },
            price: {
                required: true,
                number: true
            },
            commission:
            {
                required: true,
                number: true,
                max: 100
            }
        },
        // Specify the validation error messages
        messages: {
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $("#currency-form").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            name: {
                required: true
            },
            code: {
                required: true,
            },
            language_code:
            {
                required: true
            },
            country_code:
            {
                required:true
            },
            currency_symbol:
            {
                required:true
            }
        },
        // Specify the validation error messages
        messages: {
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $("#coupon-form").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            name: {
                required: true
            },
            code: {
                required: true,
            },
            type:
            {
                required: true
            },
            discount:
            {
                required:true,
                number: true,
                min: 0
            },
            minAmount:
            {
                required:true,
                number: true,
                min: 0
            },
            maxDiscountAmount:
            {
                required:true,
                number: true,
                min: 0
            },
            startDateTime:
            {
                required:true,
            },
            endDateTime:
            {
                required:true,

            },
            maxUseCustomer:{
                number:true,
            },
            maxTotalUse: {
                number:true,
            }
        },
        // Specify the validation error messages
        messages: {
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $("#banner-form").validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            name: {
                required: true
            }
        },
        // Specify the validation error messages
        messages: {
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    
    vender_validation('add-vendor-form');
    
    $(document).on('click', '.btnadd', function () {

    var classname = $(this).attr('data-main-class');
    
    var row = parseInt($(".btnremove").length)+1;


    if (row <= 5) {
      
        dynamic_field(row,classname);
    } else {
        // alert -> 'You can add only 5 degrees.'
        // activate alert box if you need it (Rahul Kewat)
    }
    //removing class
    for($i=0;$i<$(".btnadd").length-1;$i++)
    { 
        $(".btnadd").eq($i).addClass("hidden")
    }

   
});

    
 $(document).on('click', '.btnremove', function () {
    
        var row = parseInt($(".btnremove").length);

        if (row  > 1) {

            $('.row-'+row).remove();
            button_html = '<button type="button" data-row="'+(row-1)+'" data-main-class="'+$(this).attr('data-main-class')+'" class="btn btn-success add">Add</button>';
            
            vender_validation('add-vendor-form');
        } else {
            // alert -> 'You cant delete first row'
            // activate alert box if you need it (Rahul Kewat)
        }

       $i=$(".btnadd").length-1;
  
        $(".btnadd").eq($i).removeClass("hidden")


        

});

//  $(document).on('click', '.add-vendor', function () {
   
//     var classname = $(this).attr('data-main-class');
    
//     var row = parseInt($(this).attr('data-row'))+1;
    
//     if (row <= 5) {
      
//         dynamic_field(row,classname);
//         vender_validation('add-vendor-form');
//     } else {
   
//         alert('You can add only 5 degrees.');
//     }
   
// });

    
//  $(document).on('click', '.remove-vendor', function () {
    
//          var row = parseInt($(this).attr('data-row'));

//         if (row  > 1) {

//             $('.row-'+row).remove();

//             button_html = '<button type="button" data-row="'+(row-1)+'" data-main-class="'+$(this).attr('data-main-class')+'" class="btn btn-success add">Add</button>';

//             $(".row-"+(row-1)).find('.buttons').children('.form-group').prepend(button_html);
            
//             vender_validation('add-vendor-form');
//         } else {
//             alert('You cant delete first row');
//         }

// });

    $("#change-image").change(function () {
        var view_id = $(this).attr('data-view');
        readURL(this,view_id);
    });
    
});


    function vender_validation(form_id,type)
    {
        
          $("#"+form_id).validate({
        ignore: [],
        // Specify the validation rules
        rules: {
            first_name: {
                required: true  
            },
            last_name: {
                required: true
            },
            password: {
                required: true,
                nowhitespace: true,
                minlength: 5
                
            },
            email: {
                required: true,
                 email: true
            },
            confirm_password: {
                required: true,
                equalTo: '#password' 
            },
            phone: {
                required:true,
                number:true,
                minlength:5
            },
            gender:{
                required:true
            },
            address: {
                required:true
            },
            city: {
                required:true
            },
             state: {
                required:true
            },
            country: {
                required:true
            },
            postal_code: {
               required:true
            },
            experience: {
                required:true
            },
            price: {
                required:true,
                integer:true,
                min:1
            },
            specialization: {
                required:true
            },
            
           "degree[]": {
                required:true,
            },
            "batch[]": {
                required:true
            },
            "edu_desc[]": {
                required:true
            },
          
            image: {
                required: function(){
                    return $("#category_id").val() == 0;
                },
                extension: "jpe?g|png|gif|bmp",
                filsize: 3072
            }
        },
        // Specify the validation error messages
        messages: {
            image: {
                extension: "Upload only jpeg, jpg, png, gif, bmp"
            },
        },
        submitHandler: function (form) {
            form.submit();
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.closest('.form-group').find('.cke').length) {
                error.appendTo(element.closest('.form-group'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    }
   
function dynamic_field(number,classname){
    
    var html = '<div  class ="col-xs-12 row-' + number + '">' + $(".row-" + ((number>1)?number-1:1)).html() + '</div>';
    $('.'+classname).append(html);
    
    
    $(".row-" + (number)).find('input[type=text]').val('');
    
    $(".row-" + (number)).find('.has-success').removeClass('has-success');

    $(".row-" + (number)).find('.glyphicon-ok').removeClass('glyphicon-ok');
    $(".row-" + (number)).find('textarea').val('');
    $(".row-" + (number)).find('button').attr('data-row',number);
    
    
    
   
    vender_validation('add-vendor-form');
}



function initMap() {


    var input = document.getElementById('address');


    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', function () {

    var place = autocomplete.getPlace();
    
    var details = ["postal_code", "city", "country"];
    if(place.address_components){
        
        
        
    }

    });
}

function readURL(input,view_id) {
    var name = input.files[0].name;
    var Extension = name.substring(name.lastIndexOf('.') + 1).toLowerCase();
    var valid_extension = ['gif','png','bmp','jpeg','jpg'];
    
    if (input.files && input.files[0] && valid_extension.includes(Extension)) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+view_id).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }else{
        var default_image = $('#'+view_id).attr('data-src');
        $('#'+view_id).attr('src',default_image);
        $("#change-image").val('');
        // alert -> 'please upload valid image'
        // activate alert box if you need it (Rahul Kewat)
    }
}

function saveToken(token){
    
    if(token){
        
        $.ajax({
            type: "POST",
            url: url+'/doctor/fcm/savetoken',
              data: {
                fcm_token: token,
                _token: csrf_token,
            },
            success: function(response){
              
            }
        });
        
    }
 
    

}