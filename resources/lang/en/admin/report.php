<?php
return [

    /*
    |--------------------------------------------------------------------------
    | vendor report Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the admin sidebar links.
    | You are free to change them to anything you want to customize 
    | your views to better match your application.
    |
    */
    "appointment_report" => "Appointment Report",
    "name" => "Name",
    "date" => "Date",
    "issue" => "Issue",
    "report" => "Report",
    "submit" => "Generate"

];