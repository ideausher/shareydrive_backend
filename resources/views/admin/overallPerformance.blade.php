@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: Users List
@stop
@section('styles')
<link href="{!! asset('assets/admin/plugins/bootstrap3-editable/css/bootstrap-editable.css') !!}" rel="stylesheet"
  type="text/css" />
<style>
.credit-txt {
  cursor: pointer;
}
</style>
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h4>{!! trans('Overall Performance') !!} </h4>


        <!-- <ol class="breadcrumb">
                <li><a href="/admin"><span class="fa fa-dashboard"></span> {!! trans('admin/common.home') !!}</a></li>
                <li class="active">{!! trans('admin/service.services') !!}</li>
            </ol> -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
       
       <div class="row box" style="margin-left:0;">
            <div class="col-md-4">
                <div class="dbox dbox--color-3">			
                    <div class="dbox__body">
                        <span class="dbox__count"> {{$totalUser}}</span>
                        <span class="dbox__title">Total Users</span>
                    </div>		
                </div>
            </div> 
            <div class="col-md-4">
                <div class="dbox dbox--color-3">			
                    <div class="dbox__body">
                        <span class="dbox__count"> {{$totalEarnings}} </span>
                        <span class="dbox__title">Total Earnings</span>
                    </div>		
                </div>
            </div> 
            <div class="col-md-4">
                <div class="dbox dbox--color-3">			
                    <div class="dbox__body">
                        <span class="dbox__count"> {{$totalRideOffered}} </span>
                        <span class="dbox__title">Total Rides Offered</span>
                    </div>		
                </div>
            </div> 
            <div class="col-md-4">
                <div class="dbox dbox--color-3">			
                    <div class="dbox__body">
                        <span class="dbox__count">  {{$totalPassengerTraveled}} </span>
                        <span class="dbox__title">Total Passengers Travelled</span>
                    </div>		
                </div>
            </div> 
            <div class="col-md-4">
                <div class="dbox dbox--color-3">			
                    <div class="dbox__body">
                        <span class="dbox__count">  {{$totalReferalCode}} </span>
                        <span class="dbox__title">Total Referrals Used</span>
                    </div>		
                </div>
            </div> 
     </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop