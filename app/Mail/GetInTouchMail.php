<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GetInTouchMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $Emaildata = array();
    public function __construct($Emaildata)
    {
        $this->Emaildata =$Emaildata;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $Emaildata = $this->Emaildata;
        
        return $this->view('emails.GetInTouchMailView',compact('Emaildata'));
    }
    // public function build()
    // {
    //     return $this->view('view.emails.GetInTouchMailView');
    // }
}
