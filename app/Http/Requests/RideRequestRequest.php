<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class RideRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'ride_date_time.*' => 'required|date_format:Y-m-d H:i:s',
                'gender'=>'required|integer|between:0,2',
                'max_no_seats' => 'required|integer',
                'price_per_seat' => 'required',
                'total_toll_tax' => 'required',
                'start_latitude' => 'required',
                'start_longitude' => 'required',
                'start_country' => 'required|string',
                'start_place_id' => 'required|string',
                'start_city' => 'required|string',
                'start_state' => 'required|string',
                'start_full_address' => 'required|string',
                'end_latitude' => 'required',
                'end_longitude' => 'required',
                'end_country' => 'required|string',
                'end_place_id' => 'required|string',
                'end_city' => 'required|string',
                'end_state' => 'required|string',
                'end_full_address' => 'required|string'
            ];

    }

    /**
     * Overwrite Validation error response
     * 
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([ 
            'status' => 0, 
            'message' => $validator->errors()->first(),
            'errors' => $validator->errors()
        ], 422);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
