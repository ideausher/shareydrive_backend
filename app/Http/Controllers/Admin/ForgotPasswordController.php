<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use Devrahul\Signinupapi\Models\phoneOtp;
use Aloha;
use App\User;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \Config::set("auth.defaults.passwords","admin");
        $this->middleware('guest.admin');
    }
    
    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('admin.forgotPassword');
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetOtp(Request $request)
    {
		$user= User::where('phone_number',$request['phone_number'])->first();
		if($user)
		{
			if($user->hasRole('admin'))
			{
				//$phone= $user->phone_number;
			
		 $phone = "+". $user['phone_country_code'] . $user['phone_number'];
            $otp = rand(1000, 9999);
            $twilio = new Aloha\Twilio\Twilio(env('TWILIO_SID'), env('TWILIO_TOKEN'), env('TWILIO_SMS_FROM_NUMBER'));
        try{
			
            if ($twilio->message($phone, 'Otp is ' . $otp)) {
                $user_otp = phoneOtp::where('phone_no', $user['phone_number'])->first();
                
                if ($user_otp) {
                    $user_otp->update(['otp' => $otp,'no_of_attempts'=> 0,'is_verified'=>0]);
                    $id = $user_otp->id;

                } else {
                    $phoneOtp = phoneOtp::create(['phone_no' => $user['phone_number'], 'otp' => $otp,'phone_country_code' =>  $user['phone_country_code']]);                  $id = $phoneOtp->id;  
                }
           $data['user']=$user;
       return view('admin/verifyForgotOtp',$data);
		 }else{
				
				return redirect('admin/')->with('error_message', 'User Not Registered for otp')->withInput();  
				
			}
			}
			catch (\Exception $ex) {
                
            $message = $ex->getMessage();
			auth()->guard('admin')->logout();
			return redirect('admin')->with('error_message', $message)->withInput();
			}
          }
		  return redirect('admin')->with('error_message', "user is not admin")->withInput();
		}
		return redirect('admin')->with('error_message', "User not found")->withInput();
		
		}
		
		public function verifyOtp(Request $request)
		{
			$data['user']=User::where('id',$request['id'])->first();
	   
	   if($data['user']){
	   $user_phone=$data['user']->phone_number;
	   $otpAttempt=phoneOtp::where('phone_no',$user_phone)->first();
	    //echo "<pre>";print_r($user);exit;
	   if($otpAttempt)
	   {
	   $otpAttempt->no_of_attempts=$otpAttempt->no_of_attempts+1;
	   $otpAttempt->save();
	   // echo "<pre>";print_r($otpAttempt);exit;
	   if($otpAttempt->no_of_attempts<=4)
	   {
		   $data['attempt_no']=$otpAttempt->no_of_attempts;
		  
		   if($otpAttempt->otp==$request['otp'])
		   {
			  
			   return view('admin/changeForgotPassword',$data);
			   
		   }
		   else{
			   // echo "<pre>";print_r($otpAttempt);exit;
			    
			   return view('admin/verifyForgotOtp',$data);
		   }
	  
	   }
		   else{
			    $otpAttempt=phoneOtp::where('phone_no',$user_phone)->delete();
			   return redirect('admin')->with('error_message', 'No of attemps failed Plese re login')->withInput(); 
		   }
	   }
	   }
	   return redirect('admin')->with('error_message', 'User Not found')->withInput(); ;
        // return view('admin.loginVerifyOtp');
			
		}
		
		public function makeNewPassword(Request $request)
		{
			$user=User::where('id',$request['id'])->first();
			if($user)
			{
				$user->password=$request['password'];
				$user->save();
				return redirect('admin')->with('success_message', 'Password Changed SuccessFully')->withInput();
			}
			
		}
}
