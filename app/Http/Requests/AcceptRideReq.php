<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use App\Helpers\Response;
use App\Rules\CheckRideDetRule;
use App\Rules\CheckRefferalCodeWhileAcceptingRide;

class AcceptRideReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ride_id' => ['required','integer','exists:ride,id',new CheckRideDetRule],
            'consume_seats' => ['required','integer','between:1,1'],
            'card_source_id' => 'required',
            'price' =>  'required',
            'currency' => 'required',
            // 'card_id' => 'required',
            'referral_code' => ['sometimes', new CheckRefferalCodeWhileAcceptingRide],
            'discount_applied' => 'required_with:referral_code'
        ];
    }


    /**
     * Overwrite Validation error response
     * 
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {   
        $failedRules = $validator->failed();
        $statusCode = Response::resposeCode(key($failedRules),$failedRules);
        $response = new JsonResponse([ 
            'message' => $validator->errors()->first()
        ], $statusCode);

        throw new ValidationException($validator, $response);

    }

}
