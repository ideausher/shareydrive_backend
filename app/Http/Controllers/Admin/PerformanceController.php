<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use App\User;
use App\Ride;
use App\RideAccepted;
use App\Review;
use App\Transaction;
use DB;
use App\ReferralRecord;

class PerformanceController extends Controller {

    public function showOverallPerformance(){
        $data['totalUser']= \App\RoleUser::where('role_id', '1')->count();;	
        $data['totalEarnings']=Transaction::pluck('amount')->sum();
        $data['totalRideOffered']=Ride::all()->count();
        $data['totalPassengerTraveled']=RideAccepted::select('*')->distinct()->count('user_id');
        $data['totalReferalCode']=ReferralRecord::all()->count();
        
        return view('admin.overallPerformance', $data);
    }


}
