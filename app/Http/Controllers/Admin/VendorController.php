<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use Spatie\Geocoder\Facades\Geocoder;
use App\Http\Controllers\Controller;
use App\Mail\RejectVendor;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\UserAddresses;
use App\ServiceCategory;
use App\Helpers\Datatable\SSP;
use App\User;
use App\VenderService;
use App\vendorEducation;
use App\Service;
use App\venderSlot;
use File;
use Illuminate\Support\Facades\Auth;
use App\Mail\Admin\NotificationEmail;
use App\Models\Notification;
use App\Jobs\SendEmailJob;
use App\Admin as Admin;



class VendorController extends Controller {

    /**
     * User Model
     * @var User
     */
    protected $user;
    protected $pageLimit;

    /**
     * Inject the models.
     * @param User $user
     */
    public function __construct(User $user) {
        $this->user = $user;
        $this->pageLimit = config('settings.pageLimit');
    }

    /**
     * Display a listing of user
     *
     * @return Response
     */
    public function index() {

        // Show the page
        
        return view('admin/vendorList');
    }

        /**
     * Display a listing of user
     *
     * @return Response
     */
    public function create() {     
        $categories = ServiceCategory::orderBy('created_at', 'Asc')->get();
        return view('admin/addDoctor',compact('categories'));
    }

    /**
     * Display the specified user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show_vendor($id) {

        $user = User::with('venderServices')->find($id);
        $venderServices = User::find($id)->venderServices()->paginate(2);
        return view('admin/vendorView', compact('user', 'venderServices'));
    }

    public function update(Request $request, $id) {
        $user = User::findOrFail($id);
        
        $data = $request->all();
        $validator = Validator::make($data, [
                    'firstname' => 'required|string|max:45',
                    'lastname' => 'required',
                    'email' => 'required|string|email|max:255',
                    'phone_number' => 'required',
                    'gender' => 'required|in:male,female',
                    'address' => 'required',
                    'experience' => 'required',
                    'specialization' => 'required',
                    'city' => 'required',
                    'state' => 'required',
                    'country' => 'required',
                    'postal_code' => 'required|int',
                    'price' => 'required'
        ]);
            
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($request->hasFile('image')) { //check the file present or not
            $rules = array('image' => 'mimes:jpeg,png,jpg,gif,svg||max:4096');
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $image = $request->file('image'); //get the file
            $data['image'] = md5(uniqid()) . '.' . $image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('/images/avatars/'); //public path folder dir

            if (!$image->move($destinationPath, $data['image'])) {
                echo "image not uploaded correclty! Try Later";
                die;
            }
        }

        $user->update($data);

        $geo_locations = Geocoder::getCoordinatesForAddress($data['address']);

        if (isset($geo_locations) && ($geo_locations['lat'] == 0 || $geo_locations['lng'] == 0)) {

            return redirect()->back()->withErrors(['msg', 'Invalid address'])->withInput();
        }

        $userAddress = UserAddresses::where('user_id', $id)->firstOrFail();
        if ($userAddress) {
            $userAddress->update([
                'place_id' => (isset($geo_locations['place_id']) && $geo_locations['place_id']) ? $geo_locations['place_id'] : '',
                'latitude' => (isset($geo_locations['lat']) && $geo_locations['lat']) ? $geo_locations['lat'] : '',
                'longitude' => (isset($geo_locations['lng']) && $geo_locations['lng']) ? $geo_locations['lng'] : '',
                'gender' => $data['gender'],
                'city' => $data['city'],
                'state' => $data['state'],
                'country' => $data['country'],
                'pincode' => $data['postal_code'],
                'full_address' => $data['address'],
            ]);
        }

         vendorEducation::where('user_id', $id)->delete();

        if (isset($data['degree']) && isset($data['batch']) && isset($data['edu_desc'])) {

            $edu_details = [];

            for ($i = 0; $i <= (count($data['degree']) - 1); $i++) {

                vendorEducation::create([
                    'user_id' => $user->id,
                    'degree' => $data['degree'][$i],
                    'batch' => $data['batch'][$i],
                    'edu_desc' => $data['edu_desc'][$i],
                ]);
            }
        }

        if (isset($data['specialization']) && $data['specialization']) {

            $vendor_service = VenderService::where('vender_id', $id)->firstOrFail();
            $vendor_service->update([
                'cat_id' => $data['specialization'],
                'experience' => $data['experience'],
                'price' => $data['price']
            ]);
        }

        return redirect('admin/doctors')->with('success_message', trans('admin/user.user_update_message'));
    }

    public function edit($id) {
        
        $user = User::with('userAddress', 'venderServices', 'vendorEducations')->find($id);
        $venderServices = User::find($id)->venderServices()->get();
        $vendorEducation = User::find($id)->vendorEducations()->get();
        $categories = ServiceCategory::orderBy('created_at', 'Asc')->get();
        
        if ($user) {
            return view('admin/editvendor', compact('user','categories', 'venderServices','vendorEducation'));
        }
    }
    public function deleteEdu($id) {
        vendorEducation::find($id)->delete();
       
    }


    public function show($id) {
     
        $user = User::findOrFail($id);
        return view('admin/VendorView', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
        $user = User::findOrFail($id);
        $oldFile = \Config::get('constants.USER_IMAGE_PATH') . $user->image;
        if (File::exists($oldFile)) {
            File::delete($oldFile);
        }
        
        User::destroy($id);
        UserAddresses::where('user_id', $id)->delete();
        vendorEducation::where('user_id', $id)->delete();
        VenderService::where('vender_id', $id)->delete();

        $array = array();
        $array['success'] = true;
        $array['message'] = trans('admin/user.user_delete_message');
        echo json_encode($array);
    }

    public function changeUserStatus(Request $request) {
        $data = $request->all();
        $user = User::find($data['id']);

        if ($user->status) {
            $user->status = '0';
        } else {
            $user->status = '1';
        }
        $user->save();

        $array = array();
        $array['status'] = $user->status;
        $array['success'] = true;
        $array['message'] = trans('admin/user.user_status_message');
        echo json_encode($array);
    }

    public function addVenderService(Request $request) {

        $data = $request->all();
        $user = User::find($data['user_id']);

        if (!$user) {

            $array['status'] = 0;
            $array['success'] = false;
            $array['message'] = trans('admin/user.user_not_found');
        }

        $checkServiceAssignedAlready = VenderService::where([['vender_id', '=', $data['user_id']], ['service_id', '=', $data['service_id']]])->first();
        if ($checkServiceAssignedAlready) {
            $array['status'] = 0;
            $array['success'] = false;
            $array['message'] = trans('admin/service.service_already_assigned');
            return $array;
        }
        
        $getService = Service::where('id', '=', $data['service_id'])->first();
        $data = array(
            'vender_id' => $data['user_id'],
            'service_id' => $data['service_id'],
            'status' => '1',
            'cat_id' => $getService->cat_id
        );
        
        if (VenderService::create($data)) {
            
            $array = array();
            $array['status'] = 1;
            $array['success'] = true;
            $array['message'] = trans('admin/service.service_assigned_successfully');
            return $array;
        }
    }

    /**
     * Change user credit of the specified user.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeVenderServiceStatus(Request $request) {
        $data = $request->all();
        $venderService = VenderService::find($data['id']);

        if ($venderService->status) {
            $venderService->status = '0';
        } else {
            $venderService->status = '1';
        }
        $venderService->save();

        $array = array();
        $array['status'] = $venderService->status;
        $array['success'] = true;
        $array['message'] = trans('admin/user.user_status_message');
        echo json_encode($array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteVenderService($id) {
        Schedule::where('service_id', $id)->delete();
        Service::destroy($id);

        $array = array();
        $array['success'] = true;
        $array['message'] = trans('admin/service.service_delete_message');
        echo json_encode($array);
    }

    public function deleteVendorSlot(Request $request) {
        $data = $request->all();

        venderSlot::where('vender_id', $data['vendorId'])->where('slot_id', $data['slotId'])->delete();

        $array = array();
        $array['success'] = true;
        $array['message'] = trans('admin/vendors.slot_delete_message');
        echo json_encode($array);
    }

    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
                    'first_name' => 'required|string|max:45',
                    'last_name' => 'required_if:role_id,==,2',
                    'email' => 'required|string|email|max:255',
                    'password' => 'required|string|min:6|same:confirm_password',
                    'phone' => 'required',
                    'confirm_password' => 'required',
                    'gender' => 'required|in:male,female',
                    'role_id' => 'required',
                    'phone' => 'required',
                    'address' => 'required',
                    'experience' => 'required',
                    'specialization' => 'required',
                    'image' => 'required||mimes:jpeg,png,jpg,gif,svg||max:4096',
                    'city'=>'required',
                    'state'=>'required',
                    'country'=>'required',
                    'postal_code'=>'required|integer|digits:6',
                    'price'=>'required|integer|not_in:0',
                    'degree'=> 'required',
                    'batch'=> 'required',
                    'edu_desc'=> 'required',
                    'lang_type'=> 'required|in:1,2,3',
        ],[
            'price.integer|not_in:0'=>'Invalid price'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $geo_locations = Geocoder::getCoordinatesForAddress($data['address']);
        
        
        if(isset($geo_locations) && ($geo_locations['lat']==0 || $geo_locations['lng']==0)){
            
            return redirect()->back()->withErrors(['Invalid address'])->withInput();
            
        }

        if ($request->hasFile('image')) { //check the file present or not
            $image = $request->file('image'); //get the file
            $data['image'] = md5(uniqid()) . '.' . $image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('/images/avatars/'); //public path folder dir
            if (!$image->move($destinationPath, $data['image'])) {
                echo "image not uploaded correclty! Try Later";
                die;
            } //mve to destination you mentioned
        }

        $user_check = User::where('email', $data['email'])->first();

        if (!$user_check) {

            $user = User::create([
                        'firstname' => $data['first_name'],
                        'lastname' => $data['last_name'],
                        'email' => $data['email'],
                        'password' => $data['password'],
                        'phone_number' => $data['phone'],
                        'gender' => $data['gender'] ? $data['gender'] : '0',
                        'status' => User::active,
                        'image'=>$data['image'] ? $data['image'] :0,
                        'lang_type'=>$data['lang_type'],
            ]);

            $user->attachRole($data['role_id']);
           
            if (isset($data['specialization']) && $data['specialization']) {
                VenderService::create([
                    'vender_id' => $user->id,
                    'cat_id' => $data['specialization'],
                    'experience'=>$data['experience'],
                    'price'=>$data['price']
                ]);
            }
            
                    if(isset($geo_locations) && ($geo_locations['lat']!=0 || $geo_locations['lng']!=0)) {

                            $user_adress = UserAddresses::create([
                            'user_id' => $user->id,
                            'place_id' => (isset($geo_locations['place_id']) && $geo_locations['place_id']) ? $geo_locations['place_id']:'',
                            'latitude' => (isset($geo_locations['lat']) && $geo_locations['lat']) ? $geo_locations['lat']:'',
                            'longitude' => (isset($geo_locations['lng']) && $geo_locations['lng']) ? $geo_locations['lng']:'',
                            'gender' => $data['gender'],
                            'city' => $data['city'],
                            'state'=>$data['state'],
                            'country' => $data['country'],
                            'pincode' => $data['postal_code'],
                            'full_address' => $data['address'],
                            'address_type' => 1,
                ]);
            }
            
            if(isset($data['degree']) && isset($data['batch']) && isset($data['edu_desc'])){
                
                $edu_details = [];
                
                for($i=0 ;$i<=(count($data['degree'])-1);$i++){

                    // if any of the fields in the dynamically created fields is empty then don't add it to the database
                    if(isset($data['degree'][$i]) && isset($data['batch'][$i]) && isset($data['edu_desc'][$i]))
                    {
                        vendorEducation::create([
                            'user_id' => $user->id,
                            'degree' => $data['degree'][$i],
                            'batch' => $data['batch'][$i],
                            'edu_desc'=>$data['edu_desc'][$i],
                        ]);
                    }
               
                    
                }
            }

            return redirect()->route('doctors.index')->with('success_message', 'Doctor added successfully');
   
        } else {
             return redirect()->back()->with('error_message', 'Doctor already exist')->withInput();
        }
    }

    /**
     * Change user credit of the specified user.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateCredit(Request $request) {

        $data = $request->all();
        $data['credit'] = $data['value'];
        $user = User::find($data['userId']);

        $user->update($data);
        $array = array();
        $array['success'] = true;
        session()->flash('success_message', trans('admin/user.credit_update_message'));
        echo json_encode($array);
    }

    public function getVendorData() {

        $table = 'users';

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'users.firstname', 'dt' => 0, 'field' => 'firstname'),
            array('db' => 'users.lastname', 'dt' => 1, 'field' => 'lastname'),
            array('db' => 'users.email', 'dt' => 2, 'field' => 'email'),
            array('db' => 'users.phone_number', 'dt' => 3, 'field' => 'phone_number'),
            array('db' => 'GROUP_CONCAT(sc.cat_name) as cat_name', 'dt' => 4, 'field' => 'cat_name'),
            array('db' => 'users.image', 'dt' => 5, 'formatter' => function( $d, $row ) {
                    if(isset($row) && !is_null($row['image']) && file_exists(public_path('/images/avatars/'.$row['image']))){
                        return '<img id="uploaded-image" src="'.url('/images/avatars//'.$row['image']).'" alt="image"  height="35" width="35">';
                    }else{
                        return '<img id="uploaded-image" src="'.url('uploads/user/default.png').'" alt="image"  height="35" width="35">';
                    }
            }, 'field' => 'image'),
            array('db' => 'users.status', 'dt' => 6, 'formatter' => function( $d, $row ) {
                    if ($row['status'] == User::active) {
                        return '<a href="javascript:;" class="btn btn-success status-btn" id="' . $row['id'] . '" title="' . trans('admin/common.click_to_inactive') . '" data-toggle="tooltip">' . trans('admin/common.active') . '</a>';
                    } else if ($row['status'] == User::inActive) {
                        return '<a href="javascript:;" class="btn btn-danger status-btn" id="' . $row['id'] . '" title="' . trans('admin/common.click_to_active') . '" data-toggle="tooltip">' . trans('admin/common.inactive') . '</a>';
                    } else if ($row['status'] == User::pending) {
                        return '<a href="javascript:;" class="btn btn-warning status-btn" id="' . $row['id'] . '" title="' . trans('admin/common.pending') . '" data-toggle="tooltip">' . trans('admin/common.pending') . '</a>';
                    } else {
                        return '<a href="javascript:;" class="btn btn-danger status-btn" id="' . $row['id'] . '" title="' . trans('admin/common.rejected') . '" data-toggle="tooltip">' . trans('admin/common.rejected') . '</a>';
                    }
                }, 'field' => 'status'),
            array('db' => 'users.id', 'dt' => 7, 'formatter' => function( $d, $row ) {
  
                    $operation = ' <a href="doctors/' . $d . '/edit" class="btn btn-primary d-inline-block" title="' . trans('admin/common.edit') . '" data-toggle="tooltip"><span class="fa fa-pencil"></span></a> <a href="javascript:;" id="' . $d . '" class="btn btn-danger delete-btn" title="' . trans('admin/common.delete') . '" data-toggle="tooltip"><span class="fa fa-times"></span></a>';
                    
                    return $operation;
                }, 'field' => 'id'),
            array('db' => 'users.id', 'dt' => 8, 'formatter' => function( $d, $row ) {
  
                    $operation = ' <a href="appointments/' . $d . '" class="btn btn-primary d-inline-block center" title="' . trans('View Available Appointments') . '" data-toggle="tooltip"><span class="fa fa-eye"></a>';
                    
                    return $operation;
            }, 'field' => 'id')    

        );

        $sql_details = array(
            'user' => config('database.connections.mysql.username'),
            'pass' => config('database.connections.mysql.password'),
            'db' => config('database.connections.mysql.database'),
            'host' => config('database.connections.mysql.host')
        );
  
        $joinQuery = '';
        $joinQuery .= " LEFT JOIN role_user ru ON ru.user_id = users.id";
        $joinQuery .= " LEFT JOIN roles r ON r.id = ru.role_id";
        $joinQuery .= " LEFT JOIN vender_services vs ON vs.vender_id = users.id";
        $joinQuery .= " LEFT JOIN service_categories sc ON vs.cat_id = sc.id";

        $extraWhere = "r.name='doctor'";
        $groupBy = "users.id";

       

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
        );
    }

    public function rejectVendor(Request $request) {
        $data = $request->all();
        $user = User::find($data['id']);
        $user->update(['rejection_reason' => $data['reason'], 'status' => (string) User::rejected]);
        Mail::to($user->email)->send(new RejectVendor($user));

        $array = array();
        $array['success'] = true;
        $array['message'] = trans('admin/vendors.reject_success_message');
        echo json_encode($array);
    }

}
