<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Models\Notification;
use App\Http\Controllers\Controller;
use App\User;
use App\Helpers\Datatable\SSP;
use App\Mail\Admin\NotificationEmail;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Jobs\SendEmailJob;
use App\Admin as Admin;
use App\Models\DeviceDetails;
use App\Services\PushNotification;
use Session;
use Validator;


class NotificationController extends Controller {
    
    /* #region Get Notifications  */
    public function getNotifications($type = null) {
      
        // if($type=='send'){
        //     return view('admin/sendNotifications');
        // }
        $user_id = Auth::guard('admin')->user()->id;
        if ($type == 'all') {
            $notifications = Notification::orderBy('id', 'DESC')->paginate(10);
        } else {
            
            $notifications = Notification::orderBy('id', 'DESC')->get();
            foreach ($notifications as $notification) {
                $notification->is_read = 1;
                $notification->save();
            }
        }
        $patient = User::select("users.*", "role_user.user_id", "roles.id")
                        ->join("role_user", "role_user.user_id", '=', "users.id")
                        ->join("roles", "roles.id", "role_user.role_id")
                        ->where('roles.id', 1)->get();
        $doctor = User::select("users.*", "role_user.user_id", "roles.id")
                        ->join("role_user", "role_user.user_id", '=', "users.id")
                        ->join("roles", "roles.id", "role_user.role_id")
                        ->where('roles.id', 2)->get();

        return view('admin/notifications', compact('notifications', 'type', 'patient', 'doctor'));
    }
    /* #endregion */
  
    /* #region NotificationEmail  */
    public function NotificationEmail(Request $request) {

        
        $data = $request->all();
        
        $profile = auth()->guard('admin');
        $admin = Admin::findOrFail($profile->user()->id);
        $users_detail=User::find($data['id']);
        $users_id =$users_detail->pluck('id');
        $emailJob = new SendEmailJob($users_detail,$data,$admin);
    
        // Inserting data into table Notification
        Notification::insert([
                                'vender_id'=> $admin->id,
                                'user_id' => $request->id[0],
                                'type'=> '3', 
                                'title'=>$request->subject,
                                'message'=>$request->message,
                                'is_read' => '2',
                            ]);
        
        
        DeviceDetails::sendBulkNotification($data['subject'],$data['message'],$users_id);
        session()->flash('success_message', "Notification SuccessFully Sent");
        return redirect()->back();
  
    }
    /* #endregion */

    /* #region fetchNotification  */
    public function fetchNotification(){
        $users = User::all();
        return view('admin.sendNotification',compact('users'));
    }
    /* #endregion */

    /* #region  sendNotification */
    public function sendNotification(Request $request){
		 $validator = Validator::make($request->all(), [
                        'title' => 'required',
                        'message' => 'required',
						'users'=>'required',
                            //   'c_password' => 'required|same:password',
            ]);
            if ($validator->fails()) {
                $validationError = $validator->messages()->first();
                Session::flash('flashError', $validationError);
                return redirect()->back()->with('error_message',$validationError)->withInput();
            }
		$data=[
            'type'=>12,
        ];
        foreach($request->users as $user ){
            DeviceDetails::sendNotification($request->title ,$request->message,$user,$data,Auth::guard('admin')->user()->id);
        }
        Session::flash('flashSuccess', 'Notification Send Successfully');
        return redirect('admin/sendNotifications')->with('success_message', 'Notification Sent Successfully');
    }
    /* #endregion */
    

}
