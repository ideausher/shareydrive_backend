<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use Devrahul\Signinupapi\Models\phoneOtp;
use Aloha;
use App\User;

class LoginController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest.admin', ['except' => 'doLogout']);
    }

    /**
     * Display a login page
     * @return Response
     */
    public function getIndex() {
        return view('admin.login');
    }

    /**
     * Check login with username and password.
     * @return redirect to dashboard or error_message if login fails
     */
    public function doLogin(Request $request) {
        
        $input = $request->all();
        $credentials = array(
            'phone_number' => $input['phone_number'],
            'password' => $input['password']
        );
		
        
        if (auth()->guard('admin')->attempt($credentials)) {
           
            if(auth()->guard('admin')->user()->hasRole('admin')){
				$data['user']=auth()->guard('admin')->user();
				$data['phone_number']=$input['phone_number'];
				$data['password']=$input['password'];
			                 $otp = rand(1000, 9999);

	
            // always add + in front of phone no
            $phone = "+". $data['user']['phone_country_code'] . $data['user']['phone_number'];
            
            $twilio = new Aloha\Twilio\Twilio(env('TWILIO_SID'), env('TWILIO_TOKEN'), env('TWILIO_SMS_FROM_NUMBER'));
        try{
			
            if ($twilio->message($phone, 'Otp is ' . $otp)) {
                $user_otp = phoneOtp::where('phone_no', $data['user']['phone_number'])->first();
                
                if ($user_otp) {
                    $user_otp->update(['otp' => $otp,'no_of_attempts'=> 0,'is_verified'=>0]);
                    $id = $user_otp->id;

                } else {
                    $phoneOtp = phoneOtp::create(['phone_no' => $data['user']['phone_number'], 'otp' => $otp,'phone_country_code' =>  $data['user']['phone_country_code']]);                  $id = $phoneOtp->id;  
                }
            auth()->guard('admin')->logout();
                return view('admin/verifyLoginOtp',$data);
				 // auth()->guard('admin')->logout();
       // session()->flush();
      //  session()->regenerate();
            }else{
				auth()->guard('admin')->logout();
				return redirect('admin/')->with('error_message', 'User Not Registered for otp')->withInput();  
				
			}
			}
			catch (\Exception $ex) {
                
            $message = $ex->getMessage();
			auth()->guard('admin')->logout();
			return redirect('admin')->with('error_message', $message)->withInput();
			}
        }
		}
		else{
                return redirect('admin/')->with('error_message', 'Invalid Credentials')->withInput();        
            }
        // authentication failure! lets go back to the login page
       
    }
    public function doVerifyOtp(Request $request){
      // echo "<pre>";print_r($request->all());exit;
	   $data['user']=User::where('id',$request['id'])->first();
	   $data['phone_number']=$request['phone_number'];
	   $data['password']=$request['password'];
	   if($data['user']){
	   $user_phone=$data['user']->phone_number;
	   $otpAttempt=phoneOtp::where('phone_no',$user_phone)->first();
	    //echo "<pre>";print_r($user);exit;
	   if($otpAttempt)
	   {
	   $otpAttempt->no_of_attempts=$otpAttempt->no_of_attempts+1;
	   $otpAttempt->save();
	   // echo "<pre>";print_r($otpAttempt);exit;
	   if($otpAttempt->no_of_attempts<=4)
	   {
		   $data['attempt_no']=$otpAttempt->no_of_attempts;
		  
		   if($otpAttempt->otp==$request['otp'])
		   {
			    $credentials = array(
            'phone_number' => $request['phone_number'],
            'password' => $request['password']
        );
		
        
        if (auth()->guard('admin')->attempt($credentials)) {
           
            if(auth()->guard('admin')->user()->hasRole('admin')){
				return redirect('admin/dashboard');
			}
		}
			   
		   }
		   else{
			   // echo "<pre>";print_r($otpAttempt);exit;
			    
			   return view('admin/verifyLoginOtp',$data);
		   }
	  
	   }
		   else{
			    $otpAttempt=phoneOtp::where('phone_no',$user_phone)->delete();
			   return redirect('admin')->with('error_message', 'No of attemps failed Plese re login')->withInput(); 
		   }
	   }
	   }
	   return redirect('admin')->with('error_message', 'User Not found')->withInput(); ;
        // return view('admin.loginVerifyOtp');
    }

    public function doLogout() {
        auth()->guard('admin')->logout();
        session()->flush();
        session()->regenerate();
        return redirect('admin')->with('success_message', 'Logged Out Successfully');
    }

}
