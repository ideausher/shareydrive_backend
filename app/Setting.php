<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
   
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['language', 'site_title', 'logo', 'email', 'phone', 'address', 'map', 'facebook', 'twitter', 'linkedin', 'googleplus','term_condition','referral_code_text','referral_code_discount','ride_time_diff_search','before_pass_cancel_time','before_driv_cancel_time'];
}
