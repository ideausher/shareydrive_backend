<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_chat';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sender_id', 'receiver_id', 'message_content', 'message_read', 'type'];

    public function user() {
        return $this->belongsTo('App\User', 'sender_id', 'id');
    }
    public function vender() {
        return $this->belongsTo('App\User', 'receiver_id', 'id');
    }

}
