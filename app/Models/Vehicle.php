<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $table = 'ride_vehicle';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'rego','country', 'car_manufacture', 'car_model','car_type','car_color','register_year', 'register_state','status'
    ];
}
