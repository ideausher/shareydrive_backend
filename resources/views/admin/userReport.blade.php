@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/service.services') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Report</h1>


        <!-- <ol class="breadcrumb">
                <li><a href="/admin"><span class="fa fa-dashboard"></span> {!! trans('admin/common.home') !!}</a></li>
                <li class="active">{!! trans('admin/service.services') !!}</li>
            </ol> -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
                <!-- ./ notifications -->
            </div>
            <div class="col-xs-12">
                <div class="box">
                    @if(isset($user))
                    {!! Form::model($user, array('url' =>'admin/userReport/', 'method' => 'PATCH', 'id' => 'service-form',
                    'files' => true )) !!}
                  
                   

                    <div class="box-body">
					   
       
						<div class="col-xs-6">
                            <div class="form-group has-feedback">
                                 {!! Form::label('date', trans('Select Date')) !!}
                               <input type="text" id="from" required readonly class="form-control" name="from"  placeholder="From Date" >
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
							
                            
                        </div>
						<div class="col-xs-6">
                            <div class="form-group has-feedback">
                                 {!! Form::label('date', trans('Select Date')) !!}
                               <input type="text" id="to" required readonly class="form-control" name="to" placeholder="From Date" >
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
							
                            
                        </div>
						 <div class="box-footer">
                        {!! Form::submit('Submit',array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}
                        <a href="{!! url('admin/users') !!}" class="btn btn-default">{!! 'Cancel' !!}</a>
                    </div>

                        </div>              
	   
                    {!! Form::close()!!}
					 @endif
                </div> <!-- /.box -->
				
				@if(isset($totalNewUser))
          <div> Date Range Selected {{$from}} to {{$to}}</div>
          <br>
					<div>Total New User {{$totalNewUser}}</div>
				 <br>
				<div>Total Earnings {{$totalEarnings}}</div>
				<br>
				<div>Total Ride Offered {{$totalRideOffered}}</div>
				<br>
				<div>Total Passenger Travelled {{$totalPassengerTraveled}}</div>
				<br>
				<div>Total Referral Code used  {{$totalReferalCode}}</div>
				<br>
				 @endif
        @if(isset($rides) && !empty($rides))

        
        @if(isset($ridesEmpty)&& $ridesEmpty==0)
        
				    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

      var rides = <?php echo $rides; ?>;

      console.log(rides);

      google.charts.load('current', {'packages':['corechart']});

      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(rides);

        var options = {

          title: 'No of rides offered Monthly',

          curveType: 'function',

          legend: { position: 'bottom' }

        };

        var chart = new google.visualization.LineChart(document.getElementById('linechart'));

        chart.draw(data, options);

      }

    </script>



<div class="col-xs-6">
    <div id="linechart" style="width: 700px; height: 500px"></div>
    </div>
      @else
      <div class="col-xs-6">
      
      <div>No data avaialble in this date range</div>
    
    <div>
      @endif
				@endif
				
        @if(isset($earning) && !empty($earning))
        @if(isset($earningEmpty)&& $earningEmpty==0)
					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">

      var earning = <?php echo $earning; ?>;

      console.log(earning);

      google.charts.load('current', {'packages':['corechart']});

      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(earning);

        var options = {

          title: 'Earnings by Month',

          curveType: 'function',

          legend: { position: 'bottom' }

        };

        var chart = new google.visualization.LineChart(document.getElementById('graph'));

        chart.draw(data, options);

      }

    </script>
    <div class="col-xs-6">
  <div id="graph" style="width: 700px; height: 500px"></div>
    </div>
    @else
    <div class="col-xs-6">
   
    <div>No data avaialble in this date range</div>
   
    <div>
@endif
	@endif
            </div> <!-- /.col-xs-12 -->
        </div><!-- /.row (main row) -->
		  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
	    <script>
         $(function() {
            $( "#from" ).datepicker({
				 format: 'yyyy-mm-dd',
				 endDate: '-1d',				
			});
            $( "#to" ).datepicker({
			 format: 'yyyy-mm-dd',
			endDate:'-1d',
         });
		 });
      </script>

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@stop
