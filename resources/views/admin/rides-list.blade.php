@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! 'Rides List' !!}
@stop
@section('styles')
<link href="{!! asset('assets/admin/plugins/bootstrap3-editable/css/bootstrap-editable.css') !!}" rel="stylesheet"
  type="text/css" />
<style>
.credit-txt {
  cursor: pointer;
}
</style>
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>{!! 'Rides List' !!}</h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <!-- Notifications -->
        @include('admin.includes.notifications')
        <!-- ./ notifications -->
      </div>
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive">
            <table id="rides_list" class="table table-bordered">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Ride Date Time</th>
                  <th>Max Seats</th>
                  <th>Price Per Seat</th>
                  <th>Total Toll Tax</th>
                  <th>Gender</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div> <!-- /. box body -->
        </div> <!-- /.box -->
      </div> <!-- /.col-xs-12 -->
    </div><!-- /.row (main row) -->

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@section('scripts')
<script src="{{asset('assets/admin/plugins/bootstrap3-editable/js/bootstrap-editable.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  var oTable;
  $(document).ready(function() {
    oTable = $('#rides_list').dataTable({
      "dom": "<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'l><'col-xs-12 col-sm-4 col-md-4 col-lg-4'r><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'f>>t<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'i><'col-xs-12 col-sm-4 col-md-4 col-lg-4'><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'p>>",
      "processing": true,
      "serverSide": true,
      "ajax": "{!! url('admin/rides-management/rides-data') !!}",
      "order": [
        [0, "desc"]
      ],
      "language": {
        "emptyTable": "{!! trans('admin/common.datatable.empty_table') !!}",
        "info": "{!! trans('admin/common.datatable.info') !!}",
        "infoEmpty": "{!! trans('admin/common.datatable.info_empty') !!}",
        "infoFiltered": "({!! trans('admin/common.datatable.info_filtered') !!})",
        "lengthMenu": "Total Records",
        "loadingRecords": "{!! trans('admin/common.datatable.loading') !!}",
        "processing": "Processing",
        "search": "Search :",
        "zeroRecords": "No Records Found",
        "paginate": {
          "first": "First",
          "last": "Last",
          "next": "Next",
          "previous": "Previous"
        }
      }
    });

    $("#rides_list").on('click', '.delete-btn', function() {
      var id = $(this).attr('id');
      var r = confirm("Are you sure you want to delete?");
      if (!r) {
        return false
      }
      $.ajax({
        type: "DELETE",
        url: "{{ url('admin/rides-management/ride/') }}" + '/' + id,
        data: {
          _method: 'DELETE',
          _token: "{{ csrf_token() }}"
        },
        dataType: 'json',
        beforeSend: function() {
          $(this).attr('disabled', true);
          $('.alert .msg-content').html('');
          $('.alert').hide();
        },
        success: function(resp) {
          $('.alert:not(".session-box")').show();
          $('.alert-success .msg-content').html(resp.message);
          $('.alert-success').removeClass('hide');
          $(this).attr('disabled', false);
          oTable.fnDraw();
        },
        error: function(e) {
          $('.alert-danger .msg-content').html(resp.message);
          $('.alert-danger').removeClass('hide');
        }
      });
    });

    $("#user_list").on('click', '.status-btn', function() {
      var id = $(this).attr('id');
      var r = confirm("{!! trans('admin/common.status_confirmation') !!}");
      if (!r) {
        return false
      }
      $.ajax({
        type: "POST",
        url: "{{ url('admin/users/changeStatus') }}",
        data: {
          id: id,
          _token: "{{ csrf_token() }}"
        },
        dataType: 'json',
        beforeSend: function() {
          $(this).attr('disabled', true);
          $('.alert .msg-content').html('');
          $('.alert').hide();
        },
        success: function(resp) {
          $('.alert:not(".session-box")').show();
          if (resp.success) {
            $('.alert-success .msg-content').html(resp.message);
            $('.alert-success').removeClass('hide');
          } else {
            $('.alert-danger .msg-content').html(resp.message);
            $('.alert-danger').removeClass('hide');
          }
          $(this).attr('disabled', false);
          oTable.fnDraw();
        },
        error: function(e) {
          alert('Error: ' + e);
        }
      });
    });
          
    /* $.ajax({
        type: "GET",
        url: "{{ url('admin/users/show') }}",
        data: {
          id:id,
          _token: "{{ csrf_token() }}"
        },
        dataType: 'json',
        beforeSend: function() {
          $(this).attr('disabled', true);
          $('.alert .msg-content').html('');
          $('.alert').hide();
        },                  
        success: function(resp) {
          $('.alert:not(".session-box")').show();
          if (resp.success) {
            $('.alert-success .msg-content').html(resp.message);
            $('.alert-success').removeClass('hide');
          } else {
            $('.alert-danger .msg-content').html(resp.message);
            $('.alert-danger').removeClass('hide');    
          }
          $(this).attr('disabled', false);
          oTable.fnDraw();   
          alert("success");
        },
        error: function(e) {
            alert("This is Error");
          alert('Error: ' + e); 
        } 
      }); */
  
  });
</script>
@stop
