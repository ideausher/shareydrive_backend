<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Datatable\SSP;
use App\Http\Controllers\Controller;
use App\Schedule as Schedule;
use App\Service as Service;
use File;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\URL;
use App\ServiceCategory;

class ServicesController extends Controller
{

    /**
     * Service Model
     * @var Service
     */
    protected $service;
    protected $pageLimit;

    /**
     * Inject the models.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
        $this->pageLimit = config('settings.pageLimit');
    }

    /**
     * Display a listing of services
     *
     * @return Response
     */
    public function index()
    {

        // Grab all the services
        $services = Service::paginate($this->pageLimit);
        // Show the service\
        return view('admin/servicesList', compact('services'));
    }

    /**
     * Show the form for creating a new service
     *
     * @return Response
     */
    public function create()
    {
        $categories = ServiceCategory::where('status', '1')->pluck('cat_name', 'id');
        return view('admin.services')->withcategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $startTimeRequired = $endTimeRequired = '';
        $rules = array(
            'title' => 'required|unique:services',
            'price_type' => 'required',
            'price' => 'required',
            'image' => 'required||mimes:jpeg,png,jpg,gif,svg||max:4096',
            'description' => 'required',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($request->hasFile('image')) { //check the file present or not
            $image = $request->file('image'); //get the file
            $data['image'] = uniqid() . '.' . $image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('/images'); //public path folder dir
            if (!$image->move($destinationPath, $data['image'])) {
                echo "image not uploaded correclty! Try Later";
                die;
            } //mve to destination you mentioned
        }
        
        $service = Service::create($data);
        $lastInsertId = $service->id;
        return redirect()->route('services.index')->with('success_message', trans('admin/service.service_add_message'));
    }

    /**
     * Display the specified service.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $service = Service::findOrFail($id);

        return view('admin.services.serviceDetail', compact('service'));
    }

    /**
     * Show the form for editing the specified service.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $all_categories = ServiceCategory::all()->pluck('cat_name', 'id');
        $service = Service::find($id);

        if ($service) {
            return view('admin/services', compact('service'))->withcategories($all_categories);
        } else {
            return redirect('admin/services')->with('error_message', trans('admin/service.service_invalid_message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::findOrFail($id);
        $data = $request->all();

        /*if ($data['service_type'] != 'weekly') {
            $startTimeRequired = $endTimeRequired = 'required';
        }*/

        $rules = array(
            'title' => 'required',
            'price_type' => 'required',
            'price' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif,svg||max:4096',
            'description' => 'required',
            'cat_id' => 'required'
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($request->hasFile('image')) { //check the file present or not
            $image = $request->file('image'); //get the file
            $data['image'] = uniqid() . '.' . $image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('/images'); //public path folder dir
            if (!$image->move($destinationPath, $data['image'])) {
                echo "image not uploaded correclty! Try Later";
                die;
            } //mve to destination you mentioned
        }
        $service->update($data);

        return redirect()->route('services.index')->with('success_message', trans('admin/service.service_update_message'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Schedule::where('service_id', $id)->delete();
        Service::destroy($id);

        $array = array();
        $array['success'] = true;
        $array['message'] = trans('admin/service.service_delete_message');
        echo json_encode($array);
    }

    public function changeServiceStatus(Request $request)
    {
        $array = array();
        $data = $request->all();

        $service = Service::find($data['id']);
        $catID = $service->cat_id;
        
        $category = ServiceCategory::where(['id' => $catID])->get();
        
        if(!$category['0']->status) {
            $array['message'] = 'Category of the selected service is inactive.';
            return json_encode($array);
        }

        if ($service->status) {
            $service->status = '0';
        } else {
            $service->status = '1';
        }
        $service->save();

        $array['success'] = true;
        $array['message'] = trans('admin/service.service_status_message');
        echo json_encode($array);
    }

    public function getServicesData()
    {

        /*
         * DataTables example server-side processing script.
         *
         * Please note that this script is intentionally extremely simply to show how
         * server-side processing can be implemented, and probably shouldn't be used as
         * the basis for a large complex system. It is suitable for simple use cases as
         * for learning.
         *
         * See http://datatables.net/usage/server-side for full details on the server-
         * side processing requirements of DataTables.
         *
         * @license MIT - http://datatables.net/license_mit
         */

        /*         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */

        // DB table to use
        $table = 'services';

        // Table's primary key
        $primaryKey = 'id';

        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array('db' => 'services.title', 'dt' => 0, 'field' => 'title'),
            array('db' => 'services.price', 'dt' => 1, 'field' => 'price'),
            array('db' => 'services.price_type', 'dt' => 2, 'field' => 'price_type', 'formatter' => function($d, $row) {
                if ($row['price_type'])
                {
                    return 'Fixed Price';
                }
                else
                {
                    return 'Hourly Price';
                }
            }),
            array('db' => 's_c.cat_name', 'dt' => 3, 'field' => 'cat_name'),
            array('db'=>'services.image','dt'=>4,'field' => 'image','formatter' => function ($d, $row) {
                if($row['image']){
                    return '<img src="'.URL::asset('/public/images/'.$row['image']).'" alt="image"  height="40" width="60">';
                }
            }),
            array('db' => 'services.status', 'dt' => 5, 'formatter' => function ($d, $row) {
                if ($row['status']) {
                    return '<a href="javascript:void(0);" class="btn btn-success status-btn" id="' . $row['id'] . '" title="' . trans('admin/common.click_to_inactive') . '" data-toggle="tooltip">' . trans('admin/common.active') . '</a>';
                } else {
                    return '<a href="javascript:void(0);" class="btn btn-danger status-btn" id="' . $row['id'] . '" title="' . trans('admin/common.click_to_active') . '" data-toggle="tooltip">' . trans('admin/common.inactive') . '</a>';
                }
            }, 'field' => 'status'),
            array('db' => 'services.id', 'dt' => 6, 'formatter' => function ($d, $row) {
                
                $operation = '<a href="services/' . $d . '/edit/" class="btn btn-primary" title="' . trans('admin/common.edit') . '" data-toggle="tooltip"><span class="fa fa-pencil"></span></a>&nbsp;';

                $operation .= '<a href="javascript:;" id="' . $d . '" class="btn btn-danger delete-btn" title="' . trans('admin/common.delete') . '" data-toggle="tooltip"><span class="fa fa-times"></span></a>&nbsp;';

                return $operation;
            }, 'field' => 'id'),
            array('db' => 'services.created_at', 'dt' => 7, 'field' => 'created_at')
        );

        // SQL server connection information
        $sql_details = array(
            'user' => config('database.connections.mysql.username'),
            'pass' => config('database.connections.mysql.password'),
            'db' => config('database.connections.mysql.database'),
            'host' => config('database.connections.mysql.host'),
        );

        $joinQuery = ' LEFT JOIN service_categories s_c ON s_c.id = services.cat_id ';
        $extraWhere = "";
        $groupBy = "";
        // echo "<pre>";
        // print_r($columns);
        // echo "</pre>"; exit;
        echo json_encode(
            SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
        );
    }

}
