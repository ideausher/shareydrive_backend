<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RideRequest extends Model
{
    
    protected $table = 'ride_request';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ride_id','user_id','status',
    ];

    public function mapRide(){
        return $this->hasOne('App\Models\Rides','id','ride_id');
    }

}
