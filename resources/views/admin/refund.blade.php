@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/refund.refund_list') !!}
@stop
@section('styles')
<link href="{!! asset('assets/admin/plugins/bootstrap3-editable/css/bootstrap-editable.css') !!}" rel="stylesheet" type="text/css" />
<style>
    .credit-txt{cursor: pointer;}
</style>
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{!! trans('admin/refund.refund_list') !!}</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
                <!-- ./ notifications -->
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body table-responsive">
                        <thead>
                            <tr>
                                <td>

                                </td>
                            </tr>
                        </thead>
                        <table id="user_list" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="15%">{!! trans('admin/refund.refernce_id') !!}</th>
                                    <th width="15%">{!! trans('admin/refund.useremail') !!}</th>
                                    <th width="20%">{!! trans('admin/refund.venderemail') !!}</th>
                                    <th width="10%">{!! trans('admin/refund.amount') !!}</th>
                                    <th width="10%">{!! trans('admin/refund.curreny') !!}</th>
                                    <th width="10%">{!! trans('admin/refund.reason') !!}</th>
                                    <th width="10%">{!! trans('admin/refund.status') !!}</th>
                                    <th width="10%">{!! trans('admin/common.action') !!}</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div> <!-- /. box body -->
                </div> <!-- /.box -->
            </div> <!-- /.col-xs-12 -->
        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@section('scripts')
<script src="{{asset('assets/admin/plugins/bootstrap3-editable/js/bootstrap-editable.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
    var oTable;

    oTable = $('#user_list').DataTable({
        "dom": "<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'l><'col-xs-12 col-sm-4 col-md-4 col-lg-4'r><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'f>>t<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'i><'col-xs-12 col-sm-4 col-md-4 col-lg-4'><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'p>>",
        "processing": true,
        "serverSide": true,
        "ajax": "{!! url('admin/refundData') !!}",
        // "columnDefs": [{
        //     "targets": [1,2,3,4,5,6],
        //     "orderable": false
        // },{
        //     "targets": [4,5,6],
        //     "searchable": false
        // }],
        //"order": [[6, "asc"]],
        "fnDrawCallback": function () {
            //jQuery.fn.editable.defaults.mode = 'inline';
            $.fn.editableform.buttons =
                    '<button type="submit" class="btn btn-success editable-submit btn-mini"><span class="fa fa-check"></span></button>' +
                    '<button type="button" class="btn editable-cancel btn-mini"><span class="fa fa-times"></span></button>';
        },
        "language": {
            "emptyTable": "{!! trans('admin/common.datatable.empty_table') !!}",
            "info": "{!! trans('admin/common.datatable.info') !!}",
            "infoEmpty": "{!! trans('admin/common.datatable.info_empty') !!}",
            "infoFiltered": "({!! trans('admin/common.datatable.info_filtered') !!})",
            "lengthMenu": "{!! trans('admin/common.datatable.length_menu') !!}",
            "loadingRecords": "{!! trans('admin/common.datatable.loading') !!}",
            "processing": "{!! trans('admin/common.datatable.processing') !!}",
            "search": "{!! trans('admin/common.datatable.search') !!}:",
            "zeroRecords": "{!! trans('admin/common.datatable.zero_records') !!}",
            "paginate": {
                "first": "{!! trans('admin/common.datatable.first') !!}",
                "last": "{!! trans('admin/common.datatable.last') !!}",
                "next": "{!! trans('admin/common.datatable.next') !!}",
                "previous": "{!! trans('admin/common.datatable.previous') !!}"
            },
        }
    });

    $('.search-input-select').on('change', function () {   // for select box
        var i = $(this).attr('data-column');
        var v = $(this).val();
        oTable.columns(i).search(v).draw();
    });

    $(document).on('click','.accepted',function(){
        $.ajax({
            url : "{!! url('admin/refund') !!}",
            method :  'POST',
            data : { id : $(this).data('id'), 'req_type' : 1},
            success : function(response){
               alert(response.message);
               location.reload(true);
            },
            error : function(err){
                alert(err.responseJSON.message);
            }
        });
        return false;
    });

    $(document).on('click','.delete-btn',function(){
        $.ajax({
            url : "{!! url('admin/refund') !!}",
            method :  'POST',
            data : { id : $(this).data('id'), 'req_type' : 2},
            success : function(response){
               alert(response.message);
               location.reload(true);
            },
            error : function(err){
                alert(err.responseJSON.message);
            }
        });
        return false;
    });
    
});
</script>
@stop