<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Devrahul\Signinupapi\Models\User;

class OtpUpdateForgetPasswordAndNoOfAttemptsRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    protected $message ="";
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $phoneOtpData = User::where('id','=',$value)->first();
        if($phoneOtpData == null)
            return false;
        if($phoneOtpData->is_verified == 0)
        {
            if($phoneOtpData->password_otp != 0)
                { // if no is not verified then enter the condition
                    if($phoneOtpData->no_of_attempts < 3)
                    {// if no of attempts is less then the 3 
                        $phoneOtpData->no_of_attempts = $phoneOtpData->no_of_attempts + 1;
                        $phoneOtpData->save();
                        return true;
                    }
                    else{
                        $this->message="No of attempts exceeded";
                        $phoneOtpData->password_otp = 0;
                        $phoneOtpData->no_of_attempts=0;
                        $phoneOtpData->save();
                        return false;
                    }
                }
                else{
                    $this->message="Please send OTP first";
                    return false;
                }
        
        }
        else
        {
            $this->message ="This OTP is already Verified";
            return false;
        }

        
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return  $this->message();
    }
}
