<?php 

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\GetInTouchRequest;
use App\Models\GetInTouch;
use App\Jobs\SendEmailJob;
use Auth;
use DB;
use Illuminate\Mail\Mailer;
use Mail;
use App\Mail\GetInTouchMail;
use App\Helpers\Response;
use App\Setting;

class HelpAndSupportController extends controller{
    

    
    /**
     * @SWG\Post(
     *     path="/getintouch",
     *     tags={"Help and Support"},
     *     summary="Get In Touch Form in Help and Support Page",
     *     description="A mail will be sent to the admin and will be added to the database.",
     *     operationId="getintouch",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Accept Ride object",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="ride_id",
     *              type="integer"
     *             ),
     *              @SWG\Property(
     *             property="email",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="subject",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="description",
     *              type="string"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */

    public function getintouch(GetInTouchRequest $request){
        try{
                DB::beginTransaction();
            // Adding data to the Database Table : get_in_touch
                $getintouch = new GetInTouch();
                $getintouch->email = $request['email'];
                $getintouch->user_id = Auth::User()->id;
                $getintouch->ride_id = $request['ride_id'];
                $getintouch->subject = $request['subject'];
                $getintouch->description = $request['description'];
                $getintouch->save();
                DB::commit();

            // code to send email to be added here
                $Emaildata['email'] = $getintouch->email ;
                $Emaildata['ride_id'] = $getintouch->ride_id ;
                $Emaildata['subject'] = $getintouch->subject ;
                $Emaildata['description'] = $getintouch->description ;

                
            // code of mail to sent ends here
                $mailAdmin = Setting::select('email')->first();
                Mail::to($mailAdmin)
                    ->send(new GetInTouchMail($Emaildata));


            // serving response to the user
                $this->response['message'] = "Your request is sent successfully"; 
                return response($this->response, Response::HTTP_OK);
        }
        catch(Exception $ex)
        {
            $this->response['message'] = 'Server Internal Error.';
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
    }
}
