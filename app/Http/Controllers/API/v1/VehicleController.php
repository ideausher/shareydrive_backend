<?php

namespace App\Http\Controllers\API\v1;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Response;
use App\Models\Vehicle;
use App\Http\Requests\AddVehicleReq;
use Auth;
use DB;

class VehicleController extends Controller
{
    protected $response = [
        'message' => '',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        
        $this->response['data'] = new \stdClass();
    }

    protected function setData($complexObject)
    {
        $json = json_encode($complexObject);
        $encodedString = preg_replace('/null/', '" "' , $json);
        $this->response['data'] = json_decode($encodedString);
        return $this->response['data'];
    }

    /**
     * @SWG\Post(
     *     path="/vehicle",
     *     tags={"Vehicle"},
     *     summary="Create User New Vehicle or Update existing details..",
     *     description="Create or Update User New Vehicle using API's",
     *     operationId="createVehicle",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Create Vehicle object",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="rego",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="car_manufacture",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="car_model",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="car_type",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="car_color",
     *              type="string"
     *             ),  
     *             @SWG\Property(
     *              property="register_year",
     *              type="string"
     *             ),   
     *             @SWG\Property(
     *              property="register_state",
     *              type="string"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function create(AddVehicleReq $request){
        try{
            
            /** Start Transaction here while create users **/
            DB::beginTransaction();   
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;

            $vehicle = Vehicle::create($data);

            /** Submit Transaction here if every thing working **/
            DB::commit();  
            $this->response['message'] = "Vehicle has been added successfully."; 
            $this->setData($vehicle);
            return response($this->response, Response::HTTP_OK);
            
        } catch (\Exception $ex) {
            /*** Role back all queries for fresh entry ***/
            DB::rollBack();
            $this->response['message'] = 'Server Internal Error.';
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @SWG\Get(
     *     path="/vehicle",
     *     tags={"Vehicle"},
     *     summary="Get User Vehicle Details.",
     *     description="Get user vehicle details by Id using API's",
     *     operationId="getVehicle",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function getVehicle(Request $request){

        try {
        
            $vehicle = Vehicle::where('user_id',Auth::user()->id)
                                ->latest()->first();
            if(isset($vehicle)){
                $this->response['message'] = 'Record Found.'; 
                $this->response['data'] = $vehicle;
                return response($this->response, Response::HTTP_OK);
            }
            else{
                $this->response['message'] = trans("No Record Found");    
                return response($this->response, Response::HTTP_ACCEPTED);
            }
            

        }
        catch (\Exception $ex) {

            $this->response['message'] = 'Something went wrong.';
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        } 

    }
}
