<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Ultraware\Roles\Traits\HasRoleAndPermission;
use Ultraware\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Illuminate\Support\Facades\Hash;
use \Carbon\Carbon;
use DateTime;
use App\Models\Review;

class User extends Authenticatable implements HasRoleAndPermissionContract
{
    use Notifiable,HasRoleAndPermission;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname','phone_number','stripe_custmer_id','gender','bio','phone_country_code','image', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $appends = ['userage','ratingasdriver','ratingaspassenger'];
/**
     * Accessor for Age.
     */
    public function getUserageAttribute()
    {
        if(isset($this->attributes['date_of_birth'])){
            $datetime1 = new DateTime();
            $datetime2 = new DateTime($this->attributes['date_of_birth']);
            $interval = $datetime1->diff($datetime2);
            // time in minutes
            $Userage =  $interval->format('%y');
        }
        $Userage = 0;
        return  $Userage;
    }

    public function getRatingasdriverAttribute()
    {
        $rating = Review::where('user_id',$this->id)
                        ->where('type','1')
                        ->avg('rating');
        return  $rating ? $rating : 0;
    }
    public function getRatingaspassengerAttribute()
    {
       $rating = Review::where('user_id',$this->id)
                        ->where('type','2')
                        ->avg('rating');
        return  $rating ? $rating : 0;
    }
    
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }


    public function scopeActive($query) {
        return $query->whereStatus('1');
    }
    
    public function scopeOnline($query) {
        return $query->whereOnline('1');
    }
	
	
		

}
