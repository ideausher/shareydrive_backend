@extends('admin.layouts.default')
{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/servicecategory.categories') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>





{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Send Notification</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
            </div>
            <div class="col-xs-12">
                <div class="box">
                    @if(isset($category))
                    {!! Form::model($category, array('route' => array('sendNotification'), 'method' => 'POST', 'id' => 'service-form-1', 'files' => true )) !!}
                    @else
                    {!! Form::open(array('url' => 'admin/sendNotifications', 'id' => 'add-vendor-form', 'files' => true)) !!}
                    @endif
                    
                    <div class="box-body">
                        <div class="row">
                        <div class="col-xs-12">
                            <div class="row justify-content-end">
                            <div class="flavour-details">
                                <div class="col-xs-12 col-md-12">
                                    
                                    <div class="form-group has-feedback">
                                        {!! Form::label('users', trans('Users')) !!}
                                        <select class="js-example-basic-multiple" name="users[]" multiple="multiple">
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}">({{ $user->id }}) {{ $user->firstname }} {{ $user->lastname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('title', 'Title') !!}
                                {!! Form::textarea('title',null,['class'=>'form-control','id'=>'title', 'rows' => 2]) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('message', 'Message') !!}
                                {!! Form::textarea('message',null,['class'=>'form-control','id'=>'message', 'rows' => 2]) !!}
                                
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>     
                        </div>
                        <div class="row">
                        <div class="box-footer">
                            {!! Form::submit('Submit',array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}
                            <a href="{!! url('admin/notifications/all') !!}" class="btn btn-default">{!! 'Cancel' !!}</a>
                        </div>
                        </div>
                        {!! Form::close()!!}
                    </div> <!-- /.box -->
                </div> <!-- /.col-xs-12 -->
            </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@stop
{{-- Scripts --}}
@section('scripts')
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXMpUMMrjVgbWeWF99SfuFQhe06-ST62s&libraries=places&callback=initMap" async defer></script>

@stop