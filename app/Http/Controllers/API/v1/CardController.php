<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AddCard;
use App\Http\Requests\AddBankReq;
use App\Helpers\Response;
use App\Models\Bank;
use App\Card;
use Stripe\Stripe;
use DB;

class CardController extends Controller
{
    protected $response = [
        'message' => '',
    ];

    public function __construct() {
        
        $this->response['data'] = new \stdClass();
    }
    protected function setData($complexObject)
    {
        $json = json_encode($complexObject);
        $encodedString = preg_replace('/null/', '" "' , $json);
        $this->response['data'] = json_decode($encodedString);
        return $this->response['data'];
    }
        
    /**
     * @SWG\Get(
     *     path="/card",
     *     tags={"Card"},
     *     summary="Get user card list",
     *     description="Get user cards detail for payments",
     *     operationId="getCard",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function getCard(Request $request){
        try{
            Stripe::setApiKey(env("STRIPE_SECRET"));
            $user = Auth::user();
            if($user->stripe_custmer_id){
                $card = \Stripe\Customer::allSources(
                    $user->stripe_custmer_id
                );
                
                if($card->isEmpty()){
                    $this->response['status'] = 0;
                    $this->response['message'] = trans('No Record Found');
                    $this->response['data'] = [];
                    return response()->json($this->response, 200);
                }
            
                $this->response['status'] = 1;
                $this->response['message'] = trans('Records Found');
                $this->response['data'] = $this->setData($card['data']);
                return response()->json($this->response, 200);
            }

        }
        catch (\Exception $ex) {
            
            $this->response['message'] = $ex->getMessage();
            return response($this->response, 500);
        }
    }

    /**
     * @SWG\Post(
     *     path="/bank",
     *     tags={"Card"},
     *     summary="Save bank details",
     *     description="Save bank details",
     *     operationId="saveBank",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Save Bank object",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="account_holder_name",
     *              type="string"
     *             ),
     *              @SWG\Property(
     *             property="account_number",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="bsb_no",
     *              type="string"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function saveBank(AddBankReq $request){
        try{

            /** Start Transaction here while create offer ride **/
            DB::beginTransaction();   

            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $data['details'] = json_encode([
                'account_holder_name' => $data['account_holder_name'],
                'account_number' => $data['account_number'],
                // 'bank_name' => $data['bank_name'],
                'bsb_no' => $data['bsb_no']
            ]);

                $bank = Bank::Create(['user_id' => $data['user_id'], 'details'=>$data['details']]);
            
             /** Submit Transaction here if every thing working **/
            DB::commit(); 

            $this->response['message'] = "Bank details is added successfully."; 
            $this->response['data'] = $bank;
            return response($this->response, Response::HTTP_OK);
        }
        catch(Exception $ex)
        {
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @SWG\Get(
     *     path="/bank",
     *     tags={"Card"},
     *     summary="Get user bank details",
     *     description="Get user bank detail",
     *     operationId="getBank",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function getBank(Request $request){
        try {
            
            $bank = Bank::where('user_id',Auth::user()->id)
                                ->get();
            if(count($bank) > 0){
                $this->response['message'] = trans("Bank Detail Found"); 
                $this->response['data'] = $this->setData($bank);
                return response($this->response, Response::HTTP_OK);
            }

            $this->response['message'] = trans("No Data Found");    
            return response($this->response, Response::HTTP_ACCEPTED);

        }
        catch (\Exception $ex) {

            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }

}
