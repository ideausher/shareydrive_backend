<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Setting as Setting;
use App\Helpers\Common as Common;
use App\Helpers\Datatable\SSP;
use Validator;
use File;
use App\Faq;
use App\Http\Requests\FaqSaveEdit;

class SettingsController extends Controller {

    /**
     * Setting Model
     * @var Setting
     */
    protected $setting;

    /**
     * Inject the models.
     * @param Setting $setting
     */
    public function __construct(Setting $setting) {
        $this->setting = $setting;
       
    }

    /**
     * Display a listing of settings
     *
     * @return Response
     */
    public function index() {

        // Grab all the settings
        $settings = Setting::first();
            
        if($settings){
            return redirect()->route('settings.edit',$settings->id);
        }else{
            return redirect()->route('settings.create');
        }
    }

    /**
     * Show the form for creating a new setting
     *
     * @return Response
     */
    public function create() {
        $settings = Setting::first();
            
        if($settings){
            return redirect()->route('settings.edit',$settings->id);
        }else{
            return view('admin.settings');
        }
        
    }

    /**
     * Store a newly created setting in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $rules = array(
            'language' => 'required',
            'site_title' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'logo' => 'mimes:jpeg,jpg,png,gif,bmp|max:3072'
        );
        $data = $request->all();
        
        //add http/https to url
        $data['facebook'] = $data['facebook'] != "" ? Common::addScheme($data['facebook']) : "";
        $data['twitter'] = $data['twitter'] != "" ? Common::addScheme($data['twitter']) : "";
        $data['linkedin'] = $data['linkedin'] != "" ? Common::addScheme($data['linkedin']) : "";
        $data['googleplus'] = $data['googleplus'] != "" ? Common::addScheme($data['googleplus']) : "";
        
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $setting = Setting::create($data);
        $lastInsertId = $setting->id;
        if ($request->hasFile('logo')) {
            if ($request->file('logo')->isValid()) {
                $file = $request->file('logo');
                $ext = $file->getClientOriginalExtension();
                $filename = 'logo' . '.' . $ext;
                $targetPath = \Config::get('constants.LOGO_PATH');
                $file->move($targetPath, $filename);
            } else {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            
            $setting->logo = $filename;
            $setting->save();
        }
        
        return redirect()->route('settings.edit',$lastInsertId)->with('success_message', trans('admin/generalSettings.settings_add_message'));
    }

    /**
     * Show the form for editing the specified setting.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $setting = Setting::find($id);
        if ($setting) {
            return view('admin/settings', compact('setting'));
        } else {
            return redirect('admin/settings')->with('error_message', trans('admin/generalSettings.settings_invalid_message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $rules = array(
            'site_title' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'phone' => 'required'
        );
        $setting = Setting::findOrFail($id);
        $data = $request->all();
        
        //add http/https to url
       
        
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        
        $setting->update($data);
        
        return redirect()->back()->with('success_message', trans('admin/generalSettings.settings_update_message'));
    }

    /**
     * Function to show the faq page
     * @param {}
     * @return View
     * @author Durga Parshad
     * @since 03-Jan-2020
     */
    public function faq(){
        return view('admin.faq');
    }

    /**
     * Function to get faq data for datatables
     * @param {}
     * @return JSON
     * @author Durga Parshad
     * @since 04-Jan-2020
     */
    public function faqData()
    {
        try{
            $table = 'faq';
            $primaryKey = 'id';
            $columns = array(
                array('db' => 'question', 'dt' => 0, 'field' => 'question'), 
                array('db' => 'answer', 'dt' => 1, 'field' => 'answer'),
                array('db' => 'language', 'dt' => 2, 'field' => 'language'),
                array('db' => 'id', 'dt' => 3, 'field' => 'id','formatter' => function( $d, $row ) {
                    $operation = ' <a href="faq/' . $d . '/edit" class="btn btn-primary d-inline-block" title="' . trans('admin/common.edit') . '" data-toggle="tooltip"><span class="fa fa-pencil"></span></a>';
                    $operation .= '<a href="#" data-id="' . $d . '" class="btn btn-danger delete-btn" title="' . trans('admin/common.delete') . '" data-toggle="tooltip"><span class="fa fa-times"></span></a>';
                    return $operation;
                }),
            );

            $sql_details = array(
                'user' => config('database.connections.mysql.username'),
                'pass' => config('database.connections.mysql.password'),
                'db' => config('database.connections.mysql.database'),
                'host' => config('database.connections.mysql.host')
            );
    
            $joinQuery = "";
            $extraWhere = ""; //"r.name='doctor'";
            $groupBy = "";//"users.id";

            echo json_encode(
                    SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
            );
        }
        catch (\Exception $ex) {

            $this->response['message'] = trans('api/user.something_wrong');
            return response($this->response, 500);
        }
        
    }  
    
    /**
     * Function to add/edit faq screen
     * @param 'id','action'
     * @return JSON
     * @author Durga Parshad
     * @since 06-Jan-2020
     */
    public function faqEdit(Request $request,$id){
        $data = Faq::find($id);
        return view('admin.faqForm',compact('id','data'));
    }

    /**
     * Function to add/edit faq screen
     * @param 'id','action'
     * @return JSON
     * @author Durga Parshad
     * @since 06-Jan-2020
     */
    public function faqSaveEdit(FaqSaveEdit $request,$id){
        try{

            $faq = Faq::find($id);
            $faq->language = $request['language'];
            $faq->question = $request['question'];
            $faq->answer = $request['answer'];
            $faq->save();
            return back()->with(['status' => trans('admin/faq.update_succes'),'alert' => 'alert-success']);
        }
        catch (\Exception $ex) {
            return back()->with(['status' => trans('api/user.something_wrong'),'alert' => 'alert-warning']);
        }
    }

    /**
     * Function to add faq screen
     * @param 'id','action'
     * @return JSON
     * @author Durga Parshad
     * @since 06-Jan-2020
     */
    public function faqAdd(Request $request){
        return view('admin.faqForm');
    }

    /**
     * Function to save new entry faq
     * @param 'id','action'
     * @return JSON
     * @author Durga Parshad
     * @since 06-Jan-2020
     */
    public function faqSaveAdd(FaqSaveEdit $request){
        try{
            $data = $request->all();
            $data['user_id'] = auth()->guard('admin')->user()->id;
            Faq::create($data);
            return back()->with(['status' => trans('admin/faq.added_success'),'alert' => 'alert-success']);
        }
        catch (\Exception $ex) {
            return back()->with(['status' => trans('api/user.something_wrong'),'alert' => 'alert-warning']);
        }
    }

    /**
     * Function to delete particular faq
     * @param 'id'
     * @return JSON
     * @author Durga Parshad
     * @since 06-Jan-2020
     */
    public function faqDelete(Request $request,$id){
        try{
            $faq = Faq::find($id);
            $faq->delete();
            return response(['status' => 'success' , 'message' => trans('admin/faq.delete_success')], 200); 

        }
        catch (\Exception $ex) {
            return response(['status' => 'failed' , 'message' => trans('api/user.something_wrong')], 500); 
        }
    }

    public function showReferral(){
        $referral_code_text = Setting::where([
            'id' => 1
        ])->first();
        $data =   $referral_code_text;
        return view('admin.showReferral',compact('data'));
    }

    public function updateReferral(Request $request){
        try{
            $setting = Setting::where([
                'id' => 1
            ])->update(['referral_code_text' => $request['referral_code_text'],'referral_code_discount' => $request['referral_code_discount']]);
            return back()->with('success_message', 'Referral Code Text Updated Successfully');
            
        }
        catch (\Exception $ex) {
            return back()->with('error_message', 'Something Went Wrong');
        }
    }


    public function showRideSettings(){
        $referral_code_text = Setting::where([
            'id' => 1
        ])->first();
        $data =   $referral_code_text;
        return view('admin.showRideSettings',compact('data'));
    }

    public function updateRideSettings(Request $request){
        try{
          
            $setting = Setting::where([
                'id' => 1
            ])->update(['sug_price_value' => $request['sug_price_value'],'ride_time_diff_search' => $request['ride_time_diff_search'],'before_pass_cancel_time' => $request['before_pass_cancel_time'],'before_driv_cancel_time' => $request['before_driv_cancel_time']]);
            return back()->with('success_message', 'Ride Related Settings Updated Successfully');

        }
        catch (\Exception $ex) {
            return back()->with('error_message', 'Something Went Wrong');
        }
    }
    
}
