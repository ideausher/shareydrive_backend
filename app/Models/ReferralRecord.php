<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Referral Record"))
 */

class ReferralRecord extends Model
{
    
    /**
     * @var string
     * @SWG\Property(
     *   property="user_id_one",
     *   type="integer" 
     * )
     *  @SWG\Property(
     *   property="user_id_two",
     *   type="integer" 
     * )
     * @SWG\Property(
     *   property="code",
     *   type="string" 
     * )
     */

    protected $table ='referral_record';
    protected $fillable = [
        'user_id_one','user_id_two','code'
    ];

    

}
