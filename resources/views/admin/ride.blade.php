@extends('admin.layouts.default')
{{-- Web site Title --}}
@section('title')
@parent :: {!! 'Ride Management' !!}
@stop
@section('styles')
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if(isset($ride_id))
        <h1>{!! 'Edit Ride' !!}</h1>
        @else
        <h1>{!! 'Add Ride' !!}</h1>
        @endif
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
            </div>
            <div class="col-xs-12">
                <div class="box">
                    @if(isset($ride_id))
                    {!! Form::model($ride_id, array('route' => array('admin/rides-management/ride'), 'method' => 'PUT', 'id' => 'edit-ride' )) !!}
                    {!! Form::hidden('id', $ride_id ,array('class'=>'form-control', 'id' => 'ride_id')) !!}
                    @else
                    {!! Form::open(array('url' => 'admin/rides-management/add-ride', 'id' => 'add-ride')) !!}
                    @endif
                    
                    <div class="box-body">
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('first_name', trans('admin/vendors.firstname')) !!}
                                {!! Form::text('first_name', old('first_name'), array('class'=>'form-control')) !!}
                                {!! Form::hidden('role_id',1) !!}
                                @if(isset($category->id))
                                <input type="hidden" name="id" value="{{ $category->id }}">
                                @endif
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('last_name', trans('admin/vendors.lastname')) !!}
                                {!! Form::text('last_name', old('last_name'), array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('email', trans('admin/vendors.email')) !!}
                                {!! Form::text('email', old('email'), array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('password', trans('admin/vendors.password')) !!}
                                {!! Form::password('password', array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('confirm_password', trans('admin/vendors.confirm_password')) !!}
                                {!! Form::password('confirm_password',array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('gender', trans('admin/vendors.gender')) !!}
                                {!! Form::select('gender', array('male' => 'Male', 'female' => 'Female'),null,array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('phone', trans('admin/vendors.Phone_no')) !!}
                                {!! Form::text('phone','',array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('country_code', trans('admin/user.country_code')) !!}
                                {!! Form::text('country_code',null,array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                       
                         <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('blood_group', trans('admin/user.blood_group')) !!}
                                {!! Form::text('blood_group',null,array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
    
                        <div class="img-details">
                            <div class="col-xs-12">
                                <div class="form-group has-feedback">
                                    {!! Form::label('image', trans('admin/vendors.add_image')) !!}
                                    <div class=row>

                                        <div class="col-md-10">
                                            {!! Form::file('image', array('id'=>'change-image','data-view'=>'uploaded-image','class'=>'form-control','style'=>'height:auto;')) !!}
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-2">
                                            <img id="uploaded-image" data-src="{!! URL::asset('uploads/user/default.png') !!}" src="{!! URL::asset('uploads/user/default.png') !!}" alt="image"  height="100" width="100">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="box-footer">
                            {!! Form::submit(trans('admin/common.submit'),array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}
                            <a href="{!! url('admin/patients') !!}" class="btn btn-default">{!! trans('admin/common.cancel') !!}</a>
                        </div>
                        {!! Form::close()!!}
                    </div> <!-- /.box -->
                </div> <!-- /.col-xs-12 -->
            </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@stop
{{-- Scripts --}}
@section('scripts')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXMpUMMrjVgbWeWF99SfuFQhe06-ST62s&libraries=places&callback=initMap" async defer></script>
@stop