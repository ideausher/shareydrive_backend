<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Rides"))
 */

class Rides extends Model
{
    /**
     * @var string
     * @SWG\Property(
     *   property="ride_date_time",
     *   type="string" 
     * )
     *  @SWG\Property(
     *   property="vehicle_id",
     *   type="integer" 
     * )
     * @SWG\Property(
     *   property="max_no_seats",
     *   type="integer" 
     * )
     * @SWG\Property(
     *   property="price_per_seats",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="total_toll_tax",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="start_latitude",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="start_longitude",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="start_country",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="start_place_id",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="start_city",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="start_state",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="start_full_address",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="end_latitude",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="end_longitude",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="end_country",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="end_place_id",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="end_city",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="end_state",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="end_full_address",
     *   type="string" 
     * )
     */

    protected $table = 'ride';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'ride_date_time', 'max_no_seats', 'price_per_seat','total_toll_tax','status','vehicle_id'
    ];

     /**
     * Append new column to response
     * 
     */
    protected $appends = ['consume_seat_sum','accepted_seats'];


    /**
     * Attach sum of consume seats in rides
     */
    public function getconsumeSeatSumAttribute(){
        return $this->rideAccepted->sum('consume_seats');
    }
    public function getacceptedSeatsAttribute(){
        return $this->rideAccepted->where('is_accepted_by_driver',1)->count();
    }
    

    /**
     * Relation with ride address
     * Start Location
     */
    public function startLocation(){
        return $this->hasOne('App\Models\RideAddress','ride_id','id')
                    ->where('type',1);
    }       

    /**
     * Relation with ride address 
     * End Location
     */
    public function endLocation(){
        return $this->hasOne('App\Models\RideAddress','ride_id','id')
                    ->where('type',2);
    }

    /**
     * Relation with ride accepted 
     * 
     */
    public function rideAccepted(){
        return $this->hasMany('App\Models\RideAccepted','ride_id','id')->join('users','user_id','users.id')->selectRaw('ride_id,user_id,is_accepted_by_driver,ride_accepted.status,consume_seats,firstname,lastname,image,social_image');
    }

    public function vehicleinformation(){
        return $this->hasMany('App\Models\Vehicle', 'id','vehicle_id')->latest();
    }
    
    public function userReviews(){
        return $this->hasMany('App\Models\Review','ride_id','id');
    }

    public function userRatingAvg(){
        return $this->hasMany('App\Models\Review','ride_id','id')
                    ->where('type',2);
    }

    public function userDetails(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function ridesRequest(){
        return $this->hasMany('App\RideRequest','ride_id','id');
    }
}
