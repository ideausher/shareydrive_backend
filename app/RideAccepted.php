<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RideAccepted extends Model
{
    //
	protected $table = 'ride_accepted';
}
