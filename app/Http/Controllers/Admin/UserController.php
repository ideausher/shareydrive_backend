<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Datatable\SSP;
use App\Http\Controllers\Controller;
use App\User;
use App\phoneOtp;
use App\UserAddresses;
use File;
use Validator;
use Illuminate\Http\Request;
use App\Ride;
use App\RideAccepted;
use App\Review;
use App\Transaction;
use DB;
use App\ReferralRecord;

class UserController extends Controller {

    /**
     * User Model
     * @var User
     */
    protected $user;
    protected $pageLimit;

    /**
     * Inject the models.
     * @param User $user
     */
    public function __construct(User $user) {
        
        $this->user = $user;
        
        $this->pageLimit = config('settings.pageLimit');
        
    }

    /**
     * Display a listing of user
     *
     * @return Response
     */
    public function index() {
        
        // Show the page
        return view('admin/userList');
    }
    

    /**
     * Display the specified user.
     *
     * @param  int  $id
     * @return Response
     */
    
    public function show($id) {

        $user = User::findOrFail($id);
        
        return view('admin/userDetails', compact('user'));
    }
    public function showuser($id) {

        $user['user'] = User::findOrFail($id);
        
       //$user['userAddress']=UserAddresses::where(['user_id'=> $id])->get();
        return view('admin/patientView',$user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
	
        //$user = User::with('userAddress')->find($id);
        $user = User::find($id);
        // $home_addresses = $user->userAddress()->where('address_type', 'LIKE', '%home%')->get();
        // $office_addresses = $user->userAddress()->where('address_type', 'LIKE',  '%office%')->get();
        // $other_addresses = $user->userAddress()->where('address_type', 'LIKE',  '%other%')->get();
        // $work_addresses = $user->userAddress()->where('address_type', 'LIKE',  '%work%')->get();

        if ($user) {
            return view('admin/edituser', compact('user'));
            // return view('admin/edituser', compact('user', 'home_addresses', 'office_addresses', 'other_addresses', 'work_addresses'));
        }
    }
	public function userReport(Request $request)
	{
		if($request->all())
		{
			$from=date('Y-m-d H:i:s',strtotime($request['from']));
			$to=date('Y-m-d H:i:s',strtotime($request['to']));
        $data['from']=$request['from'];
        $data['to']=$request['to'];
			$data['totalNewUser']=User::whereBetween('created_at',array($from,$to))->count();
		
			$data['totalEarnings']=Transaction::whereBetween('created_at',array($from,$to))->count();
			$data['totalRideOffered']=Ride::whereBetween('created_at',array($from,$to))->count();
			$data['totalPassengerTraveled']=RideAccepted::whereBetween('created_at',array($from,$to))->distinct()->count('user_id');
			$data['totalReferalCode']=ReferralRecord::whereBetween('created_at',array($from,$to))->count();
			
			 $currentdate=date('Y-m-d H:i:s',strtotime("-12 months"));
			
			$id=$request['id'];
			$data['user']=User::all();
			               $rides = DB::table('ride')
                            ->where('ride_date_time','>=',"$from")
                            ->where('ride_date_time','<=',"$to")
                    ->select(

                        DB::raw("monthname(ride_date_time) as month"),

                        DB::raw("count(id) as ride_offered"),
						DB::raw("(select count(*) from ride )as total")) 

                    ->orderBy("ride_date_time")

                    ->groupBy(DB::raw("monthname(ride_date_time)"))

                    ->get();

// echo "<pre>";print_r($rides);exit;
$data['ridesEmpty']=0;
if($rides->isEmpty())
{
    $data['ridesEmpty']=1;
}

        $result[] = ['Month','Rides'];

        foreach ($rides as $key => $value) {

            $result[++$key] = [$value->month, (int)$value->ride_offered];

        }
		
		
		
		$earning=DB::table('transactions')
        ->where('created_at','>=',"$from")
        ->where('created_at','<=',"$to")
		->select(
				DB::raw('monthname(created_at) as month'),
				DB::raw("sum(amount) as amount"),
				DB::raw("(select sum(amount) from transactions) as total"))
				->orderBy("created_at")
				->groupby(DB::raw("monthname(created_at)"))
				->get();
                
                $data['earningEmpty']=0;
                if($earning->isEmpty())
{
    $data['earningEmpty']=1;
}
				$earning_result[]=['Month','Earnings'];
				
				foreach($earning as $key=> $value)
				{
					$earning_result[++$key]=[$value->month,(int)$value->amount];
				}
      
                // echo "<pre>";
                // print_r($rides);
                // exit;

        return view('admin/userReport',$data)

                ->with('rides',json_encode($result))
				->with('earning',json_encode($earning_result));

			
		}
		else{
			$data['user']=User::all();
			return view('admin/userReport',$data);
		}
	}
	
	public function view($id) {
		
        //$user = User::with('userAddress')->find($id);
        $user = User::find($id);
		if($user)
		{
		$data['total_ride']=Ride::where('user_id',$id)->count();
		$data['total_ride_taken']=RideAccepted::where('user_id',$id)
		->where('is_accepted_by_driver','1')
		->whereIn('status',array(1,2))->count();
       $data['rating_as_driver']=Review::where('user_id',$id)->where('type',1)->avg('rating');
	   $data['rating_as_passenger']=Review::where('user_id',$id)->where('type',2)->avg('rating');
	   $data['total_money_earned']=Ride::where('user_id',$id)->sum('price_per_seat');
	   $data['total_money_spent']=Transaction::where('user_id',$id)->sum('amount');
	   $data['review_as_passenger']=Review::where('user_id',$id)->where('type',2)->get();
	   $data['review_as_driver']=Review::where('user_id',$id)->where('type',1)->get();
	   $data['user']=$user;
      
            return view('admin/userPerformance',$data);
           
        }
    }
    
    public function Add() {
        
        $users = User::orderBy('created_at', 'Asc')->get();
        return view('admin/addUser', compact('users'));
    }
    public function AddUser(Request $request)
    {
    
            $data = $request->all();
            
            
            $validator = Validator::make($request->all(), [
                    'first_name' => 'required|string|max:45',
                    'last_name' => 'required_if:role_id,==,2',
                    'email' => 'required|string|email|max:255',
                    'password' => 'required|string|min:6|same:confirm_password',
                    'phone' => 'required',
                    'gender' => 'required',
                    'image' => 'required||mimes:jpeg,png,jpg,gif,svg||max:4096',
                    'country_code'=>'required',
                    'bio'=>'required|string|max:255',
        ],[
            
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
                 if ($request->hasFile('image')) { //check the file present or not
            $rules = array('image' => 'mimes:jpeg,png,jpg,gif,svg||max:4096');
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $image = $request->file('image'); //get the file
            $data['image'] = uniqid() . '.' . $image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = 'images/avatars/'; //public path folder dir
            if (!$image->move($destinationPath, $data['image'])) {
                echo "image not uploaded correclty! Try Later";
                die;
            } //mve to destination you mentioned
        }
        $user_check = User::where('email', $data['email'])->first();
        if(!$user_check)
        {
        
        $user = User::create([
                        'firstname' => $data['first_name'],
                        'lastname' => $data['last_name'],
                        'email' => $data['email'],
                        'password' => $data['password'],
                        'phone_number' => $data['phone'],
                        'gender' => $data['gender'],
                        'bio' => $data['bio'],
                        'image'=> $data['image'],
                        'phone_country_code'=> $data['country_code'],
                        'stripe_custmer_id' => '',
        ]);
        
        
        $user->attachRole(1);
        
        return redirect('admin/users')->with('success_message', trans('User Created Successfully'));

        }
        else{
            return redirect()->back()->withErrors("User with same E-Mail Id already exist")->withInput();
        }
        
                
    }

    //save edit data
    public function update(Request $request, $id) {
        $user = User::findOrFail($id);
        $data = $request->all();

        $rules = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'gender' => 'required',
            'phone_number' => 'required',
            'bio' => 'required',
            'phone_country_code' => 'required'
            //'phone_number' => 'required|numeric|digits_between:10,15'
        );        
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        

        if ($request->hasFile('image')) { //check the file present or not
            $rules = array('image' => 'mimes:jpeg,png,jpg,gif,svg||max:4096');
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $image = $request->file('image'); //get the file
            $data['image'] = uniqid() . '.' . $image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = 'images/avatars/'; //public path folder dir
            if (!$image->move($destinationPath, $data['image'])) {
                echo "image not uploaded correclty! Try Later";
                die;
            } //mve to destination you mentioned
        }
        
        $user->update($data);

        $rules = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'gender' => 'required',
            'phone_number' => 'required|numeric',
        );
        
        // update user data
        
        // if (isset($data['add']['home'])) {
        //     $homeAdd = UserAddresses::findOrFail($data['add']['home']['id']);
        //     if ($homeAdd) {
        //         $addData = $data['add']['home'];
        //         $homeAdd->update($addData);
        //     }
        // }
        // if (isset($data['add']['office'])) {
        //     $homeAdd = UserAddresses::findOrFail($data['add']['office']['id']);
        //     if ($homeAdd) {
        //         $addData = $data['add']['office'];
        //         $homeAdd->update($addData);
        //     }
        // }
        // if (isset($data['add']['other'])) {
        //     $homeAdd = UserAddresses::findOrFail($data['add']['other']['id']);
        //     if ($homeAdd) {
        //         $addData = $data['add']['other'];
        //         $homeAdd->update($addData);
        //     }
        // }
        return redirect('admin/users')->with('success_message', 'User Details Updated Successfully');
    }

    public function destroy($id) {
        
        $user = User::findOrFail($id);
//        if ($user->phone_number) {
//            $existInOtpTable = phoneOtp::where(['phone_no' => $user->phone_number])->first();
//            phoneOtp::destroy($existInOtpTable->id);
//        }
        $oldFile = \Config::get('constants.USER_IMAGE_PATH') . $user->image;
        if (File::exists($oldFile)) {
            File::delete($oldFile);
        }
        \App\Models\Transaction::where('user_id',$id)->delete();
        \App\Models\Rides::where('user_id',$id)->delete();
        User::destroy($id);

        $array = array();
        $array['success'] = true;
        $array['message'] = trans('User Deleted Successfully');
        echo json_encode($array);
    }

    public function changeUserStatus(Request $request) {
        $data = $request->all();
        $user = User::find($data['id']);

        if ($user->status == User::pending || $user->status == User::inActive || $user->status == User::rejected) {
            $user->status = '1';
        } else {
            $user->status = '0';
        }
        $user->save();

        $array = array();
        $array['status'] = $user->status;
        $array['success'] = true;
        $array['message'] = trans('User Status Changed');
        echo json_encode($array);
    }

    /**
     * Change user credit of the specified user.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateCredit(Request $request) {

        $data = $request->all();
        $data['credit'] = $data['value'];
        $user = User::find($data['userId']);

        $user->update($data);
        $array = array();
        $array['success'] = true;
        session()->flash('success_message','Credit Updated Successfully');
        echo json_encode($array);
    }

    public function getUserData() {

        
        $table = 'users';

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'users.firstname', 'dt' => 0, 'field' => 'firstname'),
            array('db' => 'users.email', 'dt' => 1, 'field' => 'email'),
            array('db' => 'users.phone_number', 'dt' => 2, 'field' => 'phone_number'),
            array('db' => 'users.gender ', 'dt' => 3, 'formatter' => function( $d, $row ){
                        return $row['gender'] == 0 ? 'Male' : ($row['gender'] == 1 ? 'Female' : 'Other');
            }, 'field' => 'gender'),
            array('db' => 'users.bio', 'dt' => 5, 'field' => 'bio'),
            array('db' => 'users.image', 'dt' => 4, 'formatter' => function( $d, $row ) {
                    if(isset($row) && !is_null($row['image'])){
                        if(!file_exists('/images/avatars/'.$row['image'])){
                            return '<img id="uploaded-image" src="'.url('/images/avatars/'.$row['image']).'" alt="image" height="35" width="35">';    
                        }
                        else{
                            return '<img id="uploaded-image" src="'.url('/images/avatars/5e7c8139a77f8.png').'" alt="image" height="35" width="35">';    
                        }
                    }else{
                        return '<img id="uploaded-image" src="'.url('/images/avatars/5e7c8139a77f8.png').'" alt="image" height="35" width="35">';
                    }
            }, 'field' => 'image'),
            array('db' => 'users.id', 'dt' => 6, 'formatter' => function ($d, $row) {
                    $operation = '<a href="javascript:;" id="' . $d . '" class="btn btn-danger d-inline-block delete-btn" title="' . 'Delete'. '" data-toggle="tooltip"><span class="fa fa-times"></span></a>'
                            .'&nbsp &nbsp<a href="user/edit/' . $d . '" class="btn btn-primary d-inline-block" title="' . 'Edit' . '" data-toggle="tooltip"><span class="fa fa-pencil"></span></a>'
							.'&nbsp &nbsp<a href="user/view/' . $d . '" class="btn btn-primary d-inline-block" title="' . 'View' . '" data-toggle="tooltip"><span class="fa fa-eye"></span></a>';
                            //.' <a href="'.url("admin/users/showuser/$d").'" id="' . $d . '" class="btn btn-primary d-inline-block  view-btn" title="' . trans('admin/common.view') . '" data-toggle="tooltip"><span class="fa fa-eye"></span></a>';
                    return $operation;
                }, 'field' => 'id'),
        );

        $sql_details = array(
            'user' => config('database.connections.mysql.username'),
            'pass' => config('database.connections.mysql.password'),
            'db' => config('database.connections.mysql.database'),
            'host' => config('database.connections.mysql.host'),
        );
        $joinQuery = '';
//        $joinQuery = "LEFT JOIN (SELECT COUNT(*) AS total_bookings, user_id FROM bookings GROUP BY user_id ) as bk ON bk.user_id = users.id";
//        $joinQuery .= " LEFT JOIN (SELECT COUNT(*) AS total_transactions, user_id FROM transactions GROUP BY user_id ) as trans ON trans.user_id = users.id";
        $joinQuery .= " LEFT JOIN role_user ru ON ru.user_id = users.id";
        $joinQuery .= " LEFT JOIN roles r ON r.id = ru.role_id";
        // $joinQuery .= " LEFT JOIN user_addresses ur ON ur.user_id = users.id";
        $extraWhere = " r.name='User'";
        $groupBy = "";
        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
        );
    }

}
