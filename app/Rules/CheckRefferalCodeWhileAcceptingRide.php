<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\ReferralRecord;
use App\Models\Transaction;
use Auth;
use DB;


class CheckRefferalCodeWhileAcceptingRide implements Rule
{
    protected $message;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //checking if the refferal record contains the code or not if it contains then check whether its
        // applied or not
        $records = DB::table("referral_record")->select('*')
                                    ->whereNotIn('code',function($query){
                                                    $query->select('referral_code')->from('transactions');
                                    })
                                    ->where('user_id_two','=',Auth::User()->id)
                                    ->orWhere('user_id_one','=',Auth::User()->id)
                                    ->where('code','=',$value)
                                    ->get();
        
        if(count($records)>0){
            return true;
        }
        else{
            $this->message = "Referral Code Already Used or Invalid";
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
