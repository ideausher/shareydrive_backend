<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Devrahul\Signinupapi\Models\phoneOtp;

class ValidateOtpAttemptsRule implements Rule
{



    protected $message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        
        $phoneOtpData = phoneOtp::where('id','=',$value)->first();
        if($phoneOtpData == null)
            return false;
        if($phoneOtpData->is_verified==0)
        { // if no is not verified then enter the condition
            if($phoneOtpData->no_of_attempts<3)
            {// if no of attempts is less then the 3 
                $phoneOtpData->no_of_attempts = $phoneOtpData->no_of_attempts + 1;
                $phoneOtpData->is_verified = 0;
                $phoneOtpData->save();
                $noOfAttempts = $phoneOtpData->no_of_attempts;    
                return true;
            }
            else{
                $this->message="No of attempts exceeded";
            }
        }
        else{
            $this->message="OTP already Verified";
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
       return $this->message;;
    }
}
