@extends('admin.layouts.default')
@section('title')
@parent :: {!! trans('admin/dashboard.dashboard') !!}
@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Admin</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-fuchsia">
                    <div class="inner">
                        <h3>{!! $users !!}</h3>
                        <p>Total Users<br><br></p>
                    </div>
                    <div class="icon">
                        <span class="ion ion-person-add"></span>
                    </div>
                    <!-- <a href="{!!url('admin/patients')!!}" class="small-box-footer">See all Users &nbsp;<span class="fa fa-arrow-circle-right"></span></a> -->
                </div>
            </div><!-- ./col -->
           
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-fuchsia">
                    <div class="inner">
                        <h3>{!! $totalrides !!}</h3>
                        <p>Total Rides Published<br><br></p>
                    </div>
                    <div class="icon">
                        <span class="ion ion-person-add"></span>
                    </div>
                    <!-- <a href="{!!url('admin/doctors')!!}" class="small-box-footer">{!! trans('admin/dashboard.go_to') !!} <span class="fa fa-arrow-circle-right"></span></a> -->
                </div>
            </div><!-- ./col -->

            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>{!! $totalridescompleted !!}</h3>
                        <p>Total Rides Completed<br><br></p>
                    </div>
                    <div class="icon">
                        <span class="ion ion-bookmark"></span>
                    </div>
                    <!-- <a href="{!!url('admin/appointments')!!}" class="small-box-footer">{!! trans('admin/dashboard.go_to') !!} <span class="fa fa-arrow-circle-right"></span></a> -->
                </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-fuchsia">
                    <div class="inner">
                        <h3>{!! $transactions !!}</h3>
                        <p>Total Transactions<br><br></p>
                    </div>
                    <div class="icon">
                        <span class="ion ion-cash"></span>
                    </div>
                    <!-- <a href="{!!url('admin/transaction')!!}" class="small-box-footer">{!! trans('admin/dashboard.go_to') !!} <span class="fa fa-arrow-circle-right"></span></a> -->
                </div>
            </div>
            
            <!-- ./col -->
           <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>{!! @$totalsignupusingreferral !!}</h3>
                        <p>Total Signups using Referral Codes<br><br></p>
                    </div>
                    <div class="icon">
                        <span class="ion ion-email"></span>
                    </div>
                    <!-- <a href="{!!url('admin/enquiry')!!}" class="small-box-footer">{!! trans('admin/dashboard.respond_now') !!} <span class="fa fa-arrow-circle-right"></span></a> -->
                </div>
            </div>
            <!-- ./col -->
        </div><!-- /.row -->
        <!-- Main row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
