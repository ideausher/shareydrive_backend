@if (count($errors->all()) > 0)
<div class="alert alert-danger alert-block">
    <span class="fa fa-ban"></span>
    <button type="button" class="close" data-dismiss="alert"><span class="glyphicon glyphicon-remove"></span></button>
    <strong>{!! trans('admin/common.error') !!}</strong>
    <ul>
        @foreach($errors->all() as $message)
        <li>{{ $message }}</li>
        @endforeach
    </ul>
</div>
@endif

@if ($message = Session::get('success_message'))
<div class="alert alert-success alert-block session-box">
    <span class="fa fa-check"></span>
    <button type="button" class="close" data-dismiss="alert"><span class="glyphicon glyphicon-remove"></span></button>
    <strong>{!! trans('admin/common.success') !!}</strong>
    @if(is_array($message))
    @foreach ($message as $m)
    {{ $m }}
    @endforeach
    @else
    {{ $message }}
    @endif
</div>
@endif

@if ($message = Session::get('error_message'))
<div class="alert alert-danger alert-block session-box">
    <span class="fa fa-ban"></span>
    <button type="button" class="close" data-dismiss="alert"><span class="glyphicon glyphicon-remove"></span></button>
    <strong>{!! trans('admin/common.error') !!}</strong>
    @if(is_array($message))
    @foreach ($message as $m)
    {{ $m }}
    @endforeach
    @else
    {{ $message }}
    @endif
</div>
@endif

@if ($message = Session::get('warning_message'))
<div class="alert alert-warning alert-block session-box">
    <span class="fa fa-warning"></span>
    <button type="button" class="close" data-dismiss="alert"><span class="glyphicon glyphicon-remove"></span></button>
    <strong>{!! trans('admin/common.warning') !!}</strong>
    @if(is_array($message))
    @foreach ($message as $m)
    {{ $m }}
    @endforeach
    @else
    {{ $message }}
    @endif
</div>
@endif

@if ($message = Session::get('info_message'))
<div class="alert alert-info alert-block session-box">
    <span class="fa fa-info"></span>
    <button type="button" class="close" data-dismiss="alert"><span class="glyphicon glyphicon-remove"></span></button>
    <strong>{!! trans('admin/common.info') !!}</strong>	
    @if(is_array($message))
    @foreach ($message as $m)
    {{ $m }}
    @endforeach
    @else
    {{ $message }}
    @endif
</div>
@endif

<!--ajax response message-->
<div class="alert alert-danger hide">
    <span class="fa fa-ban"></span>
    <button type="button" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></span></button>
    <span class="msg-content"></span>
</div>
<div class="alert alert-success hide">
    <span class="fa fa-check"></span>
    <button type="button" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></span></button>
    <span class="msg-content"></span>
</div>
<div class="alert alert-info hide">
    <span class="fa fa-info"></span>
    <button type="button" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></span></button>
    <span class="msg-content"></span>
</div>
<div class="alert alert-warning hide">
    <span class="fa fa-warning"></span>
    <button type="button" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></span></button>
    <span class="msg-content"></span>
</div>
<!--Ajax response message over-->