<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use App\User;
use App\Ride;
use App\RideAccepted;
use App\Review;
use App\Transaction;
use DB;
use App\ReferralRecord;
use App\Helpers\Datatable\SSP;

class GetInTouchController extends Controller {

    public function showAllIssues(){
        $data = \App\Models\GetInTouch::all();	
        
        
        return view('admin.issuesList', $data);
    }

    public function getIssueRequestData() {
        
        
        $table = 'get_in_touch';

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'user_id', 'dt' => 0, 'field' => 'user_id'),
            array('db' => 'email', 'dt' => 1, 'field' => 'email'),
            array('db' => 'ride_id', 'dt' => 2, 'field' => 'ride_id'),
            // array('db' => 'subject ', 'dt' => 4, 'field' => 'subject'),
            array('db' => 'description', 'dt' => 3, 'field' => 'description'),
            array('db' => 'created_at', 'dt' => 4, 'field' => 'created_at'),
        );

        $sql_details = array(
            'user' => config('database.connections.mysql.username'),
            'pass' => config('database.connections.mysql.password'),
            'db' => config('database.connections.mysql.database'),
            'host' => config('database.connections.mysql.host'),
        );
        $joinQuery = '';
//        $joinQuery = "LEFT JOIN (SELECT COUNT(*) AS total_bookings, user_id FROM bookings GROUP BY user_id ) as bk ON bk.user_id = users.id";
//        $joinQuery .= " LEFT JOIN (SELECT COUNT(*) AS total_transactions, user_id FROM transactions GROUP BY user_id ) as trans ON trans.user_id = users.id";
        // $joinQuery .= " LEFT JOIN role_user ru ON ru.user_id = users.id";
        // $joinQuery .= " LEFT JOIN roles r ON r.id = ru.role_id";
        // $joinQuery .= " LEFT JOIN user_addresses ur ON ur.user_id = users.id";
        $extraWhere = " ";
        $groupBy = "";
        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }

}
