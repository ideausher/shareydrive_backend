<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Transaction;
use Carbon\Carbon;
use DB;
use App\Models\Rides;
use App\Models\ReferralRecord;

class DashboardController extends Controller {
    
    public function __construct() {
        
    }

    /**
     * Admin dashboard
     *
     */
    public function index() {
       
        try{
            
            $users = \App\RoleUser::where('role_id', '1')->count();
            //$onlineUsers = \App\User::active()->online()->count();
            //$totalVendors = \App\RoleUser::where('role_id', '2')->count();
            //$services = \App\Service::count();
            //$bookings = \App\Booking::count();
            $transactions = Transaction::count();
            //$enquiries = \App\Enquiry::count();
    
            $user = auth()->guard('admin')->user();
            $totalrides = Rides::select('id')->count();
            $uniqueUserRidestakeOrAcc = Rides::selectRaw('distinct(user_id)')->get()->count();
            $totalridescompleted = Rides::select('id')->where('status',2)->count();
            $totalsignupusingreferral = ReferralRecord::select('id')->count();
            
            return view('admin/dashboard',  compact('users','transactions','totalrides','uniqueUserRidestakeOrAcc','totalridescompleted','totalsignupusingreferral'));

        }
        catch (\Exception $ex) {
            return view('errors/500');
        }

    }
}