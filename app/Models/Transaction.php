<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Referral Record"))
 */

class Transaction extends Model
{
    
    /**
     * @var string
     * @SWG\Property(
     *   property="user_id",
     *   type="integer" 
     * )
     *  @SWG\Property(
     *   property="trans_id",
     *   type="integer" 
     * )
     * @SWG\Property(
     *   property="ride_id",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="payment_method",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="amount",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="original_amount",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="coupon_code",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="refferal_code",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="currency",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="status",
     *   type="string" 
     * )
     */

    protected $table ='transactions';
    protected $fillable = [
        'user_id','trans_id','ride_id','payment_method','amount','original_amount','coupon_code','referral_code','currency','status'
    ];

    

}
