@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: Users List
@stop
@section('styles')
<link href="{!! asset('assets/admin/plugins/bootstrap3-editable/css/bootstrap-editable.css') !!}" rel="stylesheet"
  type="text/css" />
<style>
.credit-txt {
  cursor: pointer;
}
</style>
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Users List</h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <!-- Notifications -->
        @include('admin.includes.notifications')
        <!-- ./ notifications -->
      </div>
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive">
            <table id="user_list" class="table table-bordered">
              <thead>
                <tr>
                
                  <th>First Name</th>
                  <th>Email-ID</th>
                  <th>Phone No</th>
                  <th>Gender</th>
                  <th>Photo</th>
                  <th>Bio</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div> <!-- /. box body -->
        </div> <!-- /.box -->
      </div> <!-- /.col-xs-12 -->
    </div><!-- /.row (main row) -->

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@section('scripts')
<script src="{{asset('assets/admin/plugins/bootstrap3-editable/js/bootstrap-editable.min.js')}}" type="text/javascript">
</script>
<script type="text/javascript">
var oTable;
$(document).ready(function() {
  oTable = $('#user_list').dataTable({
    "dom": "<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'l><'col-xs-12 col-sm-4 col-md-4 col-lg-4'r><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'f>>t<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'i><'col-xs-12 col-sm-4 col-md-4 col-lg-4'><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'p>>",
	"language": {                
            "infoFiltered": "",
        },
    "processing": true,
    "serverSide": true,
    "ajax": "{!! url('admin/users/UserData') !!}",
//    "columnDefs": [{
//      "orderable": false,
//      "targets": [3, 4, 5]
//    }, ],
    "order": [
      [0, "asc"]
    ],
    "fnDrawCallback": function() {
      //jQuery.fn.editable.defaults.mode = 'inline';
      $.fn.editableform.buttons =
        '<button type="submit" class="btn btn-success editable-submit btn-mini"><span class="fa fa-check"></span></button>' +
        '<button type="button" class="btn editable-cancel btn-mini"><span class="fa fa-times"></span></button>';

      $('.credit-txt').editable({
        type: 'text',
        pk: '1',
        url: '/admin/users/updateCredit',
        params: function(params) {
          // add additional params from data-attributes of trigger element
          params._token = "{!! csrf_token() !!}";
          params.userId = $(this).editable().data('userid');
          return params;
        },
        name: 'credit',
        title: "Credit Title",
        success: function() {}
      });
    },
   
  });

  $("#user_list").on('click', '.delete-btn', function() {
    var id = $(this).attr('id');
    var r = confirm("Are you sure you want to delete the record");
    if (!r) {
      return false;
    }
    $.ajax({
      type: "POST",
      url: "users/" + id,
      data: {
        _method: 'DELETE',
        _token: "{{ csrf_token() }}"
      },
      dataType: 'json',
      beforeSend: function() {
        $(this).attr('disabled', true);
        $('.alert .msg-content').html('');
        $('.alert').hide();
      },
      success: function(resp) {
        $('.alert:not(".session-box")').show();
        if (resp.success) {
          $('.alert-success .msg-content').html(resp.message);
          $('.alert-success').removeClass('hide');
        } else {
          $('.alert-danger .msg-content').html(resp.message);
          $('.alert-danger').removeClass('hide');
        }
        $(this).attr('disabled', false);
        oTable.fnDraw();
      },
      error: function(e) {
        alert('Error: ' + e);
      }
    });
  });

  $("#user_list").on('click', '.status-btn', function() {
    var id = $(this).attr('id');
    var r = confirm("Are you sure you want to change the status");
    if (!r) {
      return false
    }
    $.ajax({
      type: "POST",
      url: "{{ url('admin/users/changeStatus') }}",
      data: {
        id: id,
        _token: "{{ csrf_token() }}"
      },
      dataType: 'json',
      beforeSend: function() {
        $(this).attr('disabled', true);
        $('.alert .msg-content').html('');
        $('.alert').hide();
      },
      success: function(resp) {
        $('.alert:not(".session-box")').show();
        if (resp.success) {
          $('.alert-success .msg-content').html(resp.message);
          $('.alert-success').removeClass('hide');
        } else {
          $('.alert-danger .msg-content').html(resp.message);
          $('.alert-danger').removeClass('hide');
        }
        $(this).attr('disabled', false);
        oTable.fnDraw();
      },
      error: function(e) {
        alert('Error: ' + e);
      }
    });
  });
  
  
            
            
            
  /* $.ajax({
      type: "GET",
      url: "{{ url('admin/users/show') }}",
      data: {
        id:id,
        _token: "{{ csrf_token() }}"
      },
      dataType: 'json',
      beforeSend: function() {
        $(this).attr('disabled', true);
        $('.alert .msg-content').html('');
        $('.alert').hide();
      },                  
      success: function(resp) {
        $('.alert:not(".session-box")').show();
        if (resp.success) {
          $('.alert-success .msg-content').html(resp.message);
          $('.alert-success').removeClass('hide');
        } else {
          $('.alert-danger .msg-content').html(resp.message);
          $('.alert-danger').removeClass('hide');    
        }
        $(this).attr('disabled', false);
        oTable.fnDraw();   
        alert("success");
      },
      error: function(e) {
          alert("This is Error");
        alert('Error: ' + e); 
      } 
    }); */
 
});
</script>
@stop
