<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <!--<b>Version</b> 2.2.0-->
    </div>
    <strong>Copyright &copy; {!! date('Y') !!} <a href="{!! url('/') !!}" target="_blank">Car Pooling App</a> - </strong> All rights reserved
</footer>