<?php

return [

    /*
    |--------------------------------------------------------------------------
    | admin Settings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the admin sidebar links.
    | You are free to change them to anything you want to customize 
    | your views to better match your application.
    |
    */
    
    'question' => 'Question',
    'answer' => 'Answer',
    'language' => 'Language',
    'faq_list' => 'Faq List',
    'faq_add' => 'Faq Add',
    'faq_edit' => 'Faq Edit',
    'add_new' => 'Add New',
    'update_success' => 'Faq udpated successfully.',
    'added_success' => 'Faq added successfully.',
    'delete_success' => 'Faq is deleted successfully.',
];
