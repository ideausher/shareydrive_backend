<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use App\Helpers\Response;

class AddBankReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account_holder_name' => 'required|string|regex:/^[a-z A-Z]+$/|max:45',
            'account_number' => 'required|string|regex:/^[0-9]+$/|min:9|max:30',
            // 'bank_name' => 'required|string|regex:/^[a-z A-Z]+$/|max:150',
            'bsb_no' => 'required|string|regex:/^[0-9]+$/|max:7'
            // 'bsb_no' => 'required|integer|max:9999999'
        ];
    }
    
    /**
     * Overwrite Validation error response
     * 
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {   
        $failedRules = $validator->failed();
        $statusCode = Response::resposeCode(key($failedRules),$failedRules);
        $response = new JsonResponse([ 
            'message' => $validator->errors()->first()
        ], $statusCode);

        throw new ValidationException($validator, $response);

    }
}
