@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: Users List
@stop
@section('styles')
<link href="{!! asset('assets/admin/plugins/bootstrap3-editable/css/bootstrap-editable.css') !!}" rel="stylesheet"
  type="text/css" />
<style>
.credit-txt {
  cursor: pointer;
}
</style>
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h4>{!! trans('User Performance') !!} / {{$user['firstname']." " .$user['lastname']}} </h4>


        <!-- <ol class="breadcrumb">
                <li><a href="/admin"><span class="fa fa-dashboard"></span> {!! trans('admin/common.home') !!}</a></li>
                <li class="active">{!! trans('admin/service.services') !!}</li>
            </ol> -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
       
       <div class="row box" style="margin-left:0;">
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <span class="fa fa-car"> Total Rides offered : {{$total_ride}}</span><br>
                    <span class="fa fa-car"> Total Rides Taken : {{$total_ride_taken}} </span><br>
                </div>
                <div class="col-sm-4 col-md-4">
                    <span class="fa fa-money"> Total Money Earned (USD) : {{number_format($total_money_earned,2)}}</span><br>
                    <span class="fa fa-money"> Total Money Spent (USD) : {{number_format($total_money_spent,2)}} </span><br>
                </div>
                <div class="col-sm-4 col-md-4">
                    <span class="fa fa-star"> Rating as Driver : {{number_format($rating_as_driver,1)}}</span><br>
                    <span class="fa fa-star"> Rating as Passenger : {{number_format($rating_as_passenger,1)}} </span><br>
                </div>
            </div>
        
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="inner-custom-content">
                        <span><h4>Last 5 Reviews as Driver</h4></span>
                    </div>
                        @foreach($review_as_driver as $key=>$driver)
                        <span class="cont-info"><b></b><input placeholder="Email.." readonly value="{{$driver->info}}" type="email"></span>
                        @if($key>=4)
                            @php
                        break;
                        @endphp
                        @endif
                        @endforeach
                    <div class="inner-custom-content">
                        <span><h4>Last 5 Reviews as Passenger</h4></span>
                    </div>
                        @foreach($review_as_passenger as $key=>$passenger)
                        <span class="cont-info"><b></b><input placeholder="Email.." readonly value="{{$passenger->info}}" type="email"></span>
                        @if($key>=4)
                            @php
                        break;
                        @endphp
                        @endif
                        @endforeach
                </div>
                <div class="col-sm-4 col-md-4">
                    
                    <div id="earnedVsSpentPie">
                        <!-- Earned Vs Spent Graph -->
                    </div>
                </div>
            </div>
     
       <div class="create-report-button">
         <a href="{!! url('admin/users') !!}"><button>Go Back</button></a>
     </div>
     </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop