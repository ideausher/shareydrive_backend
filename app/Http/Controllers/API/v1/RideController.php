<?php 

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RideRequestRequest;
use App\Http\Requests\GetRides;
use App\Http\Requests\AcceptRideReq;
use App\Http\Requests\ReviewReq;
use App\Models\RideAccepted;
use App\Models\Review;
use App\Helpers\Response;
use App\Models\Rides;
use App\Models\RideAddress;
use App\Models\Vehicle;
use \Carbon\Carbon;
use Auth;
use DB;
use DateTime;
use App\Models\ReferralRecord;
use App\Models\Transaction;
use App\Setting;
use Stripe\Stripe;

use App\Models\RideRequest;
use App\Http\Requests\sendBookingRequestRequest;
use App\Http\Requests\getRideRequestRequest;
use App\Http\Requests\hitCustomNotificationRequest;
use App\Http\Requests\acceptOrRejectRideRequestRequest;
use App\Http\Requests\CompleteRideRequest;
use App\Http\Requests\driverchangeridestatusRequest;
use App\Http\Requests\bookingDetailsRequest;
use App\User;
use App\Models\DeviceDetails;
use Illuminate\Support\Facades\Log;

class RideController extends controller{
    


    protected $response = [
        'message' => '',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct() {
        
        $this->response['data'] = new \stdClass();
    }

    protected function setData($complexObject){
        $json = json_encode($complexObject);
        $encodedString = preg_replace('/null/', '" "' , $json);
        $this->response['data'] = json_decode($encodedString);
        return $this->response['data'];
    }



    /**
     * @SWG\Get(
     *     path="/hitCustomNotification",
     *     tags={"custom"},
     *     summary="This Api will be used to send custom notification. Developed by keeping in mind the needs of Developer",
     *     description="This Api will be used to send custom notification. Developed by keeping in mind the needs of Developer",
     *     operationId="hitCustomNotification",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         name="title",
     *         required = true,
     *         in="query",
     *         description="Notification Title",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="recieverID",
     *         required = true,
     *         in="query",
     *         description="Receiver ID | To whom notification is to be sent",
     *         type="string"
     *     ),
     *    @SWG\Parameter(
     *         name="message",
     *         required = true,
     *         in="query",
     *         description="Message | The message that will be sent to the reciever",
     *         type="string"
     *     ),
     *   @SWG\Parameter(
     *         name="data",
     *         required = true,
     *         in="query",
     *         description="Data | Used to send some data in Notification",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function hitCustomNotification(hitCustomNotificationRequest $request){

        try {
            $type=13; // Means the notification is of custom notification Type
            $dataPush = $request['data'];
            unset($dataPush["google.original_priority"]); 
            unset($dataPush["google.delivered_priority"]); 
            // Log::error($request);
            $dataPush['firstName'] = Auth::user()->firstname;
            $dataPush['lastName'] = Auth::user()->lastname;
            $dataPush['image'] = Auth::user()->image ? Auth::user()->image : (Auth::user()->social_image ? Auth::user()->social_image : "" );
            $dataPush['image'] = substr($dataPush['image'], strrpos($dataPush['image'],'/')+1);
            DeviceDetails::sendNotification($request['title'] ,$request['message'], $request['recieverID'],$dataPush,"",$type);
            $this->response['message'] = "Notification Sent Successfully";    
            $this->response['data'] = $dataPush;
            return response($this->response, Response::HTTP_ACCEPTED);
        }
        catch (\Exception $ex) {
            Log::error($ex);
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }








    /* #region  getRideById */
    
    /**
     * @SWG\Get(
     *     path="/getRideById",
     *     tags={"Ride"},
     *     summary="Get Ride details by Id",
     *     description="Get Ride details by Id using API's",
     *     operationId="getRideById",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="ride_id",
     *         required = true,
     *         in="query",
     *         description="Ride Id",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function getRideById(Request $request){

        try {
            if(!isset($request['ride_id']))
            {
                $this->response['message'] ="Please send Ride Id";    
                return response($this->response, Response::HTTP_ACCEPTED);
            }
            
            
            $resultQuery = Rides::with([
                'startLocation',
                'endLocation',
                'vehicleinformation',
                'userDetails'
            ])
            ->whereHas('startLocation',function($query)  use($request){
                $query->where('ride_id', $request->get('ride_id'));
            })
            ->whereHas('endLocation',function($query)  use($request){
                $query->where('ride_id', $request->get('ride_id'));
            })
            ->has('vehicleinformation');
            $rides = $resultQuery->get();
            
            if(count($rides) > 0){
                $this->response['message'] = trans("Record Found"); 
                $this->response['data'] = $rides;
                return response($this->response, Response::HTTP_OK);
            }
            $this->response['message'] = trans("No Data Found");    
            $this->response['data'] =[];
            return response($this->response, Response::HTTP_ACCEPTED);
        }
        catch (\Exception $ex) {
            Log::error($ex);
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }

    /* #endregion */


    
    /* #region  getRides */
    
    /**
     * @SWG\Get(
     *     path="/getRides",
     *     tags={"Ride"},
     *     summary="Get list of rides available",
     *     description="Get list of rides available using API's",
     *     operationId="getRide",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="start_latitude",
     *         required = true,
     *         in="query",
     *         description="Start Latitude",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="start_longitude",
     *         required = true,
     *         in="query",
     *         description="Start Longitude",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end_latitude",
     *         required = true,
     *         in="query",
     *         description="End Latitude",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end_longitude",
     *         required = true,
     *         in="query",
     *         description="End Longitude",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         required = true,
     *         in="query",
     *         description="Date and time",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="min_price",
     *         in="query",
     *         description="Minimum price",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="max_price",
     *         in="query",
     *         description="Maximum price",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="review_ratings",
     *         in="query",
     *         description="Review Ratings should be (1-5)",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="available_seats",
     *         in="query",
     *         description="Available seats should be (0 for not filtered,1 for filtered data)",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="distance",
     *         required = true,
     *         in="query",
     *         description="Distance",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="gender",
     *         in="query",
     *         description="Gender (0 = male, 1 = female, 2 = Trans)",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         required = true,
     *         in="query",
     *         description="Page",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         required = true,
     *         in="query",
     *         description="Limit",
     *         type="integer"
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function getRides(GetRides $request){
        $timeaddorsub = Setting::select('ride_time_diff_search')->get()->first();
        $timeadd = '+'.$timeaddorsub->ride_time_diff_search.' hours';
        $timesub = '-'.$timeaddorsub->ride_time_diff_search.' hours';
        $dateprevious = (new \DateTime($request['date']))->modify($timeadd);
        $datenext = (new \DateTime($request['date']))->modify($timesub);

        // if the date next is passed then set the current time as date next
        if((new \DateTime(now())) > $datenext){
            $datenext = new \DateTime(now());
        }
        
        try {
            DB::connection()->enableQueryLog();
            $resultQuery = Rides::with([
                                    'startLocation',
                                    'endLocation',
                                    'userReviews',
                                    'rideAccepted',
                                    'userDetails'=> function($query){
                                        $query->selectRaw('users.id,fb_id,google_id,social_image,firstname,lastname,email,image,online,phone_number,phone_country_code,date_of_birth,gender');
                                    
                                    },
                                ])
                                ->whereHas('startLocation',function($query) use($request){
                                    $lat = $request['start_latitude'];
                                    $long = $request['start_longitude'];
                                    $query->select(DB::raw("COALESCE(( 6371 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ),0 ) AS distance "))
                                    ->havingRaw('distance <= '.$request['distance'].' ');

                                })
                                ->whereHas('endLocation',function($query) use($request){
                                    $lat = $request['end_latitude'];
                                    $long = $request['end_longitude'];
                                    $query->select(DB::raw("COALESCE(( 6371 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ),0 )AS distance "))
                                    ->havingRaw('distance <= '.$request['distance'].' ');
                                })
                                
                                ->where([
                                    ['ride_date_time','>=', $datenext],
                                    ['ride_date_time','<=', $dateprevious],
                                    'status' => 1,
                                ])
                                ->where('user_id','!=',Auth::user()->id);
            // adding other conditions to the parameters when available for the filter
            // filter parameters
            // if request containes gender parameters
            if(isset($request['gender'])){
                $resultQuery->where(['gender' => $request['gender']]);
            }
            
            // if request contains min_price and max_price
            if(isset($request['min_price']) && isset($request['max_price']))
            {
                $resultQuery->where([
                    ['price_per_seat','>=',$request['min_price']],
                    ['price_per_seat','<=',$request['max_price']]
                ]);
            }
            
            //if request contains review-> ratings -> value
            if(isset($request['review_ratings']))
            {
                $resultQuery->whereHas('userReviews', function($query) use($request){
                    $query->where([['rating','>=',$request['review_ratings']],['type','=','2']]);
                });
            }
            
            //if request contains availabile seats
            if(isset($request['available_seats']) == true && $request['available_seats'] == 1){
                    $resultQuery->whereHas('rideAccepted', function($query){
                        $query->select(DB::raw('sum(consume_seats) as totalacceptedseats'))
                        ->having('ride.max_no_seats','>','`totalacceptedseats`');
                        
                    });
            }

            
            $resultQuery->selectRaw("*,(select count(*) from (select ride.user_id as user_one, ride_accepted.user_id as user_two  from ride_accepted inner join ride  on ride.id = ride_accepted.ride_id) as userslist where (user_one=".Auth::user()->id." and user_two=ride.user_id) or (user_one=ride.user_id and user_two=".Auth::user()->id.") ) as is_travelled_already_with");
            
            
            if($request['page'] ){
                $resultQuery->offset($request['limit']*$request['page']);
            }

            if($request['limit']){
                $resultQuery->limit($request['limit']);
            }

            // dd($resultQuery->toSql());
            $rides = $resultQuery->get();
            $rides = $rides->reject(function ($rides) {
                if($rides->max_no_seats < $rides->consume_seat_sum){
                    return $rides->consume_seat_sum;
                }
            });

            
            

            if(count($rides) > 0){
                $this->setData(
                    $rides
                );
                $this->response['message'] = trans("List Found"); 
                return response($this->response, Response::HTTP_OK);
            }

            $this->response['message'] = trans("No Data Found");    
            $this->response['data'] =[];
            return response($this->response, Response::HTTP_ACCEPTED);

        }
        catch (\Exception $ex) {
            Log::error($ex);

            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        } 

    }
    
    /* #endregion */
    
    
    
    /* #region  getUserRides */
    /**
     * @SWG\Get(
     *     path="/getUserRides",
     *     tags={"Ride"},
     *     summary="Get User Rides list",
     *     description="Get User Rides list using API's",
     *     operationId="getUserRides",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         required = true,
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         required = true,
     *         in="query",
     *         description="Upcoming,History,All(1,2,3)",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="fetch",
     *         required = true,
     *         in="query",
     *         description="Accepted,Offered,Both(a/A,o/O,b/B)",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         required = true,
     *         in="query",
     *         description="Pagination page number",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         required = true,
     *         in="query",
     *         description="Number of records in list",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *            name="time-zone",
     *            in="header",
     *            description="Time Zone",
     *            type="string"
     *     ),  
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function getUserRides(Request $request){
        try{
            if($request->type==null){$this->response['message'] = "Please specify type";return response($this->response, Response::HTTP_OK);}
            if($request->fetch==null){$this->response['message'] = "Please specify what to fetch";return response($this->response, Response::HTTP_OK);}
            

            DB::connection()->enableQueryLog();

            $user = Auth::User();
            $date = Carbon::now()->subDays(1)->timezone($request->header('time-zone'))->format('Y-m-d H:i:s');

            DB::connection()->enableQueryLog();
            $rides = Rides::with([
                                    'startLocation',
                                    'endLocation',
                                    'userReviews',
                                    'userRatingAvg' => function($query){
                                        $query->select('ride_id', DB::Raw('AVG(rating) as avg_rating'));
                                        $query->groupBy('ride_id');
                                    },
                                    'userDetails',
                                    'vehicleinformation'
                            ]);
            
            
            // 1. Get User's Upcoming Rides that are accepted by the user
            // 2. Get User's History Rides that are accepted by the user
            // 3. Get User's all Rides (Upcoming & History) that are accepted by the user
            $rides->where(function ($rides) use($user,$request){
                if($request->type==1 ||$request->type==2 || $request->type==3){
                    if($request->fetch=='A' || $request->fetch=='a'){
    
                        $rides->whereHas('rideAccepted',function($query) use($user){
                            $query->where('user_id',$user->id);
                        });
                    }
                    if($request->fetch=='O' || $request->fetch=='o'){
                        $rides->where('ride.user_id',$user->id);
                    }
                    if($request->fetch=='B' || $request->fetch=='b'){
                        $rides->whereHas('rideAccepted',function($query) use($user,$request){
                            $query->where('user_id',$user->id);
                            $query->orWhere('ride.user_id',$user->id);
                            if($request->type==1){
                                $query->where('ride.status','<>',2); // if it's upcoming then dont fetch completed one
                            }
                        });
                        
                    }
                }
    
                if($request->type==1  || $request->type==2 || $request->type==3){
                    $rides->orWhere('ride.user_id',$user->id);
                }
            }); // here subquery parts end 
            
            if($request->type==1){
                $rides->whereNotIn( 'ride.status', [3,2]); // dont show rides which are cancelled by the driver i.e. he will not go to that place
                if($request->fetch=='B' || $request->fetch=='b'){
                    // 10 minutes 
                    $timeaddorsub = 10;
                    $timesub = '-'.$timeaddorsub.' minutes';
                    $datenext = (new \DateTime())->modify($timesub);

                    //  upcoming rides the 10 minute  case has been added (means ride will not be show in upcoming list after 10 minutes)
                    $rides->where( 'ride.ride_date_time', '>=', $datenext );
                    
                }
            }
            if($request->type==2){
                $rides->whereIn( 'ride.status',[2,3]); // dont show rides which are cancelled by the driver i.e. he will not go to that place
                
                if($request->fetch=='B' || $request->fetch=='b'){
                    $rides->orwhere(function($q)  {
                        //and
                        $q->where( 'ride.status',1);
                        // 10 minutes 
                        $timeaddorsub = 10;
                        $timesub = '-'.$timeaddorsub.' minutes';
                        $datenext = (new \DateTime())->modify($timesub);
                       
                        //  upcoming rides the 10 minute  case has been added (means ride will not be show in upcoming list after 10 minutes)
                        $q->where( 'ride.ride_date_time', '<=', $datenext );    
                    });
                }
            }
                    
            if($request->type==3){
                $rides->where('ride.status','<>',2); // dont show rides which are cancelled by the driver i.e. he will not go to that place
            }
                
            $rides->selectRaw("*, @AccOrOff := (CASE WHEN ride.user_id = $user->id  THEN 'Ride Offered' else 'Ride Requested' END) as AccOrOff,
            (CASE WHEN @AccOrOff = 'Ride Offered'  
            THEN 
				(CASE WHEN ride.status = 0 THEN 'In Progress'
                    WHEN  ride.status = 1 THEN 'Active'
                    WHEN ride.status = 2 THEN 'Completed' 
                    WHEN ride.status =3 THEN 'Offline'
                END)
                
                WHEN @AccOrOff = 'Ride Requested'  
                THEN 
                (CASE WHEN ride.status = 0 THEN 'In Progress'
                    END)
                
            END )as statusRide  
            ");
               
            if($request->type!= 3){
                $orderBy =  'DESC'; // add ternary operator on the basis of $request->type is equal to 1 and apply ASC or DESC
                $operatorDate = $request->type == 1 ? '>=' : '<=';
               
                if($request->type!=2){
                    $rides->where(function($q) use($operatorDate, $date, $request) {
                        //and
                        $q->where(function($qq) use($operatorDate, $date, $request) {
                                //and
                            $qq->where('ride_date_time',$operatorDate,$date);
                                $qq->where(function($qqq) use( $request) {        
                                    // or
                                    if($request->type != 2){
                                        $qqq->orWhere('status',0);
                                        $qqq->orWhere('status',1);
                                    }
                                });
                        
                        });
                        if($request->type != 2){
                            $q->orwhere('status',0);
                        }

                    });
                }
            
                $rides = $rides->offset($request['limit']*$request['page'])
                        
                        ->orderBy('ride.created_at','DESC')
                        ->orderBy('ride_date_time',$orderBy)
                        ->limit($request['limit'])
                        // ->toSql();
                        // var_dump($rides);
                        // exit;
                        ->get();

            }
            else{
              
                $rides = $rides->offset($request['limit']*$request['page'])
                        ->orderBy('ride_date_time','DESC')
                        ->limit($request['limit'])
                        ->get();
            }
           
            // 4. Get User's Upcoming Rides that are offered by the user
            // 5. Get User's History Rides that are offered by the user
            // 6. Get User's all Rides(Upcoming & History) that are offered by the user

            // 7. Get User's Upcoming Rides whether it is created or accepted by the user
            // 8. Get User's History Rides whether it is created or accepted by the user
            // 9. Get User's all Rides (Upcoming & History) whether it is created or accepted by the user.
            
            
            if(count($rides) > 0) {
                $this->response['message'] = "Record Found.";
                $this->setData($rides);
                return response($this->response, Response::HTTP_OK);
            }

            $this->response['message'] = 'No Record Found.';
            $this->response['data'] =[];
            return response($this->response, Response::HTTP_ACCEPTED);
            
        }
        catch (\Exception $ex) {
            Log::error($ex);
            
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /* #endregion */

    /* #region  offerRide */
    /**
     * @SWG\Post(
     *     path="/offerRide",
     *     tags={"Ride"},
     *     summary="Offer Ride",
     *     description="Will register a new offer ride request",
     *     operationId="createRide",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Create Ride object",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="ride_date_time",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="max_no_seats",
     *              type="string"
     *             ),
     *              @SWG\Property(
     *              property="gender",
     *              type="integer"
     *             ), 
     *             @SWG\Property(
     *              property="price_per_seat",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="total_toll_tax",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="start_latitude",
     *              type="string"
     *             ),  
     *             @SWG\Property(
     *              property="start_longitude",
     *              type="string"
     *             ),   
     *             @SWG\Property(
     *              property="start_country",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="start_place_id",
     *              type="string"
     *             ),
     *              @SWG\Property(
     *              property="start_city",
     *              type="string"
     *             ),
     *               @SWG\Property(
     *              property="start_state",
     *              type="string"
     *             ),
     *               @SWG\Property(
     *              property="start_full_address",
     *              type="string"
     *             ),
     *               @SWG\Property(
     *              property="end_latitude",
     *              type="string"
     *             ),
     *                @SWG\Property(
     *              property="end_longitude",
     *              type="string"
     *             ),
     *                @SWG\Property(
     *              property="end_country",
     *              type="string"
     *             ),
     *                @SWG\Property(
     *              property="end_place_id",
     *              type="string"
     *             ),
     *             
     *                @SWG\Property(
     *              property="end_city",
     *              type="string"
     *             ),
     *              
     *                @SWG\Property(
     *              property="end_state",
     *              type="string"
     *             ),
     *             
     *                @SWG\Property(
     *              property="end_full_address",
     *              type="string"
     *             )
     *         )    
     *     ),
     *     @SWG\Parameter(
     *            name="device-token",
     *            in="header",
     *            description="Device Token",
     *            type="string"
     *     ),  
     *     @SWG\Parameter(
     *            name="device-id",
     *            in="header",
     *            description="Device Id",
     *            type="string"
     *     ),  
     *     @SWG\Parameter(
     *            name="build-version",
     *            in="header",
     *            description="Build Version",
     *            type="string"
     *     ),  
     *     @SWG\Parameter(
     *            name="platform",
     *            in="header",
     *            description="Platform",
     *            type="string"
     *     ), 
     *     @SWG\Parameter(
     *            name="build",
     *            in="header",
     *            description="Build",
     *            type="string"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */

    public function createRide(RideRequestRequest $request){    
        try{    
        /** Start Transaction here while create offer ride **/
        DB::beginTransaction();         
        
        $ridespublished = array();
        for($i=0; $i < count($request['ride_date_time']); $i++){
            // storing every ride date time in a variable
            $ride_date_time = $request['ride_date_time'][$i];

            $date1 = strtotime($ride_date_time);
            $date2 = time();
            if($date1<$date2)
            {
                $this->response['message']= "Date and Time cannot be less then the current date time";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            
                // ride data table data
                    $vehicleid = Vehicle::where('user_id',Auth::User()->id)->latest()->first();
                    if(!isset($vehicleid))
                    {
                        $this->response['message'] = trans('No vehicle found for the user ');
                        return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    
                    $ridedata =['user_id'=>Auth::User()->id,'vehicle_id'=>$vehicleid->id,'gender'=>$request['gender'],'ride_date_time'=>$ride_date_time,'max_no_seats'=>$request['max_no_seats'],'price_per_seat'=>$request['price_per_seat'],'total_toll_tax'=>$request['total_toll_tax'],'status'=>'1'];
                    $rideID = Rides::insertGetId($ridedata);
    
                // ride address table data ---> 
                // (1) starting address (2) Ending address
                    $ride_Start_Address_Data = ['ride_id'=>$rideID,'type'=>'1','latitude'=> $request['start_latitude'],'longitude'=>$request['start_longitude'],'country'=>$request['start_country'],'place_id'=>$request['start_place_id'],'city'=>$request['start_city'],'state'=>$request['start_state'],'full_address'=>$request['start_full_address']];
                    $ride_End_Address_Data = ['ride_id'=>$rideID,'type'=>'2','latitude'=> $request['end_latitude'],'longitude'=>$request['end_longitude'],'country'=>$request['end_country'],'place_id'=>$request['end_place_id'],'city'=>$request['end_city'],'state'=>$request['end_state'],'full_address'=>$request['end_full_address']];
                
                // Adding data to the database
                $data=[$ride_Start_Address_Data,$ride_End_Address_Data];
                RideAddress::insert($data);
                
                array_push($ridespublished,$rideID);
        }
        
            /** Submit Transaction here if every thing working **/
            DB::commit(); 
                    
            $this->response['message'] = trans('Ride_successfully_published');
            $this->response['data'] =['ride_id'=>$ridespublished];
            return response($this->response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
                /*** Role back all queries for fresh entry ***/
            DB::rollBack();
            Log::error($ex);

                $this->response['message'] = trans('something_wrong');
                return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }
    /* #endregion */

    
    /* #region /ride/payment  */
    
    /**
     * @SWG\Post(
     *     path="/ride/payment",
     *     tags={"Ride"},
     *     summary="User Will make payment for the ride",
     *     description="User will make payment for the specific ride",
     *     operationId="acceptRide",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Accept Ride object",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="ride_id",
     *              type="integer"
     *             ),
     *             @SWG\Property(
     *              property="consume_seats",
     *              type="integer"
     *             ),
     *             @SWG\Property(
     *              property="card_source_id",
     *              type="string"
     *             )
     * ,
     *             @SWG\Property(
     *              property="card_save",
     *              type="integer"
     *             )
     * ,
     *             @SWG\Property(
     *              property="currency",
     *              type="string"
     *             )
     * ,
     *             @SWG\Property(
     *              property="original_amount",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="coupon_code",
     *              type="string"
     *             ),
     *              @SWG\Property(
     *              property="referral_code",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="discount_applied",
     *              type="integer"
     *             ),
     *          @SWG\Property(
     *              property="card_id",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="price",
     *              type="integer"
     *             ),
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function rideAccept(AcceptRideReq $request){
        try{
            Stripe::setApiKey(env("STRIPE_SECRET"));
            /** Start Transaction here while create users **/
            DB::beginTransaction();   

            $rideAccepted = RideAccepted::where(['ride_id' => $request['ride_id'], 'user_id' => Auth::User()->id, 'is_accepted_by_driver'=> 1])
                            ->first();
            //if rideAccepted is null means that the logged in user haven't made any request for the ride id
            if($rideAccepted == null){
                $this->response['message'] = "Please make a request before making any payment or your ride is not accepted by the Driver";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            //if user has completed the ride or paid for the ride then it will return the response accordingly
            if($rideAccepted->status == 1 || $rideAccepted->status == 2){
                $this->response['message'] = "Either you have already made the payment or Ride is completed";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            
            
            $user = Auth::user();
            $inputData = $request->all();
            $inputData['user_id'] = $user->id;
            $stripeCustomerId = $user->stripe_custmer_id;

            if(!$stripeCustomerId){

                $customerDetails = \Stripe\Customer::create([
                    'description' => 'New Customer added '.$user->firstname,
                    'email' => $user->email,
                    'source' => $inputData['card_source_id']
                ]);
                $user->stripe_custmer_id = $customerDetails->id;
                $user->save();
                $stripeCustomerId = $customerDetails->id;
                
            }

            if($inputData['card_save'] == 1){
            
                $cardDetails = \Stripe\Customer::allSources(
                    $stripeCustomerId
                );

                $tokenDetails = \Stripe\Token::retrieve(
                    $inputData['card_source_id']
                );

                $isAlreadyAdded = false;

                foreach($cardDetails as $card){
                    if($card['fingerprint'] == $tokenDetails['card']['fingerprint']){
                        $isAlreadyAdded = true;
                    }
                }

                if($isAlreadyAdded == false){
                    $source = \Stripe\Customer::createSource(
                        $stripeCustomerId,
                        ['source' => $inputData['card_source_id']]
                    );
            
                    $charge = \Stripe\Charge::create([
                        'amount' => $inputData['price'] * 100, 
                        'currency' => $inputData['currency'], 
                        'source' => $source->id,
                        'customer' => $stripeCustomerId 
                    ]);
                }
                else{
                    $charge = \Stripe\Charge::create([
                        'amount' => $inputData['price'] * 100, 
                        'currency' => $inputData['currency'], 
                        'source' => $inputData['card_source_id']
                    ]);
                }  

            }
            else
            {

                $charge = \Stripe\Charge::create([
                    'amount' => $inputData['price'] * 100, 
                    'currency' => $inputData['currency'], 
                    'customer' => $stripeCustomerId 
                ]);

            }
            
            $rideAccepted->update(['status' => '1']);
            
            if($charge){
                if(!isset($inputData['referral_code'])) {$inputData['referral_code']='';}
                if(!isset($inputData['discount_applied'])) {$inputData['discount_applied']='0.00';}
                
                Transaction::create([
                    'user_id' => $user->id,
                    'trans_id' => $charge->id,
                    'payment_method' => $charge->payment_method_details->card->brand,
                    'ride_id' => $inputData['ride_id'],
                    'currency' => $inputData['currency'],
                    'amount' => $inputData['price'],
                    'original_amount' => $inputData['original_amount'],
                    'referral_code' => $inputData['referral_code'],
                    'discount' => $inputData['discount_applied'],
                    'status' => $charge->paid
                ]);
            }

            // if($charge->paid == 1)
            //     $ride = Rides::where('id',$inputData['ride_id'])->update(['status' => 1]);
            


            /** Submit Transaction here if every thing working **/
            DB::commit();  
            $this->response['message'] = "Ride Payment accepted succesfully."; 
            $this->setData($rideAccepted);
            return response($this->response, Response::HTTP_OK);
        }
        catch(\Stripe\Exception\CardException $ex) {
            Log::error($ex);
            DB::rollback();
            // Since it's a decline, \Stripe\Exception\CardException will be caught
            $this->response['message'] = 'Message is:' . $e->getError()->message . '\n' . 'Code is:' . $e->getError()->code . '\n';
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);

        }
        catch (\Stripe\Exception\RateLimitException $ex) {
            Log::error($ex);
            DB::rollback();
            // Too many requests made to the API too quickly
            $this->response['message'] = "Too many requests made to the API too quickly";
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        catch (\Stripe\Exception\InvalidRequestException $ex) {
            Log::error($ex);
            DB::rollback();
            $this->response['message']= "Invalid parameters were supplied to Stripe's API";
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
            // Invalid parameters were supplied to Stripe's API
        }
        catch (\Stripe\Exception\AuthenticationException $ex) {
            Log::error($ex);
            DB::rollback();
            $this->response['message']="Authentication with Stripe's API failed" . '\n' . "Or Maybe you changed API keys recently";
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
        }
        catch (\Stripe\Exception\ApiConnectionException $ex) {
            Log::error($ex);
            DB::rollback();
            $this->response['message']="Network communication with Stripe failed";
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
            // Network communication with Stripe failed
        }
        catch (\Stripe\Exception\ApiErrorException $ex) {
            Log::error($ex);
            DB::rollback();
            $this->response['message']="API Error Ocurred";
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
            
            // Display a very generic error to the user, and maybe send
            // yourself an email
        }
        catch (\Exception $ex) {
            /*** Role back all queries for fresh entry ***/
            DB::rollBack();
            Log::error($ex);
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /* #endregion */
    
    /* #region  review */
    
    /**
     * @SWG\Post(
     *     path="/review",
     *     tags={"Ride"},
     *     summary="Add Ride Review",
     *     description="Driver or passenger can Add Ride Review using Api<br>Please see the input as shown below :<br> <p>{<br>&quot;type&quot;:&quot;1&quot;,<br>&quot;ratings&quot;:<br>[<br>{<br>&quot;ride_id&quot;:1,<br>&quot;rating&quot;:3,<br>&quot;rating_select&quot;:&quot;Car Neatness&quot;,<br>&quot;info&quot;:&quot;Nothing&quot;<br>},<br>{<br>&quot;ride_id&quot;:1,<br>&quot;rating&quot;:3,<br>&quot;rating_select&quot;:&quot;Fragrance was just Awesome&quot;,<br>&quot;info&quot;:&quot;Nothing&quot;<br>},<br>{<br>&quot;ride_id&quot;:1,<br>&quot;rating&quot;:3,<br>&quot;rating_select&quot;:&quot;Neat and Clean&quot;,<br>&quot;info&quot;:&quot;Nothing&quot;<br>}<br>]<br>}</p>",
     * 
     *     operationId="acceptReview",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Accept Ride object",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="ride_id",
     *              type="integer"
     *             ),
     *             @SWG\Property(
     *              property="rating",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="rating_select",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="info",
     *              type="string"
     *             ),
     *              @SWG\Property(
     *              property="type",
     *              description="Type 1 = Driver Review, Type 2 = Passenger Review ",
     *              type="integer"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function createReview(ReviewReq $request){
        try{
            
            /** Start Transaction here while create users **/
            DB::beginTransaction();   
            $data = $request->all();
            $data['type'] = $request['type'];
            // dd($data);
          
            for($i=0; $i < count($data['ratings']); $i++){
                $data['ratings'][$i]['user_id'] = Auth::user()->id;
                $data['ratings'][$i]['type'] = $request['type'];
            }

            if($data['type']==2){
                //means we have got the rating from the passenger as an array
                
                $review = Review::create($data['ratings'][0]);
                
            }
            else{
                //means we have got the ratings from the driver as an array
                
                for($i=0; $i < count($data['ratings']); $i++){
                    $review = Review::create($data['ratings'][$i]);
                }
                
            }
            

            /** Submit Transaction here if every thing working **/
            DB::commit();  
            $this->response['message'] = "Review is added succesfully."; 
            return response($this->response, Response::HTTP_OK);
            
        } catch (\Exception $ex) {
            /*** Role back all queries for fresh entry ***/
            DB::rollBack();
            Log::error($ex);
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /* #endregion */

    /* #region  bookingDetails */
    
    /**
     * @SWG\Get(
     *     path="/bookingDetails",
     *     tags={"Booking"},
     *     summary="Check booking details by ride id",
     *     description="(ride_id, previous)<br> * previous = 0 or 1 <br>* 0 - means if you want to check the calculated value of the booking details for a ride which is yet to be accepted then pass ride id with previous = 0 <br> * 1 - means you are going to calculate the booking details for the ride which was published earlier and was taken then pass ride id with previous = 1 ",
     *     operationId="bookingDetails",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Ride ID",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="ride_id",
     *              type="integer"
     *             ),
     *              @SWG\Property(
     *              property="previous",
     *              type="integer"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function bookingdetails(bookingDetailsRequest $request){
        try{
            
            // check the ride owner of the ride_id is same as auth user
            $resultQuery = Rides::where('id',$request['ride_id']);
            $data = $resultQuery->first();
            if($data->user_id == Auth::user()->id){
                $this->response['message'] = trans("You can't check the booking details of your own created rides"); 
                $this->response['data'] = [];
                return response($this->response, Response::HTTP_OK);
            }
            
            $value = $request['ride_id'];
            
            if($request['previous']==1){
                $resultQuery = Rides::with([
                    'startLocation',
                    'endLocation',
                    'vehicleinformation',
                    'rideAccepted',
                    'userDetails'
                ])
                ->where('status','=','2'); 
                // this status is for rides table not for ride_accepted
            }
            else{
                $resultQuery = Rides::with([
                    'startLocation',
                    'endLocation',
                    'vehicleinformation',
                    'userDetails' => function($query){
                        $query->select('id','firstname','lastname', 'phone_number','phone_country_code','image','social_image');
                    }
                ])
                ->whereIn('status',[0,1]);
                
            }
            
            $resultQuery->whereHas('startLocation',function($query)  use($request){
                                    $query->where('ride_id', $request->get('ride_id'));
                                    })
                        ->whereHas('endLocation',function($query)  use($request){
                            $query->where('ride_id', $request->get('ride_id'));
                        });
            if($request['previous']==1){
                        $resultQuery->whereHas('rideAccepted',function($query)  use($request){
                            $query->where('ride_id', $request->get('ride_id'));
                        })
                        ->has('vehicleinformation');
            }
            else{
                $resultQuery->has('vehicleinformation');
            }
            
            $data = $resultQuery->get();            
            $noofseats_accepted = 0;

            // If the query count is less then or equal to 0 then it will return No Data Found
            if($resultQuery->count()<=0){
                $this->response['message'] = trans("No Data Found");    
                $this->response['data'] =[];
                return response($this->response, Response::HTTP_ACCEPTED);
            }

            // If the query count is greater then 0 then it will proceed further
            $noofseats_invehicle = $data[0]['max_no_seats'];
            foreach($data[0]['rideAccepted'] as $data1){
                $noofseats_accepted = $noofseats_accepted+ $data1["consume_seats"];
            }
            
            $availableseats = $noofseats_invehicle - $noofseats_accepted;
            
            
            if(count($data) > 0){
                    if($availableseats>0)
                    {   
                        //set available seats to 1 as only 1 seat is booked by the passenger
                        $availableseats=1; // comment this line when you want to book more then one seats 

                        //Counting the records of referral code of the user which are not used
                        $records = DB::table("referral_record")->select('*')
                                    ->whereNotIn('code',function($query){
                                                    $query->select('referral_code')->from('transactions');
                                    })
                                    ->where(function ($query){
                                        $query->where('user_id_two','=',Auth::User()->id)
                                                ->orWhere('user_id_one','=',Auth::User()->id);
                                    })
                                    ->get();
                                 
                        $inviteCodeDiscount=0;
                        if(count($records)>0){
                            $inviteCodeDiscount = Setting::pluck('referral_code_discount')->first();
                        }
                        $payableamount_withoutdiscount = $data[0]['price_per_seat']*$availableseats + $data[0]['total_toll_tax'];    
                        $discountapplied = ($payableamount_withoutdiscount * $inviteCodeDiscount)/100;

                        $this->response['message'] = trans("Record Found"); 
                        $this->response['data'] = $data;
                        if(count($records)>0){
                            $this->response['data'][0]['referral_Code_To_Use'] =$records[0]->code;
                        }
                        else{
                            $this->response['data'][0]['referral_Code_To_Use'] ="";
                        }
                       
                        $this->response['data'][0]['available_Seats'] =$availableseats;
                        // $this->response['data'][0]['ride_Fare'] =number_format(round($data[0]['price_per_seat']*$availableseats,2),2);
                        // $this->response['data'][0]['toll_Tax'] =number_format(round( $data[0]['total_toll_tax'],2),2);
                        $this->response['data'][0]['without_Discount_Payable'] =number_format(round($payableamount_withoutdiscount,2),2);
                        $this->response['data'][0]['discount_Applied'] =number_format(round($discountapplied,2),2);
                        $this->response['data'][0]['with_Discount_Payable'] =number_format(round($payableamount_withoutdiscount - $discountapplied,2),2);
                        return response($this->response, Response::HTTP_OK);
                    }
                    else
                    {
                        // means can't accept the ride again as seats are already filled
                        $this->response['message'] = trans("Record Found"); 
                        $this->response['data'] = $data;
                        $this->response['data']['Available_seats'] =$availableseats;
                        return response($this->response, Response::HTTP_OK);
                    }
            }
            
            
        }
        catch(Exception $ex){
            Log::error($ex);
            $this->response['message'] = 'Server Internal Error.';
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /* #endregion */

    /* #region  sendBookingRequest */
    
    /**
     * @SWG\Post(
     *     path="/sendBookingRequest",
     *     tags={"Booking"},
     *     summary="Send Booking Request to the Driver(Another User). It means when Booked Successfully screen will be used this api will be used.",
     *     description="Using this API you can send the Booking Request to the Driver(Another User). ",
     *     operationId="sendBookingRequest",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Ride ID",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="rideid",
     *              type="integer"
     *             ),
     *             @SWG\Property(
     *             property="consume_seats",
     *             type="integer"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function sendBookingRequest(sendBookingRequestRequest $request){
        $type="";
        try{

            $ridedatetime = Rides::select('id','ride_date_time')
                            ->where(['id' => $request['rideid']])
                            ->first();

            if( !$ridedatetime->ride_date_time >= now() ){
                $this->response['message'] = "Ride Date time is passed. Can't send booking request";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }


            $is_accepted_by_driver="";
            $ride = RideAccepted::where([
                'ride_id' => $request['rideid'],
                'user_id' => Auth::user()->id,
                'status' => 0,
            ])
            ->first();
            
            // if rides is not equal to null then it means that this user has already sent a ride 
            // request regarding this ride 
            if($ride!= null){
                // if he has sent the request then check what is it, pending or accepted or rejected
                if($ride->is_accepted_by_driver == 0)
                    $is_accepted_by_driver = "Your ride is still in Pending Status, You can't send again request.";
                if($ride->is_accepted_by_driver == 1)
                    $is_accepted_by_driver = "Your ride is already Accepted, You can't send again request.";
                if($ride->is_accepted_by_driver == 2)
                    $is_accepted_by_driver = "Your ride is Rejected by the Driver, You can't send again request.";
                $ride =[];
                $this->response['message'] = $is_accepted_by_driver;
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            else{
                // if there doesn't exists any record then check whether the total consume_seats is
                // equal or less then the available seats
                $ride = Rides::selectRaw('ride.user_id,max_no_seats ,sum(ride_accepted.consume_seats) as total_consume_seats')
                            ->leftjoin('ride_accepted','ride_accepted.ride_id','ride.id')
                            ->where(['ride_id' => $request['rideid']])
                            ->groupBy('max_no_seats','ride.user_id')
                            ->first();
                
                if($ride == null || $ride->max_no_seats >= ($ride->total_consume_seats + $request['consume_seats']))
                {
                    if($ride == null)
                    {
                        // creating a record for ride request
                        $ride = RideAccepted::create([
                            'user_id' => Auth::user()->id,
                            'ride_id' => $request['rideid'],
                            'consume_seats' => $request['consume_seats'],
                            'is_accepted_by_driver' => 0,
                            'status' => 0
                        ]);
                        if($ride)
                            $is_accepted_by_driver = 'Booking Request Sent Successfully';
                         // sending Notification
                         $type=0; // Booking Request Received
                         $title = 'Booking Request Received';
                         $message = 'Ride ID : '.  $request['rideid'].', Dear Sir/Madam, Booking Requests Recieved Successfully';
                         $dataPush = [
                            'type' => $type,
                         ];
                         $rideNoti  = Rides::where([
                            'id' => $request['rideid'],
                        ])
                        ->first();

                        

                        DeviceDetails::sendNotification($title ,$message, $rideNoti->user_id,$dataPush,"",$type);
                    }
                    else{
                        //checking if the user is the owner of the ride requested
                        if($ride->user_id == Auth::user()->id){
                            $this->response['message'] = "You can't send request to your own published Rides";
                            $this->response['data'] = [];
                            return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        else{
                            // creating a record for ride request
                            $ride = RideAccepted::create([
                                'user_id' => Auth::user()->id,
                                'ride_id' => $request['rideid'],
                                'consume_seats' => $request['consume_seats'],
                                'is_accepted_by_driver' => 0,
                                'status' => 0
                            ]);
                            if($ride)
                            {
                                $rideNoti  = Rides::where([
                                    'id' => $request['rideid'],
                                ])
                                ->first();
                                $this->response['data'] = $ride;
                                $is_accepted_by_driver = 'Booking Request Sent Successfully';
                                
                                 // sending Notification
                                $type=0; // Booking Request Received
                                $title = 'Booking Request Received';
                                $message = 'Ride ID : '.  $request['rideid'].', Dear Sir/Madam, Booking Requests Recieved Successfully';
                                $dataPush = [
                                    'type' => $type,
                                ];
                                
                                DeviceDetails::sendNotification($title ,$message, $rideNoti->user_id,$dataPush,"",$type);
                            }
                        }
                    }
                    
                }
                else{
                    $is_accepted_by_driver = "The ride is already filled with the passengers or you are requesting for more no of seats then available seats.";
                    $this->response['message'] = $is_accepted_by_driver;
                    $this->response['data'] = [];
                    return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
            }
            
            $this->response['message'] = $is_accepted_by_driver;
            return response($this->response, Response::HTTP_OK);
        }
        catch(Exception $ex){
            Log::error($ex);
            $this->response['message'] = 'Server Internal Error.';
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
    }
    /* #endregion */

    /* #region  getRideRequest */
    
    /**
     * @SWG\Get(
     *     path="/getRideRequest",
     *     tags={"Booking"},
     *     summary="Get the list of all the requests from the passengers(Another User)",
     *     description="Get the list of all the requests from the passengers(Another User) <br> * 0 means pending ride only <br>* 1 means accepted rides only <br>* 2 means rejected rides only <br>* 3 means all rides (Pending, Accepted, Rejected) <br>* Note :: This api will not return those rides which are paid or completed",
     *     operationId="getRideRequest",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Parameters",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="type",
     *              description="* 0 means pending ride only * 1 means accepted rides only * 2 means rejected rides only * 3 means all rides (Pending, Accepted, Rejected) * Note :: This api will not return those rides which are paid or completed",
     *              type="integer"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function getRideRequest(getRideRequestRequest $request){
        try{
            // Get Ride Request of the authorized user 
            // type means 0,1,2,3 
            //=> 0 means pending ride only
            //=> 1 means accepted rides only
            //=> 2 means rejected rides only
            //=> 3 means all rides (Pending, Accepted, Rejected)
            // Note :: This api will not return those rides which are paid or completed
            $rides = RideAccepted::with([
                'startLocation' => function($query){
                    $query->select(['ride_id',"latitude","longitude","city","state","country","full_address","type"]);
                },
                'endLocation' => function($query){
                    $query->select(['ride_id',"latitude","longitude","city","state","country","full_address","type"]);
                },
                'rideDetails' => function($query){
                    $query->select(["id","user_id","ride_date_time"]);
                },
                'rideDetails.userDetails' => function($query){
                    $query->select(["id","firstname","lastname","image","social_image"]);
                },
                
            ]);
            
            if($request->type != 3)
            {
                $rides->where([
                        'is_accepted_by_driver' => $request->type,
                        'ride_accepted.status' =>0 
                        ]);
            }
            else
            {
                // output depends on request->type value 0 or 1 or 2
                $rides->where([
                        'ride_accepted.status' =>0 
                        ]);
            }
            
            $rides->join('ride','ride.id','=','ride_accepted.ride_id');
            $rides->join('users','users.id','=','ride_accepted.user_id');
            $rides->where('ride.user_id', Auth::user()->id);
            $rides->selectRaw('ride_accepted.*,ride.user_id as Driverid, ride_date_time, max_no_seats, price_per_seat,total_toll_tax,ride.status as RideStatus,vehicle_id,users.firstname,users.lastname,users.social_image,users.image' );
            
            
            $rides = $rides->get();
            
            
            if($rides == null || count($rides) == 0)
            {
                $this->response['message']= "No Data Found";
                $this->response['data']= [];
                return response($this->response, Response::HTTP_OK);    
            }
            else
            {
                $this->response['message']= "Data Found";
                $this->setData($rides);
                $this->response['data']=$this->setData($rides);
                return response($this->response, Response::HTTP_OK);    
            }
        }
        catch(Exception $ex){
            Log::error($ex);
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /* #endregion */

    /* #region  acceptOrRejectRideRequest */
    
    /**
     * @SWG\Post(
     *     path="/acceptOrRejectRideRequest",
     *     tags={"Booking"},
     *     summary="Please use this api when you are the creater of the ride and you have got the request for the ride and you want to change the status of the ride for that particular user",
     *     operationId="acceptOrRejectRideRequest",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Parameters",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="rideid",
     *              description="Ride Id for which you want to change the request",
     *              type="integer"
     *             ),
     *              @SWG\Property(
     *              property="userid",
     *              description="User Id whose request status is to be changed for that ride",
     *              type="integer"
     *             ),
     *              @SWG\Property(
     *              property="status",
     *              description="change status",
     *              type="integer"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function acceptOrRejectRideRequest(acceptOrRejectRideRequestRequest $request){
        try{
            //check if the user is the owner of the ride request or not
            //if he is the right user then fetch data
            // parameters --> rideid,
            // parameter --> status
            //=> 0 means pending ride only
            //=> 1 means accepted rides only
            //=> 2 means rejected rides only
            $ride = RideAccepted::where([
                                    'ride_id' => $request->rideid,
                                    'status' => 0,
                                    'user_id' => $request->userid
                                ])
                                ->first();
                                
            
            if($ride == null){
                $this->response['message']= "No available request found, Either this ride is completed or Paid";
                $this->response['data']= [];
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);    
            }
            else{
                if($ride->user_id == Auth::user()->id){
                    $this->response['message']= "You can't accept this ride, It was requested by You.";
                    $this->response['data']= [];
                    return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);    
                }
                $ride->is_accepted_by_driver = $request->status;
                $ride->save();
                
                $ride = RideAccepted::where([
                    'ride_id' => $request->rideid,
                    'user_id' => $request->userid
                ])
                ->first();
                
                // sending Notification
                $title = 'Ride Request status changed';
                if($request->status==0){
                    $message ='Ride ID : '.  $ride->ride_id. ', Dear Sir/Madam, Your Ride request is put into Pending State ';
                    $type=1; // Ride Request Status Changed
                }
                if($request->status==1){
                    $message ='Ride ID : '.  $ride->ride_id. ', Dear Sir/Madam, Your Ride Request is accepted by the driver';
                    $type=2; // Ride Request Status Changed
                }
                if($request->status==2){
                    $message ='Ride ID : '.  $ride->ride_id. ', Dear Sir/Madam, Ride Request is Rejected by the driver';
                    $type=3; // Ride Request Status Changed
                }
                $dataPush = [
                    'type' => $type,
                ];
                DeviceDetails::sendNotification($title ,$message, $ride->user_id,$dataPush,"",$type);

                $this->response['message']= "Updated Successfully";
                $this->response['data']= [];
                return response($this->response, Response::HTTP_OK);    
            }
        }
        catch(Exception $ex){
            Log::error($ex);
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /* #endregion */


    /* #region  ride/completeRide */
    
    /**
     * @SWG\Post(
     *     path="ride/completeRide",
     *     tags={"Booking"},
     *     summary="This API is used to complete the ride either by the Passenger or the Driver",
     *     description="This API is used to complete the ride. <br>* When a passenger wants to complete the ride then he must send the ride id and his user id <br>* When a driver is using this api then he will be able to complete the ride for all the user and ride by him (but this will work only if any of the passenger has completed the ride)",
     *     operationId="completeRide",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Parameters",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="rideid",
     *              description="Ride Id for which you the user wants to change the request",
     *              type="integer"
     *             ),
     *              @SWG\Property(
     *              property="userid",
     *              description="User Id which is requesting for completing the ride",
     *              type="integer"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function completeRide(CompleteRideRequest $request){
        try{
            // user_type : user_driver or user_passenger; this functionality will be implemented after asking from rajneesh sir
            // user--> if he is the creator of the ride then update in ride table and ride_accepted table of the ride status to completed
            // Parameters -> rideid, userid
            // now check if the current user is the creator of the user
            $userride = Rides::where([
                'user_id'=> $request['userid'],
                'id' => $request['rideid'],
            ])->first();
            
            if($userride != null && $userride->user_id == Auth::user()->id){
                // means we are using this api with the user id passed = creator of the ride,(user)
                
                // update the status in the ride table
                $ride_updated = $userride->update([
                    'status' => 2
                ]);
                
                $ride = RideAccepted::where([
                    'ride_id' => $request['rideid'],
                    'status' => 1,//In ride_accepted 1=> paid for the ride //In ride table 0 => In Progress
                ])
                ->update(['status'=>2]);

                $userride = RideAccepted::where([
                    'ride_id'=>  $request['rideid'],
                    'is_accepted_by_driver'=> 1,
                ])->get();
               
                foreach($userride as $user){
                    $title = 'Ride Status Changed';
                    $message = 'Ride ID : '.  $request['rideid'].', Dear Sir/Madam, Ride is completed by the driver';
                    $type=4;
                    $dataPush = [
                        'type' => $type,
                    ];
                    
                    DeviceDetails::sendNotification($title ,$message,$user->user_id,$dataPush,"",$type);
                }
                
               if($ride == 1 || $ride_updated == 1 ){
                    $this->response['message']= "Ride Completed Successfully from the Driver";
                    return response($this->response, Response::HTTP_OK);    
                }
                else{
                    $this->response['message']= "No available request found, Either this ride is completed or still is in Requested State(Rejected, Pending)";
                    return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);    
                }
            }




            // now if ride publisher is not equal to the passed user that means the user is passenger
            $ride = RideAccepted::where([
                'ride_id' => $request['rideid'],
                'status' => 1,//In ride_accepted 1=> paid for the ride //In ride table 0 => In Progress
                'is_accepted_by_driver' => 1,
                'user_id' => $request['userid'],
            ])
            ->first();

            if($ride == null){
                $this->response['message']= "No available request found, Either this ride is completed or still is in Requested State(Rejected, Pending)";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);    
            }
            else{
                //Now check the total count with ride_id and status and is_accepted_by_driver
                $ridecheck = RideAccepted::where([
                    'ride_id' => $request['rideid'],
                    'status' => 1,//In ride_accepted 1=> paid for the ride //In ride table 0 => In Progress
                    'is_accepted_by_driver' => 1,
                ]);
                $ridescountlast = $ridecheck->count();

                //update the ride status for the ride which is passed as parameter
                $ride->update([
                        'status'=>2
                ]);
                $this->response['message']= "Ride Completed Successfully from your side";

                 // sending Notification
                 $title = 'Ride Completed';
                 $message ='Ride ID : '.  $request['rideid'].', Dear Sir/Madam, Ride completed from the passenger';
                 $type=5;
                 $dataPush = [
                    'type' => $type,
                 ];
                 
                 DeviceDetails::sendNotification($title ,$message, $userride->user_id,$dataPush,"",$type);


                $ridecheck = RideAccepted::where([
                    'ride_id' => $request['rideid'],
                    'status' => 2,//In ride_accepted 2=> completed the ride //In ride table 0 => In Progress
                    'is_accepted_by_driver' => 1,
                ]);
                $ridescounttotal = $ridecheck->count();
                if($ridescounttotal - $ridescountlast == 1 ){
                    //means this user is the last user to complete the ride
                    //therefore it means we should update the ride table for the complete ride
                    $userride = Rides::where([
                        'id' => $request['rideid'],
                    ])->update([
                        'status' => 2
                    ]);
                    $this->response['message']= "Ride Completed Successfully from your side and Driver too";
                    
                }

                return response($this->response, Response::HTTP_OK);    
            }
            
        }
        catch(Exception $ex){
            Log::error($ex);
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /* #endregion */


    /* #region  /ride/driver/changeStatus */
    
    /**
     * @SWG\Post(
     *     path="/ride/driver/changeStatus",
     *     tags={"Ride"},
     *     summary="API to change the ride status by the driver (Whether to Start the Ride or Cancel the Ride)",
     *     description="Status Allowed : 0 = In Progress, 1 = Active,  3 = Offline(Ride Canceled) means Driver is no more interested in going <br> Status Not Allowed : 2 = Completed,",
     *     operationId="driverchangeridestatus",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Parameters",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="rideid",
     *              description="Ride Id for which you want to change the status",
     *              type="integer"
     *             ),
     *              @SWG\Property(
     *              property="ridestatus",
     *              description="Status Allowed : 0 = In Progress, 1 = Active,  3 = Offline <br> Status Not Allowed : 2 = Completed,",
     *              type="integer"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    

    /**
     * driverchangeridestatus
     *
     * @param  mixed $request
     * @return void
     */

    public function driverchangeridestatus(driverchangeridestatusRequest $request){
        try{
            $userride = Rides::where([
                'user_id'=> Auth::user()->id,
                'id' => $request['rideid'],
            ])->first();
            if($userride == null){
                $this->response['message'] = "Please Check, May be you are passing Invalid data.";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            
            if($userride->status == 3)
            {
                $this->response['message'] = "This Ride is unavailable now";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            if($request['ridestatus']==2){
                $this->response['message'] = "Invalid Request or You are not allowed to do this operation";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $ridetime = Rides::where([
                'id' => $request['rideid'],
                'status' => 1
            ])->first();

            if($ridetime == null){
                $this->response['message'] = "May be the Ride is not in Active state or May not be available. Please Check";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            
            // check the time before which the passenger can cancel the ride 
            $timesub = Setting::select('before_driv_cancel_time')->get()->first()->before_driv_cancel_time;

            // $timenowmodified = (new \DateTime(now()))->modify($timesub);
            $datetime1 = new DateTime();
            $datetime2 = new DateTime($ridetime->ride_date_time);
            $seconds = $datetime1->getTimestamp() - $datetime2->getTimestamp();
            // dd($seconds ."  " . $timesub);

            
            // dd($seconds);
            // $interval = $datetime1->diff($datetime2);
            // // time in minutes
            // dd($interval);
            // $minutes =  ($interval->format('%r%a'))*24*60 + ($interval->format('%r%h'))*60 +    ($interval->format('%r%h'))*60;
            // dd( ($interval->format('%r%a'))*24*60  ."  ". ($interval->format('%r%h'))*60 . "  " . ($interval->format('%r%h'))*60 . "  ". $timesub );
            
            // greater then is used because second should be less then ride datetime
            if($seconds >= $timesub*60 && $request['ridestatus']==3){
                $this->response['message'] = "You can not change the status right now. As the time has expired. The expiration time is ".$timesub;
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            // here 600 means --> 10 minutes earlier
            else if($seconds <= -600 && $seconds <= 60*60*2 && $request['ridestatus']==0){
                $this->response['message'] = "You can not start the ride before it's published time";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            else{
                $userride = Rides::where([
                    'user_id'=> Auth::user()->id,
                    'id' => $request['rideid'],
                ])->update(['status' => $request['ridestatus'],'started_time' => $datetime1]);

                if($userride==0){
                    $this->response['message'] = "You are not the owner of this Ride-Id";
                    return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);    
                }
                else{
                    $userride = RideAccepted::where([
                        'ride_id'=> $request['rideid'],
                        'is_accepted_by_driver' => 1,
                    ])->get();
                    
                    foreach($userride as $user){
                       
                        $title = 'Ride Status Changed';
                        if($request['ridestatus']==0){
                            $type=10;
                            $message = 'Ride ID : '.$user->ride_id. ', Dear Sir/Madam, Ride is now started, Please Pay for your Ride';
                            // 
                            
                            // check the ride owner of the ride_id is same as auth user
                            $resultQuery = Rides::where('id',$user->ride_id);
                            $data = $resultQuery->first();
                            $value =$user->ride_id;
                            $resultQuery = Rides::with([
                                'rideAccepted',
                            ])
                            ->whereIn('status',[0,1]);
                            $data = $resultQuery->get();            
                            $noofseats_accepted = 0;
                           
                            // If the query count is greater then 0 then it will proceed further
                            $noofseats_invehicle = $data[0]['max_no_seats'];
                            foreach($data[0]['rideAccepted'] as $data1){
                                $noofseats_accepted = $noofseats_accepted+ $data1["consume_seats"];
                            }
                            $availableseats = $noofseats_invehicle - $noofseats_accepted;
                            if(count($data) > 0){
                                    if($availableseats>0)
                                    {   
                                        //set available seats to 1 as only 1 seat is booked by the passenger
                                        $availableseats=1; // comment this line when you want to book more then one seats 

                                        //Counting the records of referral code of the user which are not used
                                        $records = DB::table("referral_record")->select('*')
                                                    ->whereNotIn('code',function($query){
                                                                    $query->select('referral_code')->from('transactions');
                                                    })
                                                    ->where(function ($query) use($user){
                                                        $query->where('user_id_two','=',$user->user_id)
                                                                ->orWhere('user_id_one','=',$user->user_id);
                                                    })
                                                    ->get();
                                        $inviteCodeDiscount=0;
                                        if(count($records)>0){
                                            $inviteCodeDiscount = Setting::pluck('referral_code_discount')->first();
                                        }
                                        $payableamount_withoutdiscount = $data[0]['price_per_seat']*$availableseats + $data[0]['total_toll_tax'];    
                                        $discountapplied = ($payableamount_withoutdiscount * $inviteCodeDiscount)/100;
                                        if(count($records)>0){
                                            $referral_Code_To_Use = $records[0]->code;
                                        }
                                        else{
                                            $referral_Code_To_Use = "";
                                        }
                                        
                                        $payableamount_withoutdiscount = number_format(round($payableamount_withoutdiscount,2),2);
                                        $discountapplied = number_format(round($discountapplied,2),2);
                                        $with_Discount_Payable =number_format(round($payableamount_withoutdiscount - $discountapplied,2),2);                               
                                    }
                            }
                            $dataPush = [
                                'ride_id' => $user->ride_id,
                                'referral_code' => $referral_Code_To_Use ,
                                'original_amount' =>$payableamount_withoutdiscount,
                                'discount' => $discountapplied,
                                'amount' => $with_Discount_Payable,
                                'type' => $type,
                            ];
                        }
                        
                        if($request['ridestatus']==1){
                            $message = 'Ride ID : '.$user->ride_id. ', Dear Sir/Madam, Ride is now put into Active state';
                            $type=6;
                            $dataPush = [
                                'type' =>$type,
                            ];
                            
                        }
                        if($request['ridestatus']==3){
                            $message = 'Ride ID : '.$user->ride_id. ', Dear Sir/Madam, Ride is cancelled by the driver  ';
                            $type=7;
                            $dataPush = [
                                'type' =>$type,
                            ];
                            
                        }
                            

                        
                        DeviceDetails::sendNotification($title ,$message,$user->user_id,$dataPush,"",$type);
                    }
    
                    $this->response['message'] = "Ride Status Changed Successfully";
                    if($request['ridestatus']==0)
                        $this->response['data'] = array('startedTime'=>$datetime1->format('Y-m-d H:i:s'));
                    return response($this->response, Response::HTTP_OK);
                }
            }
            
        }
        catch(Exception $ex){
            Log::error($ex);
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /* #endregion */



    /* #region  /ride/passenger/changeStatus */
    
    /**
     * @SWG\Post(
     *     path="/ride/passenger/changeStatus",
     *     tags={"Ride"},
     *     summary="API to change the ride status by the Passenger (if passenger wants to  Cancel the Ride)",
     *     description="Status Allowed :  0 = Make Request for the ride, 3 = Request Cancelled <br> Status Not Allowed : 1= Paid, 2 = Completed",
     *     operationId="passengerchangeridestatus",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Parameters",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="rideid",
     *              description="Ride Id for which you want to change the status",
     *              type="integer"
     *             ),
     *              @SWG\Property(
     *              property="ridestatus",
     *              description="Status Allowed :  0 = Make Request for the ride, 3 = Request Cancelled <br> Status Not Allowed : 1= Paid, 2 = Completed",
     *              type="integer"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */

    public function passengerchangeridestatus(driverchangeridestatusRequest $request){
        try{
            $userride = RideAccepted::where([
                'user_id'=> Auth::user()->id,
                'ride_id' => $request['rideid'],
            ])->first();
            
            if($userride == null){
                $this->response['message'] = "Please Check, May be you are passing Invalid data.";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            
            if($userride->status == 1){
                $this->response['message'] = "You have already paid for this ride. So you can't change the status of the ride";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            if($userride->status == 2){
                $this->response['message'] = "Invalid Operation being performed. Your ride is already completed.";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            if($request['ridestatus'] == 1 || $request['ridestatus'] == 2){
                $this->response['message'] = "You can't perform this operation on the request";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            
            $ridetime = Rides::where([
                'id' => $request['rideid'],
                'status' => 1
            ])->first();

            if($ridetime == null){
                $this->response['message'] = "May be the Ride is not in Active state or May not be available. Please Check";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            
            // check the time before which the passenger can cancel the ride 
            $timesub = Setting::select('before_pass_cancel_time')->get()->first()->before_pass_cancel_time;

            // $timenowmodified = (new \DateTime(now()))->modify($timesub);
            $datetime1 = new DateTime();
            $datetime2 = new DateTime($ridetime->ride_date_time);
            $seconds = $datetime1->getTimestamp() - $datetime2->getTimestamp();
            // time in minutes

            // $minutes =  $interval->format('%r%a')*24*60 + $interval->format('%r%h')*60 + $interval->format('%r%i');
            // dd($seconds." ".-$timesub*60 ." ".($seconds < $timesub*60));
            if($seconds > - $timesub*60){
                $this->response['message'] = "You can not change the status right now. As the time has expired and you can't change the ride status after ".$timesub." min of  starting ride timing";
                return response($this->response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            
            
            
            //updating status
            $userride = $userride->update(['status' => $request['ridestatus']]);
            $this->response['message'] = "Ride Request Changed Successfully";

            // sending Notification 0(Make Request) or 3(Cancel Ride Request)
            if($request['ridestatus']==0){
                $title = 'Ride Requested';
                $message = 'Ride ID : '. $request['rideid']. ', Dear Sir/Madam, There is a ride request for a ride  ';
                $type=8;
                $dataPush = [
                    'type'=>$type,
                ];
                
            }
            if($request['ridestatus']==3){
                $title = 'Ride Requested';
                $message = 'Ride ID : '. $request['rideid']. ', Dear Sir/Madam, Ride Request cancelled by the passenger  ';
                $type=9;
                $dataPush = [
                    'type' => $type,
                ];
                
            }
            
            
            
            DeviceDetails::sendNotification($title ,$message,$ridetime->user_id,$dataPush,"",$type);

            return response($this->response, Response::HTTP_OK);
            
            
        }
        catch(Exception $ex){
            Log::error($ex);
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /* #endregion */


    /* #region  /ride/passengers */
    /**
     * @SWG\Get(
     *     path="/ride/passengers",
     *     tags={"Ride"},
     *     summary="API to change the ride status by the Passenger (if passenger wants to  Cancel the Ride)",
     *     description="This API will return the Passenger Details for the passed Ride ID",
     *     operationId="checkPassengers",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Parameters",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="ride_id",
     *              description="Ride Id for which you want to check the Passengers ",
     *              type="integer"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function getPassengers(Request $request){
        try{
            $rides = RideAccepted::where(['ride_id' => $request['ride_id'], 'is_accepted_by_driver' => 1 ])
                            ->select('user_id')
                            ->get();
                
            $userDetails = User::whereIn('id',$rides)
                                ->select('id','firstname','lastname','image','social_image','date_of_birth')
                                ->get();
            // dd($userDetails);
            if(count($userDetails)>0){
                $this->response['message'] = trans("Data Found");    
                $this->setData($userDetails);
                return response($this->response, Response::HTTP_OK);
            }
            else{
                $this->response['message'] = trans("No Data Found");    
                $this->response['data'] =[];
                return response($this->response, Response::HTTP_ACCEPTED);
            }
                
        }
        catch(Exception $ex){
            Log::error($ex);
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /* #endregion */

}
