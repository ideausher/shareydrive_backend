<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Bank"))
 */

class Bank extends Model
{
    /**
     * @var string
     * @SWG\Property(
     *   property="account_holder_name",
     *   type="string" 
     * )
     *  @SWG\Property(
     *   property="account_number",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="bsb_no",
     *   type="string" 
     * )
     */

    protected $table ='user_bank_details';
    protected $fillable = [
       'user_id','details'
    ];

    /**
     * Set the user's bank details column.
     *
     * @param  string  $value
     * @return void
     */
    public function setDetailsAttribute($value)
    {
        $this->attributes['details'] = Crypt::encryptString($value);
    }

    /**
     * Set the user's bank details column.
     *
     * @param  string  $value
     * @return void
     */
    public function getDetailsAttribute($value)
    {
        return json_decode(Crypt::decryptString($value));
    }
}
