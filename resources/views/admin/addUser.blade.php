@extends('admin.layouts.default')
{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/servicecategory.categories') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Add User</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
            </div>
            <div class="col-xs-12">
                <div class="box">
                    @if(isset($category))
                    {!! Form::model($category, array('route' => array('updateCategory'), 'method' => 'POST', 'id' => 'service-form-1', 'files' => true )) !!}
                    @else
                    {!! Form::open(array('url' => 'admin/user/addUser', 'id' => 'add-vendor-form', 'files' => true)) !!}
                    @endif
                    {!! Form::hidden('category_id', isset($category) ? $category->id : 0 ,array('class'=>'form-control', 'id' => 'category_id')) !!}
                    <div class="box-body">
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('first_name','First Name') !!}
                                {!! Form::text('first_name', old('first_name'), array('class'=>'form-control')) !!}
                                {!! Form::hidden('role_id',1) !!}
                                @if(isset($category->id))
                                <input type="hidden" name="id" value="{{ $category->id }}">
                                @endif
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('last_name', 'Last Name') !!}
                                {!! Form::text('last_name', old('last_name'), array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('email', 'Email-ID') !!}
                                {!! Form::text('email', old('email'), array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('password', 'Password') !!}
                                {!! Form::password('password', array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('confirm_password', 'Confirm Password') !!}
                                {!! Form::password('confirm_password',array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('gender', 'Gender') !!}
                                {!! Form::select('gender', array('0' => 'Male', '1' => 'Female','2' => 'Others'),null,array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('phone', 'Phone No.') !!}
                                {!! Form::text('phone','',array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('country_code', 'Country Code') !!}
                                {!! Form::text('country_code',null,array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>         
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('bio', 'Bio') !!}
                                {!! Form::text('bio',null,array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>             

                        <div class="img-details">
                            <div class="col-xs-12">
                                <div class="form-group has-feedback">
                                    {!! Form::label('image', 'Upload Image') !!}
                                    <div class=row>

                                        <div class="col-md-10">
                                            {!! Form::file('image', array('id'=>'change-image','data-view'=>'uploaded-image','class'=>'form-control','style'=>'height:auto;')) !!}
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-2">
                                            <img id="uploaded-image" data-src="{!! url('/images/avatars/5e7c8139a77f8.png') !!}" src="{!! url('/images/avatars/5e7c8139a77f8.png') !!}"  height="100" width="100" alt="image">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="box-footer">
                            {!! Form::submit('Submit',array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}
                            <a href="{!! url('admin/users') !!}" class="btn btn-default">{!! 'Cancel' !!}</a>
                        </div>
                        {!! Form::close()!!}
                    </div> <!-- /.box -->
                </div> <!-- /.col-xs-12 -->
            </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@stop
{{-- Scripts --}}
@section('scripts')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXMpUMMrjVgbWeWF99SfuFQhe06-ST62s&libraries=places&callback=initMap" async defer></script>
@stop