<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="{!! (Request::is('admin/dashboard') ? 'active' : '') !!}">
                <a href="{!!url('admin')!!}">
                    <span class="fa fa-dashboard"></span> <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview {!! (Request::is('admin/settings*') || Request::is('admin/paymentsettings*') || Request::is('admin/paypalsettings*') || Request::is('admin/password/change') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <span class="fa fa-wrench"></span>
                    <span>Settings</span>
                    <span class="fa fa-angle-right pull-right"></span>
                </a>
                <ul class="treeview-menu">

                   <li class="{!! (Request::is('admin/settings*') ? 'active' : '') !!}"><a href="{!!url('admin/settings')!!}"><span class="fa fa-angle-double-right"></span>General Settings</a></li>
                   <li class="{!! (Request::is('admin/rideSettings*') ? 'active' : '') !!}"><a href="{!!url('admin/rideSettings')!!}"><span class="fa fa-angle-double-right"></span>Ride Settings</a></li>
                   <li class="{!! (Request::is('admin/referralSettings*') ? 'active' : '') !!}"><a href="{!!url('admin/referralSettings')!!}"><span class="fa fa-angle-double-right"></span>Referral Settings</a></li>
<!--                    <li class="{!! (Request::is('admin/paymentsettings*') ? 'active' : '') !!}"><a href="{!!url('admin/paymentsettings')!!}"><span class="fa fa-angle-double-right"></span>Payment Settings</a></li>-->
<!--                    <li class="{!! (Request::is('admin/paypalsettings*') ? 'active' : '') !!}"><a href="{!!url('admin/paypalsettings')!!}"><span class="fa fa-angle-double-right"></span>Paypal Settings</a></li>-->
                    {{-- <li class="{!! (Request::is('admin/settings*') ? 'active' : '') !!}"><a href="{!!url('admin/faq')!!}"><span class="fa fa-angle-double-right"></span>FAQ</a></li> --}}
                    <li class="{!! (Request::is('admin/password/change') ? 'active' : '') !!}"><a href="{!!url('admin/password/change')!!}"><span class="fa fa-angle-double-right"></span>Change Password</a></li>
                </ul>
            </li>
<!--            <li class="treeview {!! (Request::is('admin/currency*') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <span class="fa fa-money"></span>
                    <span>Currency Management</span>
                    <span class="fa fa-angle-right pull-right"></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/currency/create') ? 'active' : '') !!}"><a href="{!!url('admin/currency/create')!!}"><span class="fa fa-angle-double-right"></span>{!! trans('admin/sidebar.add_currency') !!}</a></li>
                    <li class="{!! (Request::is('admin/currency') ? 'active' : '') !!}"><a href="{!!url('admin/currency')!!}"><span class="fa fa-angle-double-right"></span>{!! trans('admin/sidebar.currency_list') !!}</a></li>
                </ul>
            </li>-->
            <li class="treeview {!! (Request::is('admin/services*') ? ' active' : '') !!} {!! (Request::is('admin/addCategory') ? ' active' : '') !!} {!! (Request::is('admin/listCategory') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <span class="fa fa-cogs"></span>
                    <span>Analytics</span>
                    <span class="fa fa-angle-right pull-right"></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/performance') ? 'active' : '') !!}"><a href="{!!url('admin/performance')!!}"><span class="fa fa-angle-double-right"></span>Performance</a></li>
                    {{-- <li class="{!! (Request::is('admin/listCategory') ? 'active' : '') !!}"><a href="{!!url('admin/listCategory')!!}"><span class="fa fa-angle-double-right"></span>Category List</a></li> --}}
                    <!-- <li class="{!! (Request::is('admin/services/create') ? 'active' : '') !!}"><a href="{!!url('admin/services/create')!!}"><span class="fa fa-angle-double-right"></span>Add Service</a></li>
                    <li class="{!! (Request::is('admin/services') ? 'active' : '') !!}"><a href="{!!url('admin/services')!!}"><span class="fa fa-angle-double-right"></span>Services List</a></li> -->
                     
                </ul>
            </li>
           <!-- <li class="{!! (Request::is('admin/refund/') ? 'active' : '') !!}">
                <a href="{!!url('admin/refund/')!!}">
                    <span class="fa fa-calendar"></span><span>Refund Requests</span>
                </a>
            </li> -->
           <li class="{!! (Request::is('admin/users') ? 'active' : '') !!}">
              <a href="javascript:;">
                    <span class="fa fa-user"></span>
                    <span>Users</span>
                    <span class="fa fa-angle-right pull-right"></span>
                    <ul class="treeview-menu">
                    <!-- <li class="{!! (Request::is('admin/paymentsettings*') ? 'active' : '') !!}"><a href="{!!url('admin/user/Add/')!!}"><span class="fa fa-angle-double-right"></span>Add User</a></li> -->
                    <li class="{!! (Request::is('admin/doctors*') ? 'active' : '') !!}"><a href="{!!url('admin/users')!!}"><span class="fa fa-angle-double-right"></span>Users List</a></li>
                    
                </ul>
                
            </li>

            {{-- <li class="treeview {!! (Request::is('admin/settings*') || Request::is('admin/paymentsettings*') || Request::is('admin/paypalsettings*') || Request::is('admin/password/change') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <span class="fa fa-user"></span>
                    <span>Vendors</span>
                    <span class="fa fa-angle-right pull-right"></span>
                </a>
            </li> --}}
            {{-- <li class="{!! (Request::is('admin/booking') ? 'active' : '') !!}">
                <a href="{!!url('admin/appointments')!!}">
                    <span class="fa fa-dollar"></span><span>{!! trans('admin/sidebar.booking_list') !!}</span>
                </a>
            </li> --}}
           {{-- <li class="treeview {!! (Request::is('admin/coupons/create') ? ' active' : '') !!} {!! (Request::is('admin/coupons') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <span class="fa fa-tags"></span>
                    <span>{!! trans('admin/sidebar.coupons') !!}</span>
                    <span class="fa fa-angle-right pull-right"></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/coupons/create') ? 'active' : '') !!}"><a href="{!!url('admin/coupons/create')!!}"><span class="fa fa-angle-double-right"></span>{!! trans('admin/coupons.add_coupon') !!}</a></li>
                    <li class="{!! (Request::is('admin/coupons') ? 'active' : '') !!}"><a href="{!!url('admin/coupons')!!}"><span class="fa fa-angle-double-right"></span>{!! trans('admin/coupons.list_coupon') !!}</a></li>
                </ul>
            </li> --}}
      {{-- <li class="treeview {!! (Request::is('admin/banners/create') ? ' active' : '') !!} {!! (Request::is('admin/banners') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <span class="fa fa-cogs"></span>
                    <span>{!! trans('admin/sidebar.banners') !!}</span>
                    <span class="fa fa-angle-right pull-right"></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/banners/create') ? 'active' : '') !!}"><a href="{!!url('admin/banners/create')!!}"><span class="fa fa-angle-double-right"></span>{!! trans('admin/banners.add_banner') !!}</a></li>
                    <li class="{!! (Request::is('admin/banners') ? 'active' : '') !!}"><a href="{!!url('admin/banners')!!}"><span class="fa fa-angle-double-right"></span>{!! trans('admin/banners.list_banner') !!}</a></li>
                </ul>
            </li> --}}
<!--            <li class="{!! (Request::is('admin/transaction') ? 'active' : '') !!}">
                <a href="{!!url('admin/transaction')!!}">
                    <span class="fa fa-dollar"></span><span>{!! trans('admin/sidebar.transaction_list') !!}</span>
                </a>
            </li>-->
            <!--li class="{!! (Request::is('admin/chatboard/*') ? 'active' : '') !!}">
                            <a href="{!!url('admin/chatboard')!!}">
                                <span class="fa fa-comment"></span> <span>{!! trans('admin/sidebar.chat_dashboard') !!}</span>
                            </a>
                        </li-->
            <!-- <li class="treeview {!! (Request::is('admin/enquiry*') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <span class="fa fa-question-circle"></span>
                    <span>{!! trans('admin/sidebar.enquiry_management') !!}</span>
                    <span class="fa fa-angle-right pull-right"></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/enquiry') ? 'active' : '') !!}"><a href="{!!url('admin/enquiry')!!}"><span class="fa fa-angle-double-right"></span>{!! trans('admin/sidebar.enquiry_list') !!}</a></li>
                </ul>
            </li> -->

            </li>
            
            <?php /*
            <li class="{!! (Request::is('admin/transaction') ? 'active' : '') !!}">
                <a href="{!!url('admin/transaction')!!}">
                    <span class="fa fa-dollar"></span><span>{!! trans('admin/sidebar.transaction_list') !!}</span>
                </a>
            </li>
            */ ?>

            <!-- <li class="{!! (Request::is('admin/notifications') ? ' active' : '') !!}">
                <a href="{!!url('admin/notifications/all')!!}">
                    <span class="fa fa-bell"></span>
                    <span>Notifications</span>
                </a>
            </li> -->
            <li class="{!! (Request::is('admin/notifications') ? ' active' : '') !!}">
              <a href="javascript:;">
                    <span class="fa fa-bell"></span>
                    <span>Notifications</span>
                    <span class="fa fa-angle-right pull-right"></span>
                    <ul class="treeview-menu">
                    <!-- <li class="{!! (Request::is('admin/paymentsettings*') ? 'active' : '') !!}"><a href="{!!url('admin/user/Add/')!!}"><span class="fa fa-angle-double-right"></span>Add User</a></li> -->
                    <li class="{!! (Request::is('admin/notifications/all') ? ' active' : '') !!}"><a href="{!!url('admin/notifications/all')!!}"><span class="fa fa-angle-double-right"></span>View Notifications</a></li>
                    <li class="{!! (Request::is('admin/sendNotifications') ? ' active' : '') !!}"><a href="{!!url('admin/sendNotifications')!!}"><span class="fa fa-angle-double-right"></span>Send Notifications</a></li>
                </ul>
                
            </li>

			<li class="{!! (Request::is('admin/notifications') ? ' active' : '') !!}">
                <a href="{!!url('admin/userReport')!!}">
                    <span class="fa fa-bell"></span>
                    <span>Report</span>
                </a>
            </li>
            <li class="{!! (Request::is('admin/usersIssues') ? ' active' : '') !!}">
                <a href="{!!url('admin/usersIssues')!!}">
                    <span class="fa fa-bell"></span>
                    <span>User's Issue Requests</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>