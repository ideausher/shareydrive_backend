<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Get In Touch"))
 */

class GetInTouch extends Model
{
    
    /**
     * @var string
     * @SWG\Property(
     *   property="email",
     *   type="string" 
     * )
     *  @SWG\Property(
     *   property="ride_id",
     *   type="integer" 
     * )
     * @SWG\Property(
     *   property="subject",
     *   type="string" 
     * )
     * @SWG\Property(
     *   property="description",
     *   type="string" 
     * )
     */

     protected $table ='get_in_touch';
     protected $fillable = [
        'user_id','email','ride_id','subject','description'
     ];

     public function rides()
    {
        return $this->belongsTo('App\Models\Rides','id','ride_id');
    }

}
