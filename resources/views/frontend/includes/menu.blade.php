<ul>
    <li class="hidden-sm {!! Request::is('dashboard') ? ' active' : '' !!}">
        <a href="{!! url('/dashboard') !!}">
            <span class="fa fa-dashboard"></span>
            {!! trans('user/menu.dashboard')!!}
        </a>
    </li>
    <li class="hidden-sm {!! Request::is('credit*') ? ' active' : '' !!}">
        <a href="{!! url('/credit') !!}">
            <span class="fa fa-dollar"></span>
            {!! trans('user/menu.buy_credit')!!}
        </a>
    </li>
    <li class="hidden-sm {!! Request::is('transaction*') ? ' active' : '' !!}">
        <a href="{!! url('/transaction') !!}">
            <span class="fa fa-dollar"></span>
            {!! trans('user/menu.transaction')!!}
        </a>
    </li>
    <li class="hidden-sm {!! Request::is('reservation*') ? ' active' : '' !!}">
        <a href="{!! url('/reservation') !!}">
            <span class="fa fa-calendar"></span>
            {!! trans('user/menu.reservation')!!}
        </a>
    </li>
    <li class="hidden-sm {!! Request::is('booking*') ? ' active' : '' !!}">
        <a href="{!! url('/booking') !!}">
            <span class="fa fa-bookmark"></span>
            {!! trans('user/menu.my_bookings')!!}
        </a>
    </li>
    <li class="hidden-sm {!! Request::is('chat*') ? ' active' : '' !!}">
        <a href="{!! url('/chat') !!}">
            <span class="fa fa-comment"></span>
            {!! trans('user/menu.start_chat')!!}
        </a>
    </li>
</ul>