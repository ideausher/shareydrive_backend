<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Datatable\SSP;
use App\Helpers\Common;
use App\User;
use App\Models\Transaction;
use App\PaymentSetting;
use App\Currency;
use Validator;
use DB;

class TransactionController extends Controller {

    /**
     * Transaction Model
     * @var Transaction
     */
    protected $transaction;
    protected $pageLimit;

    /**
     * Inject the models.
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction) {
        $this->transaction = $transaction;
        $this->pageLimit = config('settings.pageLimit');
        $this->userList = ['' => 'Select User'] + User::select(DB::raw("CONCAT(firstname, ' ',lastname) as name"), 'id')->pluck('name', 'id')->toArray();
    }

    /**
     * Display a listing of transactions
     *
     * @return Response
     */
    public function index() {
        //remove search array
        session()->forget('SEARCH');
        //end code

        $userList = $this->userList;

        $userId = request()->segment(3);
        if ($userId) {
            // Grab user the transactions
            $transactions = Transaction::userBy($userId)->orderBy('created_at', 'DESC')->paginate($this->pageLimit);
        } else {
            // Grab all the transactions
            $transactions = Transaction::orderBy('created_at', 'DESC')->paginate($this->pageLimit);
        }
        
        // Show the transaction
        return view('admin/transactionList', compact('transactions', 'userList',));
    }

    /**
     * Display the specified transaction.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $transaction = Transaction::findOrFail($id);
        return view('admin/transactionDetails', compact('transaction'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Transaction::destroy($id);

        session()->flash('success_message', trans('admin/transaction.transaction_delete_message'));
        $array = array();
        $array['success'] = true;
        //$array['message'] = 'Transaction deleted successfully!';
        echo json_encode($array);
    }

    /**
     * search transaction from database.
     * 
     * @author Dhaval
     * @param  Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {

        //reset search
        if ($request->has('reset')) {
            $request->session()->forget('SEARCH');
            return redirect('admin/transaction');
        }
        //end code

        if ($request->get('search_by') != '') {
            session(['SEARCH.SEARCH_BY' => trim($request->get('search_by'))]);
        }

        if ($request->get('search_txt') != '') {
            session(['SEARCH.SEARCH_TXT' => trim($request->get('search_txt'))]);
        }

        if ($request->get('user_id') != '') {
            session(['SEARCH.USER_ID' => trim($request->get('user_id'))]);
        }

        if ($request->get('search_date') != '') {
            session(['SEARCH.SEARCH_DATE' => trim($request->get('search_date'))]);
            session(['SEARCH.SEARCH_DATE_TO' => trim($request->get('search_date_to'))]);
        }

        if ($request->session()->get('SEARCH.SEARCH_BY') != '') {
            $query = Transaction::select('*');

            if ($request->session()->get('SEARCH.SEARCH_BY') == 'user') {
                $query->where('user_id', $request->session()->get('SEARCH.USER_ID'));
            }

            if ($request->session()->get('SEARCH.SEARCH_BY') == 'trans_id') {
                $query->where('trans_id', $request->session()->get('SEARCH.SEARCH_TXT'));
            }

            if ($request->session()->get('SEARCH.SEARCH_BY') == 'transaction_date') {
                $date = date('Y-m-d', strtotime($request->session()->get('SEARCH.SEARCH_DATE')));
                $date_to = date('Y-m-d', strtotime($request->session()->get('SEARCH.SEARCH_DATE_TO')));
                $query->where([[DB::raw("date(created_at)"), '>=', $date], [DB::raw("date(created_at)"), '<=', $date_to]]);
            }

            if ($request->session()->get('SEARCH.SEARCH_BY') == 'online') {
                $query->where('payment_method', 0);
            }
            if ($request->session()->get('SEARCH.SEARCH_BY') == 'offline') {
                $query->where('payment_method', 1);
            }

            $transactions = $query->orderBy('created_at', 'desc')->paginate($this->pageLimit);

//            $transactions = $query->orderBy('created_at', 'DESC')->toSql();
//            echo $transactions;
//            $bindings = $query->getBindings();
//            dd($bindings);

            $userList = $this->userList;
            return view('admin/transactionList', compact('transactions', 'userList'));
        } else {
            return redirect('admin/transaction');
        }
    }

    /**
     *  export transactions-list.csv
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request) {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=transaction_' . strtotime(date('Y-m-d H:i:s')) . '.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array("Transaction", "Booking ID", "User Name", "Payment Method", "Credit", "Total Amount", "Admin Commission", "Vender Amount", "Transaction Date", "Transaction Status"), ";");
        
        $transactions = Transaction::orderBy('created_at', 'DESC')->get();
        foreach ($transactions as $data) {
            $date = date('d-m-Y h:i:s A', strtotime($data->created_at));
            $payment_method = 'Cash on delivery';
            if($data->payment_method == 0) {
                $payment_method = 'Online';
            } 
            $currency = Currency::select('code')->where('id', '=', $data->currency)->first();
            $currency = $currency->code;
            
            fputcsv($output, array(
                $data->trans_id,
                $data->booking_id,
                $data->user->firstname . ' ' . $data->user->lastname,
                $payment_method,
                $data->credit,
                $data->amount . ' ' . $currency,
                $data->admin_amount . ' ' . $currency,
                $data->vender_amount . ' ' . $currency,
                $date,
                $data->status
                    ), ";"
            );
        }
        fclose($output);
        exit;
    }

}
