<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Datatable\SSP;
use App\Helpers\Response;
use App\Models\Rides;

class RidesController extends Controller
{
    /**
     * Function to show rides management page
     * @param
     * @return View 
     * @author 'Durga Parshad'
     * @since 02-Mar-2020
     */
    public function index(Request $request){
        return view('admin/rides-list');
    }

    /**
     * Function to get rides data from database
     * @param 'data-table options'
     * @return JSON
     * @author 'Durga Parshad'
     * @since 02-Mar-2020
     */
    public function ridesData(Request $request){
        try{
            $table = 'ride';
            $primaryKey = 'id';
            $columns = array(
                array('db' => 'id', 'dt' => 0, 'field' => 'id'),
                array('db' => 'ride_date_time', 'dt' => 1, 'field' => 'ride_date_time'),
                array('db' => 'max_no_seats', 'dt' => 2, 'field' => 'max_no_seats'),
                array('db' => 'price_per_seat', 'dt' => 3, 'field' => 'price_per_seat'),
                array('db' => 'total_toll_tax', 'dt' => 4, 'field' => 'total_toll_tax'),
                array('db' => 'gender', 'dt' => 5, 'formatter' => function($d,$row){
                    if($d == 0 ){
                        $gender = 'Female';
                    }elseif($d == 1){
                        $gender = 'Male';
                    }else{
                        $gender = 'Trans-Gender';
                    }   
                    return $gender;

                },'field' => 'gender'),
                array('db' => 'status', 'dt' => 6, 'formatter' => function($d, $row){
                    switch ($d) {
                        case 1:
                            $status = '<span class="label label-info">Active</span>';
                            break;
                        case 2:
                            $status = '<span class="label label-success">Completed</span>';
                            break;
                        default:
                            $status = '<span class="label label-danger">Rejected</span>';
                    }

                    return $status;

                },'field' => 'status'),
                array('db' => 'id', 'dt' => 7, 'formatter' => function ($d, $row) {
                    $operation = '<a href="javascript:void(0);" id="' . $d . '" class="btn btn-danger d-inline-block delete-btn" title="Delete" data-toggle="tooltip"><span class="fa fa-times"></span></a>';
                    // $operation = '<a href="javascript:void(0);" id="' . $d . '" class="btn btn-danger d-inline-block delete-btn" title="Delete" data-toggle="tooltip"><span class="fa fa-times"></span></a>'
                    // .'&nbsp &nbsp<a href="'.url("admin/rides-management/edit") .'/'. $d . '" class="btn btn-primary d-inline-block" title="Edit" data-toggle="tooltip"><span class="fa fa-pencil"></span></a>';
                    return $operation;

                }, 'field' => 'action'),
            );

            $sql_details = array(
                'user' => config('database.connections.mysql.username'),
                'pass' => config('database.connections.mysql.password'),
                'db' => config('database.connections.mysql.database'),
                'host' => config('database.connections.mysql.host'),
            );
            $joinQuery = '';
        //     $joinQuery .= " LEFT JOIN role_user ru ON ru.user_id = users.id";
        //     $joinQuery .= " LEFT JOIN roles r ON r.id = ru.role_id";
        //     $joinQuery .= " LEFT JOIN user_addresses ur ON ur.user_id = users.id";
        $extraWhere = "";
            $groupBy = "";
            echo json_encode(
                    SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
            );
        }
        catch(\Exception $ex){
            echo json_encode($ex->getMessage());
        }
        
    }

    /**
     * Function to delete ride from database
     * @param 'id'
     * @return JSON
     * @author 'Durga Parshad'
     * @since 02-Mar-2020
     */
    public function rideDelete(Request $request, $id){
        try{
            Rides::destroy($id);
            $this->response['message'] = 'Ride has been deleted successfully.';    
            return response($this->response, Response::HTTP_ACCEPTED);
        }
        catch(\Exception $ex){
            $this->response['message'] = $ex->getMessage();
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Function to show edit ride form
     * @param 'id'
     * @return View
     * @author 'Durga Parshad'
     * @since 02-Mar-2020
     */
    public function edit(Request $request,$id){
        $ride_id = $id;
        return view('admin/ride',compact('ride_id'));
    }

    /**
     * Function to update details
     * @param ''
     * @return View
     * @author 'Durga Parshad'
     * @since 02-Mar-2020
     */
    public function update(Request $request){
        dd($request->all());
    }
}
