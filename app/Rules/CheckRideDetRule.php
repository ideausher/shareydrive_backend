<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Rides;
use App\Models\RideAccepted;
use Auth;

class CheckRideDetRule implements Rule
{

    /**
     * Define variable
     */
    protected $message;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ride = Rides::find($value);
        $rideAccepted = RideAccepted::where('ride_id',$value)
                                    ->sum('consume_seats');
        if(!isset($ride))
        {
            return $this->message = "Please pass a valid ride ID";
        }
        if($ride->max_no_seats < $rideAccepted)
        {
            $this->message = "No seats are available for this ride.";
        }
        if($ride->user_id == Auth::user()->id)
        {
            $this->message = "You are created owner of this ride.";
        }
        
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
