<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use App\Helpers\Response;

class driverchangeridestatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rideid' => 'required|exists:ride,id',
            'ridestatus' => 'required|integer|between:0,3'
        ];
    }
    /**
     * Overwrite Validation error response
     * 
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {   
        $failedRules = $validator->failed();
        $statusCode = Response::resposeCode(key($failedRules),$failedRules);
        $response = new JsonResponse([ 
            'message' => $validator->errors()->first()
        ], $statusCode);

        throw new ValidationException($validator, $response);

    }
}
