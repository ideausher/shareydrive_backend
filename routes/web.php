<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(array('prefix' => 'admin'), function() {
    Route::get('/', array('middleware' => 'guest.admin', 'uses' => 'Admin\LoginController@getIndex'));
    
    Route::get('logout', array('uses' => 'Admin\LoginController@doLogout'));
    Route::post('login', array('uses' => 'Admin\LoginController@doLogin'));
    Route::post('verifyOtp', array('uses' => 'Admin\LoginController@doVerifyOtp'));
    // Password Reset Routes...
    Route::get('password/reset', array('uses'=>'Admin\ForgotPasswordController@showLinkRequestForm', 'as'=>'admin.password.email'));
    Route::post('password/email', array('uses'=>'Admin\ForgotPasswordController@sendResetOtp', 'as'=>'admin.password.email'));
    Route::post('password/generate', array('uses'=>'Admin\ForgotPasswordController@verifyOtp', 'as'=>'admin.password.reset'));
    Route::post('makeNewPassword', array('uses'=>'Admin\ForgotPasswordController@makeNewPassword', 'as'=>'admin.password.reset'));

    //after login
    Route::group(array('middleware' => 'auth.admin'), function() {
    Route::get('dashboard', 'Admin\DashboardController@index');
        // Rides Management 
        Route::get('rides-management','Admin\RidesController@index');
        Route::get('rides-management/rides-data','Admin\RidesController@ridesData');
        Route::delete('rides-management/ride/{id}','Admin\RidesController@rideDelete');
        Route::get('rides-management/edit/{id}','Admin\RidesController@edit');
        Route::put('rides-management/ride','Admin\RidesController@update')->name('admin/rides-management/ride');
    
        // Settings Management
        Route::resource('settings', 'Admin\SettingsController');
        
        // Admin Profile Management
        Route::resource('profile', 'Admin\ProfileController');
        
        // Admin password change
        Route::get('password/change', array('uses' => 'Admin\ProfileController@changePassword', 'as' => 'admin.password.change'));
        Route::post('password/change', array('uses' => 'Admin\ProfileController@updatePassword', 'as' => 'admin.password.change'));
        
        // Services Management
        Route::get('services/ServicesData', 'Admin\ServicesController@getServicesData');
        Route::post('services/changeStatus', 'Admin\ServicesController@changeServiceStatus');
        Route::resource('services','Admin\ServicesController');
        
        Route::post('servicesCategory/changeStatus', 'Admin\ServiceCategoryController@changeServiceStatus');
        Route::get('listCategory', 'Admin\ServiceCategoryController@index')->name('listCategory');
        Route::get('addCategory', 'Admin\ServiceCategoryController@create')->name('addCategory');
        Route::post('saveCategory', 'Admin\ServiceCategoryController@saveCategory')->name('saveCategory');
        Route::get('editCategory/{id}', 'Admin\ServiceCategoryController@editCategory')->name('editCategory');
        Route::post('updateCategory', 'Admin\ServiceCategoryController@updateCategory')->name('updateCategory');
        Route::get('deleteCategory/{id}', 'Admin\ServiceCategoryController@deleteCategory')->name('deleteCategory');
        
        // Booking Management
        Route::get('booking/export', 'Admin\BookingController@export');
        Route::any('booking/search', array('uses' => 'Admin\BookingController@search', 'as' => 'admin.booking.search'));
        Route::post('booking/changeStatus', 'Admin\BookingController@changeBookingStatus');
        
        Route::resource('appointments','Admin\BookingController');
        
        // Transaction Management
        Route::get('transaction/export', 'Admin\TransactionController@export');
        Route::any('transaction/search', array('uses' => 'Admin\TransactionController@search', 'as' => 'admin.transaction.search'));
        Route::resource('transaction','Admin\TransactionController');
        
        // User Management
        Route::post('users/updateCredit', 'Admin\UserController@updateCredit');
        Route::get('users/UserData', 'Admin\UserController@getUserData');
        Route::get('vendors/VendorData', 'Admin\VendorController@getVendorData');
        
        Route::get('bookings/BookingData', 'Admin\BookingController@getBookingData');

        Route::post('users/changeStatus', 'Admin\UserController@changeUserStatus');
        Route::get('users/show/{id}', 'Admin\UserController@show');
        Route::get('users/showuser/{id}', 'Admin\UserController@showuser');
        Route::resource('users', 'Admin\UserController');
        Route::any('user/Add', 'Admin\UserController@Add');
        Route::any('user/addUser', 'Admin\UserController@AddUser');
        Route::any('user/edit/{id}', 'Admin\UserController@edit');
        Route::any('user/update/{id}', 'Admin\UserController@update');
		Route::any('user/view/{id}','Admin\UserController@view');
        Route::any('userReport','Admin\UserController@userReport');
        Route::post('users/changeStatus', 'Admin\UserController@changeUserStatus');
        
        //Notifications Management
        Route::get('notifications/{type}', 'Admin\NotificationController@getNotifications');
        Route::get('sendNotifications', 'Admin\NotificationController@fetchNotification');
        Route::POST('sendNotifications', 'Admin\NotificationController@sendNotification');
 
        //faq
        Route::get('faq','Admin\SettingsController@faq');
        Route::get('faqData','Admin\SettingsController@faqData');
        Route::get('faq/{id}/edit','Admin\SettingsController@faqEdit');
        Route::post('faq/save-edit/{id}','Admin\SettingsController@faqSaveEdit');
        Route::get('faq/add','Admin\SettingsController@faqAdd');
        Route::post('faq/save-add','Admin\SettingsController@faqSaveAdd');
        Route::delete('faq/{id}','Admin\SettingsController@faqDelete');
        Route::any('deleteEdu','Admin\VendorController@deleteEdu');
        //referral_settings
        Route::get('referralSettings','Admin\SettingsController@showReferral');
        Route::POST('referralSettings','Admin\SettingsController@updateReferral');
        //ride_settings
        Route::get('rideSettings','Admin\SettingsController@showRideSettings');
        Route::POST('rideSettings','Admin\SettingsController@updateRideSettings');
        //performance, userIssues
        Route::get('performance','Admin\PerformanceController@showOverallPerformance');
        Route::get('usersIssues','Admin\GetInTouchController@showAllIssues');
        Route::get('getIssueRequestData','Admin\GetInTouchController@getIssueRequestData');
    });
});

/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */
Route::get('/', function () {
    return redirect('admin.login');
});
//before login
Route::post('contact', array('uses' => 'Frontend\HomeController@submitEnquiry', 'as' => 'contact'));
Route::post('Notification', array('uses' => 'Frontend\HomeController@submitNotification', 'as' => 'Notification'));
//login user
Route::post('login', array('uses' => 'Frontend\LoginController@doLogin', 'as' => 'frontend.login'));
Route::resource('users', 'Frontend\UserController');
// Password Reset Routes...
Route::get('password/reset', array('uses'=>'Frontend\ForgotPasswordController@showLinkRequestForm', 'as'=>'password.email'));
Route::post('password/email', array('uses'=>'Frontend\ForgotPasswordController@sendResetLinkEmail', 'as'=>'password.email'));
Route::get('password/reset/{token}', array('uses'=>'Frontend\ResetPasswordController@showResetForm', 'as'=>'password.reset'));
Route::post('password/reset', array('uses'=>'Frontend\ResetPasswordController@reset', 'as'=>'password.reset'));
//after login
Route::group(array('middleware' => 'auth.user'), function() {
    Route::get('dashboard', 'FrontendDashboardController\DashboardController@index');
    Route::get('profile', 'Frontend\UserController@index');
    #user password change
    Route::get('password/change', array('uses' => 'Frontend\UserController@changePassword', 'as' => 'password.change'));
    Route::post('password/change', array('uses' => 'Frontend\UserController@updatePassword', 'as' => 'password.change'));
    #logout user
    Route::get('logout', 'Frontend\LoginController@doLogout');
});


