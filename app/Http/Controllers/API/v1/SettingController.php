<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TermsandCondition;
use App\Mail\ContactUsMail;
use App\Jobs\SendEmailJob;
use Illuminate\Mail\Mailer;
use App\Helpers\Response;
use App\Setting;
use Mail;
use Auth;

class SettingController extends Controller
{
    protected $response = [
        'message' => ''
    ];

    public function __construct() {
        $this->response['data'] = new \stdClass();
    }

    protected function setData($data){
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->response['data'] = $value;
        return $this->response['data'];
    }

    /* #region  /term_condition */
    
    /**
     * @SWG\Get(
     *     path="/term_condition",
     *     tags={"Terms And Condition"},
     *     summary="Get term and condition text",
     *     description="Get term and condition using API's",
     *     operationId="getTermCondition",
     *     @SWG\Parameter(
     *         name="Language",
     *         in="header",
     *         description="Language",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function termsandcondition(TermsandCondition $request){

        try {

            $setting = Setting::where('language', $request['language'])
                            ->pluck('term_condition');
                    
            $returnData = $setting[0] ? $setting[0] : '';

            if (!$returnData) {
                $this->response['status'] = 0;
                $this->response['message'] = trans('No terms and conditions found!');
                $this->response['data'] = $returnData;
                return response()->json($this->response, 200);
            }
            
            $this->response['status'] = 1;
            $this->response['message'] = trans('Terms and Conditions Found');
            $this->response['data'] = $returnData;
            return response()->json($this->response, 200);
        } catch (\Exception $ex) {
            $this->response['message'] = trans('Something Went Wrong');
            $this->response['message'] = $ex->getMessage();
            return response($this->response, 500);
        }
    }   
    /* #endregion */

    /* #region  /contact_us */
    
    /**
     * @SWG\Post(
     *     path="/contact_us",
     *     tags={"Terms And Condition"},
     *     summary="Send contact us mail",
     *     description="Send contact us mail using API's",
     *     operationId="getContactUs",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Contact us",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *              property="subject",
     *              type="string"
     *             ),
     *             @SWG\Property(
     *              property="description",
     *              type="string"
     *             )
     *          )
     *     ),
     *     @SWG\Response(response=200, description="In Case of Success Process"),
     *     @SWG\Response(response=201, description="Create New Entry in Database"),
     *     @SWG\Response(response=202, description="Empty Data Found in Database"),
     *     @SWG\Response(response=403, description="Duplicasy case"),
     *     @SWG\Response(response=400, description="Request parameter value required"),
     *     @SWG\Response(response=401, description="Wrong Access Token or null"),
     *     @SWG\Response(response=404, description="Resource not found"),
     *     @SWG\Response(response=409, description="Conflict"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function contactUs(Request $request){
        try{

            $setting = Setting::find(1);
            $user = Auth::user();
            $data = $request->all();
            // code to send email to be added here
            $Emaildata = [
                'email' => $user->email,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'subject' => $data['subject'],
                'description' => $data['description']
            ];
            Mail::to($setting->email)
                ->send(new ContactUsMail($Emaildata));
            // code of mail to sent ends here


            // serving response to the user
            $this->response['message'] = "Mail is sent to admin successfully."; 
            return response($this->response, Response::HTTP_OK);
        }
        catch(Exception $ex)
        {
            $this->response['message'] = 'Server Internal Error.';
            return response($this->response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    
    }
    /* #endregion */
}
