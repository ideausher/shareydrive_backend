<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use App\Helpers\Response;

class GetRides extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_latitude' => ['required'],
            'start_longitude' => ['required'],
            'end_latitude' => ['required'],
            'end_longitude' => ['required'],
            'date' => ['required','date','after_or_equal:today','date_format:Y-m-d H:i:s'],
            'page' => ['required','integer'],
            'limit' => ['required','integer'],
            'distance' => ['required','integer'],
            'available_seats' => ['sometimes', 'integer'],
            'gender' => ['sometimes','integer'],
            'review_ratings' => ['sometimes','integer'],
            'min_price' => ['sometimes'],
            'max_price' => ['required_with:min_price']
        ];
    }

    /**
     * Overwrite Validation error response
     * 
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {   
        $failedRules = $validator->failed();
        $statusCode = Response::resposeCode(key($failedRules),$failedRules);
        $response = new JsonResponse([ 
            'message' => $validator->errors()->first()
        ], $statusCode);

        throw new ValidationException($validator, $response);

    }

}
